#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include <vector>

#include <limits>

#include <stdint.h>

#include "image.h"
//#include "median_filter.h"

#include <dirent.h>

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.h"
#include "Color_Separability_2_Intervals_FunctOnly.h"

//#define USING_HISTOGRAM_EQUALIZATION

//#define PRINT_HISTOGRAM_STATISTICS
//#define PRINT_ORIGINAL_FEAS

#define SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability


#define INCLUDE_OneBest_SeparableRatioOf_2Feas 

#define PRINT_BEST_FEAS

//#define PRINTING_THE_DETAILS

///////////////////////////////////////////////////////////
#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)
#define RETURN_BY_THE_SAME_INTENSITY (-3)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

//	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax_La (2048) //2048
#define nWidMax_La (2048) //2048 

#define nLenMin_La 50 
#define nWidMin_La 50

//#define nImageAreaMax (nLenMax_La*nWidMax_La)

///////////////////////////////////////////////////////////////////
#define nLarge 5000000
//#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535
//////////////////////////////////////////////////////

	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageAreaMax (nLenMax*nWidMax)
#define nImageAreaMin (nLenMin*nWidMin)

///#define nLenSubimageToPrintMin  0 //342
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

///////////////////////////////////////////////////////////////////////////////

#define nNumOfGrayLevelIntensities 256 // (nGrayLevelIntensityMax + 1)

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nPositionOf_TotFeaToPrint (-1)

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

/////////////////////////////////////////////////////
//#define nNumOfVecs_Normal 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//#define nNumOfVecs_Normal 200 //test
//#define nNumOfVecs_Normal 500 //D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Normal-COBB-AOI\\ \\D:\Imago\Images\WoodyBreast\AOIs\Normal-Panoramic
//#define nNumOfVecs_Normal 898 //D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Dec12_Sym3\\Normal-Sym3-898\\

//#define nNumOfVecs_Normal 478 //D:\Imago\Images\spleen\Color\Oct_30\BenignFOV-Sym3
//#define nNumOfVecs_Normal 227 //D:\Imago\Images\spleen\Color\BenignAOI-Color-PNG
//#define nNumOfVecs_Normal 446 //D:\\Imago\\Images\\spleen\\FoV_Feb1\\BenignFOV446-Sym3\\

#define nNumOfVecs_Normal 974 //D:\\Imago\\Images\\spleen\\FoV_Feb1\\BenignFOV446-Sym3\\

//////////////////////////////

//#define nNumOfVecs_Malignant 575  //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 50 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 200 //test

//#define nNumOfVecs_Malignant 311 //D:\Imago\Images\spleen\Color\MalignantAOI-Color-PNG

//#define nNumOfVecs_Malignant 625 //D:\Imago\Images\spleen\Color\Oct_30\MalignantFOV-Sym3

//#define nNumOfVecs_Malignant 298 
//#define nNumOfVecs_Malignant 638 //D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Dec12_Sym3\\WB-Sym3-638\\
//#define nNumOfVecs_Malignant 551 //D:\\Imago\\Images\\spleen\\FoV_Feb1\\MalignantFOV551-Sym3\\

#define nNumOfVecs_Malignant 727 //D:\\Imago\\Images\\spleen\\FoV_Feb1\\MalignantFOV551-Sym3\\

////////////////////////////////////////////////////////////
//watch for '#define USING_HISTOGRAM_EQUALIZATION'

//////////////////////////////////////////////////////////////
#define nNumOfSelectedFea (-1) //2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//////////////////////////////////////////////////////

//#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

//#define nNumOfHistogramBinsStat (nNumOfGrayLevelIntensities) //(nIntensityStatMax - nIntensityStatMin + 1)

//#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

/////////////////////////////////////////////

//#define nBinSizeForColorSepar 1 
#define nBinSizeForColorSepar 2 //-- 

//#define nBinSizeForColorSepar 4 //,8,...

#define nNumOfBins_OneColor (nNumOfGrayLevelIntensities/nBinSizeForColorSepar) //256/2
//#define nNumOfBins_OneColor 8

//#define nSquareOfNumOfBins_OneColor (nNumOfBins_OneColor*nNumOfBins_OneColor)

#define nNumOfBins_OneImage_AllColors (nNumOfBins_OneColor*nNumOfBins_OneColor*nNumOfBins_OneColor) 
//#define nNumOfBins_OneImage_AllColors 512//(8^3) 

#define nIntensityOfBackground 255 // or 0 --check it up

#define nBinIntensityOfBackground (nIntensityOfBackground/nBinSizeForColorSepar) // or 0 --check it up

#define nNumOfOneColorBinsInAll_NorImages (nNumOfVecs_Normal * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_NorImages 400 //(50 * 8)

#define nNumOfOneColorBinsInAll_MalImages (nNumOfVecs_Malignant * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_MalImages 400 //(50 * 8)

#define nNumOfBins_AllColorsForAll_NorImages (nNumOfBins_OneImage_AllColors * nNumOfVecs_Normal)
//#define nNumOfBins_AllColorsForAll_NorImages 25600 //512*50

#define nNumOfBins_AllColorsForAll_MalImages (nNumOfBins_OneImage_AllColors * nNumOfVecs_Malignant)
//#define nNumOfBins_AllColorsForAll_MalImages 25600  //512*50
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//separability of non-zero bins

#define nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMin 20

#define nNumOf_NonZeroMalImagesMin 2 //4 //< nNumOfVecs_Malignant

//#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin 2.5 //4.0
#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin 5.0

//#define fRatioOfSumsOfPixelsOfNonZero_MalToNorMin 4.0 //10.0
#define fRatioOfSumsOfPixelsOfNonZero_MalToNorMin 10.0

		//fRatioOfNumOfPixelsIn_ABin_InANonZeroImage_ForSeparMin
//#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinForSeparMin 5.0

//for nPositOfBinsAboveTheMinRatio_InNonZeroNeg_Images_ForSeparArrf[[]
#define nNumOf_2_Intervals_InNonZeroMal_ImagesMax 100

////////////////////////////////////////////////////////////////////////
#define nNumOfBinCurOfInterest (-1)
///////////////////////////////
#define fEfficBestSeparOfOneFeaAcceptMax (-1.01) //the min at the perfect separation is (-2.0) 

//#define nNumVecTrain_1st (nNumOf_TrainVecs_Normal) //10 

//#define nNumVecTrain_2nd (nNumOf_TrainVecs_Malignant) //10 //(nNumOf_TrainVecs_Normal) //1000 //100

//#define nProdTrain_1st (nDim*nNumOf_TrainVecs_Normal)

//#define nProdTrain_2nd (nDim*nNumVecTrain_2nd)

//#define nNumIterForDataGenerMax (nNumOf_TrainVecs_Normal*50)

#define nNumDistiguishFeasForInit 2
/////////////////////////////////////////////////
#define fEfficiency_ToPrint (-1.5)

#define fThreshold_HighSeparability 0.5
#define fThreshold_LowSeparability_1 0.3
#define fThreshold_LowSeparability_2 0.7
/////////////////////////////////////////////////
#define fBorderBel 0.0
#define fBorderAbove 1.0

#define nNumIterMaxForFindingBestSeparByRatioOfOneFea 10 //100
#define nNumIterMaxForFindingBestSeparByDiffOfOneFea (nNumIterMaxForFindingBestSeparByRatioOfOneFea)
////////////////////////////////////
//color intervals
//#define nNumOfColorIntervals 6
//#define nNumOfColorIntervals 4
//#define nNumOfColorIntervals 5
#define nNumOfColorIntervals 3
//#define nNumOfColorIntervals 2
//#define nNumOfColorIntervals 1

#define nNumOfFeasOfOneColor (nNumOfColorIntervals*2)

#define nNumOfBoundariesFor_3ColorsAnd_OneInterval 6
#define nNumOfFeas (nNumOfColorIntervals*nNumOfBoundariesFor_3ColorsAnd_OneInterval) //times 2 (the min and max values for one inteval) and times 3 (the num of colors) //4 

//////////////////////////////////////
//CuckSear

#define SKIPPNG_PRINTING_IN_StochHillClimbing

#define SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr
//#define SKIPPING_PRINTING_IN_ValuesOf2DimFeaArr
#define SKIPPNG_PRINTING_IN_OptByCuckSear

//#define nDimOfOneFea (nNumOfFeas) //20

//#define nNumOfFeas 4 

//#define nNumOfNes 5 
//#define nNumOfNes 20 
//#define nNumOfNes 50 //-1.58
//#define nNumOfNes 100 //
#define nNumOfNes 200 //
//#define nNumOfNes 400 //-1.55
//#define nNumOfNes 800 //-1.55 //
//#define nNumOfNes 2000 //-1.55

#define nNumOfSelecCombins (nNumOfNes)

#define nNumOfItersForCuckSear 20000  
//#define nNumOfItersForCuckSear 8000  
//#define nNumOfItersForCuckSear 4000  
//#define nNumOfItersForCuckSear 2000  
//#define nNumOfItersForCuckSear 400 
//#define nNumOfItersForCuckSear 8 

#define nNumOfRandItersforRestart 2 //20 //80 //200 //400 
#define nNumOfRandItersforLocalPerturb 5 //200 //500 //100 //50 

#define nNumOfRandInitializingLoopsMax 500000
//#define nNumOfFuncEvalsMax 20000 

#define nLocalRadiusOfWholeRegion (nNumOfBins_OneColor) //20

//#define nLocalRadiusOfNeigh 20
#define nLocalRadiusOfNeigh 10
//#define nLocalRadiusOfNeigh 5 //3

#define fProbOfWorstSubstitution 0.25
//////////////////////////////////////////////

#define fEfficForPrinting (-1.4)
//#define fEfficForPrinting (-1.44)
//#define fEfficForPrinting (-1.25)
//#define fEfficForPrinting (-1.15)

typedef struct
{
	float 
		fWeightOfRed; //<= 1.0
	float
		fWeightOfGreen; //<= 1.0
	float
		fWeightOfBlue; //<= 1.0

} WEIGHTES_OF_RGB_COLORS;

/////////////////////////////////////////////////////////

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

////////////////////////////////////////////////////////////////
//cooc feas
typedef struct
{
	int nIndicOfClass;

	int nLength; // nLenCur; //<= nLenMax
	int nWidth; // nWidCur; //<= nWidMax

	//int nPixelArr[nImageAreaMax];
	int *nPixelArr; //Grayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef)/3;

} GRAYSCALE_IMAGE;
///////////////////////////////

typedef struct
{
	int nRedIntervals_Low[nNumOfColorIntervals]; // 
	int nRedIntervals_High[nNumOfColorIntervals]; // 

	int nGreenIntervals_Low[nNumOfColorIntervals]; // 
	int nGreenIntervals_High[nNumOfColorIntervals]; // 

	int nBlueIntervals_Low[nNumOfColorIntervals]; // 
	int nBlueIntervals_High[nNumOfColorIntervals]; // 

///////////////////////////////////////
	int nNumOfRed_NonOverlappingIntervals;
	int nRed_NonOverlappingIntervals_Low[nNumOfColorIntervals]; // 
	int nRed_NonOverlappingIntervals_High[nNumOfColorIntervals]; // 

	int nNumOfGreen_NonOverlappingIntervals;
	int nGreen_NonOverlappingIntervals_Low[nNumOfColorIntervals]; // 
	int nGreen_NonOverlappingIntervals_High[nNumOfColorIntervals]; // 

	int nNumOfBlue_NonOverlappingIntervals;
	int nBlue_NonOverlappingIntervals_Low[nNumOfColorIntervals]; // 
	int nBlue_NonOverlappingIntervals_High[nNumOfColorIntervals]; // 


} BOUNDARIES_OF_INTERVALS;


//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 10) + (nNumOfScalesAndDistTot*10) )

//
/////////////////////////////////////////////////////////////





