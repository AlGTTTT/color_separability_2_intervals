
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

int
	nNumOfProcessedFiles_NormalTot_Glob;
//#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//#endif //#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
#include "Color_Separability_2_Intervals_function.cpp"

int main()
{
	int Reading_AColorImage(
		const Image& image_in,

		COLOR_IMAGE *sColor_Image);

	int	Converting_OneFeaVec_ToAVecWithSelecFeas(
		const int nDim_1f,
		const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

		const float f_1Arrf[], //[nDim_1f]

		const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

		float f_SelecArrf[]); //[nDim_2f] //fActualFeasFor_OneNormalVec_CoocExtendedArr[]

#ifdef USING_HISTOGRAM_EQUALIZATION
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);

	int Histogram_Statistics_ColorActuallyGrayscale_Image(

		//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
		const COLOR_IMAGE *sColor_Imagef,

		float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		float &fMeanf,
		float &fStDevf,
		float &fSkewnessf,
		float &fKurtosisf);

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

	void Initializing_AFloatVec_To_Zero(
		const int nDimf,
		float fVecArrf[]); //[nDimf]

	int NumOfPixelsIn_ColorBins_OneImage(
		const int nImageWidthf,
		const int nImageLengthf,

		const COLOR_IMAGE *sColor_Imagef, //[]

		int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
		int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
		int nNumOfPixelsIn_BlueBinsArrf[], //[nNumOfBins_OneColor]
		///////////////////////////////////////////////////
		int nNumOfPixelsIn_CombinedColorBins_OneImageArrf[]); //[nNumOfBins_OneImage_AllColors]
	//////////////////

	int SeparabilityOf_2_Color_Intervals(

		const int nBinSizeForColorSeparCurf, //== 'nBinSizeForColorSepar'

		const int nNumOfBins_OneImage_AllColorsf,
		////////////////////////////////////
		//for NonZero images
		const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

		const int nNumOf_NonZeroMalImagesMinf,

		const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

		const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

		///////////////////////////////

		const int nNumOfVecs_Norf,
		const int nNumOfVecs_Malf,
		//////////////////////////////////////////////////////////
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Norf]
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malf]

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

		const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
	///////////////////////////////////////////////////

		long &l_Seedf, //must be already specified
	/////////////////////////////////////////////
		BOUNDARIES_OF_INTERVALS *sBoundaries_ForBestSeparf,

		float &fBestBinSeparabilityBy_2_Intervalsf);
	
/////////////////////////////////////////////////////
	//printf("\n\n 1_1: Press any key:");	getchar();

	float
		fNumOfBins_AllColorsForAll_NorImages = (float)(nNumOfBins_OneImage_AllColors)*(float)(nNumOfVecs_Normal),
		fNumOfBins_AllColorsForAll_MalImages = (float)(nNumOfBins_OneImage_AllColors)*(float)(nNumOfVecs_Malignant);

	if (fNumOfBins_AllColorsForAll_NorImages > 2.147E+9 || fNumOfBins_AllColorsForAll_MalImages > 2.147E+9)
	{
		printf("\n\n An error by too many feas in 'main': fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E",
			fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages);

		printf("\n\n Please increase 'nBinSizeForColorSepar' or decrease the number of images");
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (fNumOfBins_AllColorsForAll_NorImages > 2.147E+9 || fNumOfBins_AllColorsForAll_MalImages > 2.147E+9)

////////////////////////////////////////////////////////

#if 1
	int
		iFea,

		nImageWidth, // = image_in.width();
		nImageHeight, // = image_in.height();
		nSizeOfImage,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nIndexOfPixelCur,

		nNumOf_2_Intervals_InNonZeroNeg_Images,
		nRed, 
		nGreen,
		nBlue,

		nRedBin_Low, //int &nRedBinf,
		nRedBin_High, //int &nRedBinf,
		nGreenBin_Low, //int &nGreenBinf,
		nGreenBin_High, //int &nGreenBinf,
		nBlueBin_Low, // int &nBlueBinf)
		nBlueBin_High, // int &nBlueBinf)

		nIndex,

		nIndex_AllVecs,
		nInitFea_ForAllVecs,
		nTempf,

		nDimSelec,
		nNumOfProcessedFiles_NormalTot = 0,
		nNumOfProcessedFiles_MalignantTot = 0,

		nNumOf_ActualFeas_ForAll_Normal_VecsTotf,
		nNumOf_ActualFeas_ForAll_Malignant_VecsTotf,

		nRedOfBestSeparMin,
		nRedOfBestSeparMax,

		nGreenOfBestSeparMin,
		nGreenOfBestSeparMax,

		nBlueOfBestSeparMin,
		nBlueOfBestSeparMax,
		////////////////////////

		nNumOfPixelsIn_RedBinsArr[nNumOfBins_OneColor],
		nNumOfPixelsIn_GreenBinsArr[nNumOfBins_OneColor],
		nNumOfPixelsIn_BlueBinsArr[nNumOfBins_OneColor],

		nNumOfPixelsIn_CombinedColorBins_OneImageArr[nNumOfBins_OneImage_AllColors], //

		nAreaOf_NorImagesArr[nNumOfVecs_Normal],
		nAreaOf_MalImagesArr[nNumOfVecs_Malignant],

//#define nNumOfOneColorBinsInAll_NorImages (nNumOfVecs_Normal * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_MalImages (nNumOfVecs_Malignant * nNumOfBins_OneColor)

		nRedAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		nGreenAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		nBlueAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],

		nRedAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		nGreenAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		nBlueAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
///////////////////////
//		nAllBinsAll_Colors_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
//		nAllBinsAll_Colors_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],

		nPositOfBins_InNonZeroNeg_Images[nNumOf_2_Intervals_InNonZeroMal_ImagesMax], //[nNumOf_2_Intervals_InNonZeroMal_ImagesMax] 

		nImageAreaCur,

		iBin,
		nIndexOfBin,

		nPositOfBins_InNonZeroNeg_LowCur,
		nPositOfBins_InNonZeroNeg_HighCur,

		iIntervf,
		//iSetOfIntervf,
		///////////////////////
		iVec,
		nRes;
//////////////////////////////////
//	printf("\n\n 1_2: Press any key:");	getchar();

	long
		l_Seed = 3; //2

	float
		fFeaCur,
		////////////////////////////////////////

		fRedAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		fGreenAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		fBlueAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],

		fRedAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		fGreenAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		fBlueAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		////////////////////////////////		
		//fAllBinsAll_ColorsAver_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
		//fAllBinsAll_ColorsAver_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],

		fBestBinSeparabilityBy_2_Intervals,

		fLargestRatioOfBinsAboveTheMinRatioForSepar,

		fNumOf_NormalVecsSeparBestMax,
		fNumOf_MalignantVecsSeparBestMax;

	////////////////////////////////////////////////////////////////
	int *nAllBinsAll_Colors_NorImagesArr = new int[nNumOfBins_AllColorsForAll_NorImages]; //0 -- 1 //[nDimSelecMaxf]
		if (nAllBinsAll_Colors_NorImagesArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nAllBinsAll_Colors_NorImagesArr'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nAllBinsAll_Colors_NorImagesArr == nullptr)

	int *nAllBinsAll_Colors_MalImagesArr = new int[nNumOfBins_AllColorsForAll_MalImages]; //0 -- 1 //[nDimSelecMaxf]

	if (nAllBinsAll_Colors_MalImagesArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nAllBinsAll_Colors_MalImagesArr'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		delete[] nAllBinsAll_Colors_NorImagesArr;
		return UNSUCCESSFUL_RETURN;
	} //if (nAllBinsAll_Colors_MalImagesArr == nullptr)

	float *fAllBinsAll_ColorsAver_NorImagesArr = new float[nNumOfBins_AllColorsForAll_NorImages]; //0 -- 1 //[nDimSelecMaxf]
	if (nAllBinsAll_Colors_NorImagesArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllBinsAll_ColorsAver_NorImagesArr'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		delete[] nAllBinsAll_Colors_NorImagesArr;
		delete[] nAllBinsAll_Colors_MalImagesArr;

		return UNSUCCESSFUL_RETURN;
	} //if (fAllBinsAll_ColorsAver_NorImagesArr == nullptr)

	float *fAllBinsAll_ColorsAver_MalImagesArr = new float[nNumOfBins_AllColorsForAll_MalImages]; //0 -- 1 //[nDimSelecMaxf]
	if (nAllBinsAll_Colors_MalImagesArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllBinsAll_ColorsAver_MalImagesArr'");
		printf("\n Please press any key to exit"); getchar(); exit(1);
		delete[] nAllBinsAll_Colors_NorImagesArr;
		delete[] nAllBinsAll_Colors_MalImagesArr;

		delete[] fAllBinsAll_ColorsAver_NorImagesArr;

		return UNSUCCESSFUL_RETURN;
	} //if (fAllBinsAll_ColorsAver_MalImagesArr == nullptr)
//////////////////////////////////////////
//	BOUNDARIES_OF_INTERVALS sBoundaries_InNonZeroNeg_ImagesArr[nNumOf_2_Intervals_InNonZeroMal_ImagesMax]; //
	BOUNDARIES_OF_INTERVALS sBoundaries_ForBestSepar; //
////////////////////////////////////////////////////////////

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		nAreaOf_NorImagesArr[iVec] = 0;
	} //for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++) 

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		nAreaOf_MalImagesArr[iVec] = 0;
	} //for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++) 

////////////////////////////////////////////////////////////////
	//printf("\n\n 1: Press any key:");	getchar();
	//		nNumOfPixelsIn_CombinedColorBins_OneImageArr[nNumOfBins_OneImage_AllColors], //
	for (iFea = 0; iFea < nNumOfBins_OneImage_AllColors; iFea++)
	{
		//[nNumOfBins_OneImage_AllColors]
		nNumOfPixelsIn_CombinedColorBins_OneImageArr[iFea] = 0;
	} // for (iFea = 0; iFea < nNumOfBins_OneImage_AllColors; iFea++)

	for (iFea = 0; iFea < nNumOfOneColorBinsInAll_NorImages; iFea++)
	{
		//nNumOfOneColorBinsInAll_NorImages
		nRedAll_NorImagesArr[iFea] = 0;
		nGreenAll_NorImagesArr[iFea] = 0;
		nBlueAll_NorImagesArr[iFea] = 0;

		//nNumOfOneColorBinsInAll_NorImages
		fRedAllAver_NorImagesArr[iFea] = 0.0;
		fGreenAllAver_NorImagesArr[iFea] = 0.0;
		fBlueAllAver_NorImagesArr[iFea] = 0.0;

	} // for (iFea = 0; iFea < nNumOfOneColorBinsInAll_NorImages; iFea++)
////////////////////////////////
//	printf("\n\n 2: Press any key:");	getchar();

	for (iFea = 0; iFea < nNumOfOneColorBinsInAll_MalImages; iFea++)
	{
		//nNumOfOneColorBinsInAll_MalImages
		nRedAll_MalImagesArr[iFea] = 0;
		nGreenAll_MalImagesArr[iFea] = 0;
		nBlueAll_MalImagesArr[iFea] = 0;

		//nNumOfOneColorBinsInAll_MalImages
		fRedAllAver_MalImagesArr[iFea] = 0.0;
		fGreenAllAver_MalImagesArr[iFea] = 0.0;
		fBlueAllAver_MalImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfOneColorBinsInAll_MalImages; iFea++)
//////////////////////////////
	//printf("\n\n 3: Press any key:");	getchar();

	for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
	{
		//nNumOfBins_AllColorsForAll_NorImages
		nAllBinsAll_Colors_NorImagesArr[iFea] = 0;

		//nNumOfBins_AllColorsForAll_NorImages
		fAllBinsAll_ColorsAver_NorImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
//////////////////////////////

	for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_MalImages; iFea++)
	{
		//nNumOfBins_AllColorsForAll_MalImages
		nAllBinsAll_Colors_MalImagesArr[iFea] = 0;

		//nNumOfBins_AllColorsForAll_MalImages
		fAllBinsAll_ColorsAver_MalImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_MalImages; iFea++)

	//printf("\n\n 4: Press any key:");	getchar();

////////////////////////////////////////////////////////////
//https://docs.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=vs-2019
	//printf("\n\n LONG_MAX = %lld", (long long)(LONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);
	//printf("\n\n (float)(LLONG_MAX) = %E", (float)(LLONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);

	//////////////////////////////////////////////////////////////
	//fout = fopen("wColorBins_Spleen_50_50.txt", "w");
//fout = fopen("wColorBins_WoodyBreast_500_298_ColorIsland_COBB-Normal-CI2_all_16Bin.txt", "w");
	//fout = fopen("wColorBins_WoodyBreast_500_298_Sym3_2Intervals.txt", "w");
	//fout = fopen("wColorBins_WoodyBreast_500_298_Sym3_4Intervals.txt", "w");
	//fout = fopen("wColorBins_WoodyBreast_500_298_Sym3_6Intervals.txt", "w");
//	fout = fopen("wColorBins_WoodyBreast_898_638_Sym3_3Intervals.txt", "w");
	
	//fout = fopen("wColorBins_WoodyBreast_898_638_ByJermaine_3Intervals.txt", "w");
	//fout = fopen("wColorBins_Spleen_446_551_5Intervals.txt", "w");
	fout = fopen("wColorBins_Spleen_974_727_5Intervals.txt", "w");

	//fout = fopen("wColorBins_Spleen_227_311.txt", "w");
	//fout = fopen("wColorBins_Spleen_Sym3_478_625.txt", "w");

	//fout = fopen("wVecsSpleen_Ben_Mal_472_575_Cooc2.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fout == NULL)

	//fout_features = fopen("wColorInterval_Feas_898_638_Sym3_3Interv.txt", "w");
	//fout_features = fopen("wColorInterval_Feas_898_638_ByJermaine_3Interv.txt", "w");
	fout_features = fopen("wColorInterval_Feas_Spleen_974_727_5Interv.txt", "w");
	if (fout_features == NULL)
	{
		printf("\n\n fout_features == NULL");
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fout_features == NULL)

/////////////////////////////
	printf("\n\n The beginning: fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E, nNumOfBins_OneImage_AllColors = %d",
		fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages, nNumOfBins_OneImage_AllColors);

	fprintf(fout, "\n\n The beginning: fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E, nNumOfBins_OneImage_AllColors = %d",
		fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages, nNumOfBins_OneImage_AllColors);

	fprintf(fout, "\n\n nBinSizeForColorSepar = %d, nNumOfColorIntervals = %d, nNumOfFeas = %d",nBinSizeForColorSepar, nNumOfColorIntervals, nNumOfFeas);

	//printf("\n\n Please press any key:");	getchar();

	////////////////////
	
	Image image_in;

	char cFileName_inDirectoryOf_Normal_Images[200]; //[150]
	char cFileName_inDirectoryOf_Malignant_Images[200]; //[150]
	//image_in.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIR;
	struct dirent *entry;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//copy the same path to 'NumOfPixelsIn_ColorIntervals_All_Images()'! 

//woody breast
//50 D:\Imago\Images\spleen\Benign_AOI_50_SelecColor
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Dec12_Sym3\\Normal-Sym3-898\\"; //copy to if (pDIR = opendir(.. as well
	
//	char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Jan15_Jermaine\\Normal-Sym3-898\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\FoV_Feb1\\BenignFOV446-Sym3\\"; //copy to if (pDIR = opendir(.. as well
	
	//D:\Imago\Images\WoodyBreast\Color_Images\Feb25_2021_Sym3\Normal-UM-974-Sym3
	char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Feb25_2021_Sym3\\Normal-UM-974-Sym3\\"; //copy to if (pDIR = opendir(.. as well

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//check out '#define nNumOfVecs_Malignant'!

//	char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Jan15_Jermaine\\WB-Sym3-638\\"; //copy to if (pDIR = opendir(.. as well
//	char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\FoV_Feb1\\MalignantFOV551-Sym3\\"; //copy to if (pDIR = opendir(.. as well
	
	//D:\Imago\Images\WoodyBreast\Color_Images\Feb25_2021_Sym3\WB-UM-727-Sym3
	char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Feb25_2021_Sym3\\WB-UM-727-Sym3\\"; //copy to if (pDIR = opendir(.. as well

//////////////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

/////////////////////////////////////////////////////
	//copy the same path to 'NumOfPixelsIn_ColorIntervals_All_Images()'! 
//Normal 
	//////////////////////////////////////////////////////////////

	nNumOfProcessedFiles_NormalTot = 0;
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Benign_AOI_50_SelecColor\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\BenignAOI-Color-PNG\\"))


	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\Oct_30\\BenignFOV-Sym3\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\Normal-COBB-AOI\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-Normal-Sym3\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Dec12_Sym3\\Normal-Sym3-898\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Jan15_Jermaine\\Normal-Sym3-898\\"))
		
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\FoV_Feb1\\BenignFOV446-Sym3\\"))
	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Feb25_2021_Sym3\\Normal-UM-974-Sym3\\"))
	{
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_NormalTot += 1;

				nNumOfProcessedFiles_NormalTot_Glob = nNumOfProcessedFiles_NormalTot;

				if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
			
					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)

//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_inDirectoryOf_Normal_Images, '\0', sizeof(cFileName_inDirectoryOf_Normal_Images));

				strcpy(cFileName_inDirectoryOf_Normal_Images, cInput_DirectoryOf_Normal_Images);

				strcat(cFileName_inDirectoryOf_Normal_Images, entry->d_name);

				fprintf(fout,"\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_inDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);

				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_inDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
////////////////////////////////////////
				image_in.read(cFileName_inDirectoryOf_Normal_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n Concatenated input file: %s\n", cFileName_inDirectoryOf_Normal_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				//nImageAreaCur = 5000; //test only

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] nAllBinsAll_Colors_NorImagesArr;
					delete[] nAllBinsAll_Colors_MalImagesArr;

					delete[] fAllBinsAll_ColorsAver_NorImagesArr;
					delete[] fAllBinsAll_ColorsAver_MalImagesArr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n\n So far nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)

				nImageAreaCur = sColor_Image.nLength*sColor_Image.nWidth;

				nAreaOf_NorImagesArr[nNumOfProcessedFiles_NormalTot - 1] = nImageAreaCur;

				if (nImageAreaCur < nImageAreaMin)
				{
					printf("\n\n An error in 'main' 1: nImageAreaCur = %d < nImageAreaMin = %d", nImageAreaCur,nImageAreaMin);

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] nAllBinsAll_Colors_NorImagesArr;
					delete[] nAllBinsAll_Colors_MalImagesArr;

					delete[] fAllBinsAll_ColorsAver_NorImagesArr;
					delete[] fAllBinsAll_ColorsAver_MalImagesArr;

					return UNSUCCESSFUL_RETURN;
				} //if (nImageAreaCur < nImageAreaMin)
////////////////////////////////////

				nRes = NumOfPixelsIn_ColorBins_OneImage(
					sColor_Image.nWidth, //const int nImageWidthf,
					sColor_Image.nLength, //const int nImageLengthf,

					&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[]

					nNumOfPixelsIn_RedBinsArr, //int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_GreenBinsArr, //int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_BlueBinsArr, // int nNumOfPixelsIn_BlueBinsArrf[]) //[nNumOfBins_OneColor]

					nNumOfPixelsIn_CombinedColorBins_OneImageArr); // int nNumOfPixelsIn_CombinedColorBins_OneImageArrf[]); //[nNumOfBins_OneImage_AllColors]

				for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1) * nNumOfBins_OneColor;
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
					{
						printf("\n\n An error in 'main' 1: nIndexOfBin = %d >= nNumOfOneColorBinsInAll_NorImages = %d", nIndexOfBin, nNumOfOneColorBinsInAll_NorImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);

						delete[] nAllBinsAll_Colors_NorImagesArr;
						delete[] nAllBinsAll_Colors_MalImagesArr;

						delete[] fAllBinsAll_ColorsAver_NorImagesArr;
						delete[] fAllBinsAll_ColorsAver_MalImagesArr;

						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)

					nRedAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_RedBinsArr[iBin];
					nGreenAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_GreenBinsArr[iBin];
					nBlueAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_BlueBinsArr[iBin];

					fRedAllAver_NorImagesArr[nIndexOfBin] = (float)(nRedAll_NorImagesArr[nIndexOfBin])/(float)(nImageAreaCur);
					fGreenAllAver_NorImagesArr[nIndexOfBin] = (float)(nGreenAll_NorImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fBlueAllAver_NorImagesArr[nIndexOfBin] = (float)(nBlueAll_NorImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)

///////////////////////////////////////////
				//printf("\n\n 5_1: please press any key:");	getchar();

//fAllBinsAll_ColorsAver_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
				for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;

					//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfBins_AllColorsForAll_NorImages)
					{
						printf("\n\n An error in 'main' 2: nIndexOfBin = %d >= nNumOfBins_AllColorsForAll_NorImages = %d, iBin = %d, nNumOfBins_OneImage_AllColors = %d",
							nIndexOfBin, nNumOfBins_AllColorsForAll_NorImages, iBin, nNumOfBins_OneImage_AllColors);

						printf("\n\n Press any key to exit");	getchar(); exit(1);

						delete[] nAllBinsAll_Colors_NorImagesArr;
						delete[] nAllBinsAll_Colors_MalImagesArr;

						delete[] fAllBinsAll_ColorsAver_NorImagesArr;
						delete[] fAllBinsAll_ColorsAver_MalImagesArr;

						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)

					nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin];

					fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin]) / (float)(nImageAreaCur);

					//printf("\n iBin = %d, nAllBinsAll_Colors_NorImagesArr[%d] = %d, fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = %E",
						//iBin,nIndexOfBin, nAllBinsAll_Colors_NorImagesArr[nIndexOfBin], fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin]);

				} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)

			//	printf("\n\n 5_2: please press any key:");	getchar();

		////////////////////////////////////////////////

				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n Normal: the end of nNumOfProcessedFiles_NormalTot = %d, nNumOfVecs_Normal = %d; going to to the next image", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Normal_AOI_50_SelecColor\\"))

	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of normal images can not be opened");

		printf("\n\n Press any key to exit"); fflush(fout);	getchar(); exit(1);

		perror("");

		delete[] nAllBinsAll_Colors_NorImagesArr;
		delete[] nAllBinsAll_Colors_MalImagesArr;

		delete[] fAllBinsAll_ColorsAver_NorImagesArr;
		delete[] fAllBinsAll_ColorsAver_MalImagesArr;
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

//printf("\n\n The end of normal inages: press any key to exit");	fflush(fout);  getchar(); exit(1);
	//printf("\n\n The end of normal images: press any key to continue to the malignant ones");	fflush(fout);  getchar();

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//copy the same path to 'NumOfPixelsIn_ColorIntervals_All_Images()'! 
//Malignant 

//check out '#define nNumOfVecs_Malignant'!
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\WB-Color-AOI\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\MalignantAOI-Color-PNG\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\Oct_30\\MalignantFOV-Sym3"))

//	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-WB-Sym3\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Dec12_Sym3\\WB-Sym3-638\\"))
//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_2H\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_all\\"))


	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Jan15_Jermaine\\WB-Sym3-638\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\FoV_Feb1\\MalignantFOV551-Sym3\\"))

	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Feb25_2021_Sym3\\WB-UM-727-Sym3\\"))
	{
		nNumOfProcessedFiles_MalignantTot = 0;
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_MalignantTot += 1;

				if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

					delete[] nAllBinsAll_Colors_NorImagesArr;
					delete[] nAllBinsAll_Colors_MalImagesArr;

					delete[] fAllBinsAll_ColorsAver_NorImagesArr;
					delete[] fAllBinsAll_ColorsAver_MalImagesArr;
					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)

				//Initializing_AFloatVec_To_Zero(
					//nNumOf_CoocExtendedFeas_OneDimTot, //const int nDimf,
					//fOneDim_CoocExtendedArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_inDirectoryOf_Malignant_Images, '\0', sizeof(cFileName_inDirectoryOf_Malignant_Images));

				strcpy(cFileName_inDirectoryOf_Malignant_Images, cInput_DirectoryOf_Malignant_Images);

				strcat(cFileName_inDirectoryOf_Malignant_Images, entry->d_name);

				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_MalignantTot = %d\n", cFileName_inDirectoryOf_Malignant_Images, nNumOfProcessedFiles_MalignantTot);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)

				//fprintf(fout, "\n\n Concatenated input file: %s\n", cFileName_inDirectoryOf_Malignant_Images);

				//printf("\n\n Press any key 1:");	fflush(fout);  getchar();
////////////////////////////////////////////////
				image_in.read(cFileName_inDirectoryOf_Malignant_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					delete[] nAllBinsAll_Colors_NorImagesArr;
					delete[] nAllBinsAll_Colors_MalImagesArr;

					delete[] fAllBinsAll_ColorsAver_NorImagesArr;
					delete[] fAllBinsAll_ColorsAver_MalImagesArr;
					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n\n So far nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)

/////////////////////////////////////////////////////////////////

				nImageAreaCur = sColor_Image.nLength*sColor_Image.nWidth;

				nAreaOf_MalImagesArr[nNumOfProcessedFiles_MalignantTot - 1] = nImageAreaCur;

				if (nImageAreaCur < nImageAreaMin)
				{
					printf("\n\n An error in 'main' 2: nImageAreaCur = %d < nImageAreaMin = %d", nImageAreaCur, nImageAreaMin);

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] nAllBinsAll_Colors_NorImagesArr;
					delete[] nAllBinsAll_Colors_MalImagesArr;

					delete[] fAllBinsAll_ColorsAver_NorImagesArr;
					delete[] fAllBinsAll_ColorsAver_MalImagesArr;
					return UNSUCCESSFUL_RETURN;
				} //if (nImageAreaCur < nImageAreaMin)
////////////////////////////////////

				nRes = NumOfPixelsIn_ColorBins_OneImage(
					sColor_Image.nWidth, //const int nImageWidthf,
					sColor_Image.nLength, //const int nImageLengthf,

					&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[]

					nNumOfPixelsIn_RedBinsArr, //int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_GreenBinsArr, //int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_BlueBinsArr, // int nNumOfPixelsIn_BlueBinsArrf[]) //[nNumOfBins_OneColor]

					nNumOfPixelsIn_CombinedColorBins_OneImageArr); // int nNumOfPixelsIn_CombinedColorBins_OneImageArrf[]); //[nNumOfBins_OneImage_AllColors]

				for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfBins_OneColor;
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)
					{
						printf("\n\n An error in 'main' 3: nIndexOfBin = %d >= nNumOfOneColorBinsInAll_MalImages = %d", nIndexOfBin, nNumOfOneColorBinsInAll_MalImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);

						delete[] nAllBinsAll_Colors_NorImagesArr;
						delete[] nAllBinsAll_Colors_MalImagesArr;

						delete[] fAllBinsAll_ColorsAver_NorImagesArr;
						delete[] fAllBinsAll_ColorsAver_MalImagesArr;
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)

					nRedAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_RedBinsArr[iBin];
					nGreenAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_GreenBinsArr[iBin];
					nBlueAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_BlueBinsArr[iBin];

					fRedAllAver_MalImagesArr[nIndexOfBin] = (float)(nRedAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fGreenAllAver_MalImagesArr[nIndexOfBin] = (float)(nGreenAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fBlueAllAver_MalImagesArr[nIndexOfBin] = (float)(nBlueAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
///////////////////////////////////////////

//fAllBinsAll_ColorsAver_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],
				for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfBins_OneImage_AllColors;

					//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfBins_AllColorsForAll_MalImages)
					{
						printf("\n\n An error in 'main' 4: nIndexOfBin = %d >= nNumOfBins_AllColorsForAll_MalImages = %d", nIndexOfBin, nNumOfBins_AllColorsForAll_MalImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);

						delete[] nAllBinsAll_Colors_NorImagesArr;
						delete[] nAllBinsAll_Colors_MalImagesArr;

						delete[] fAllBinsAll_ColorsAver_NorImagesArr;
						delete[] fAllBinsAll_ColorsAver_MalImagesArr;
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)

					nAllBinsAll_Colors_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin];

					fAllBinsAll_ColorsAver_MalImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
////////////////////////////////////////////////////////////////////
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n Malignant: the end of nNumOfProcessedFiles_MalignantTot = %d, nNumOfVecs_Malignant = %d; going to to the next image",
						nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n The directory of malignant images can not be opened");

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

		delete[] nAllBinsAll_Colors_NorImagesArr;
		delete[] nAllBinsAll_Colors_MalImagesArr;

		delete[] fAllBinsAll_ColorsAver_NorImagesArr;
		delete[] fAllBinsAll_ColorsAver_MalImagesArr;		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else
////////////////////////////////////////////////////////////////////

nRes = SeparabilityOf_2_Color_Intervals(

	// must be equal to 'nBinSizeForColorSepar' 
	nBinSizeForColorSepar, //const int nBinSizeForColorSeparCurf, //could be less than 'nBinSizeForColorSepar'

	nNumOfBins_OneImage_AllColors, //const int nNumOfBins_OneImage_AllColorsf,
	//////////////////////////////////////
	/*
		const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	*/
	nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMin, //const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

	nNumOf_NonZeroMalImagesMin, //const int nNumOf_NonZeroMalImagesMinf,

	//fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin
	fRatioOfSumsOfPixelsOfNonZero_MalToNorMin, //const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

	fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin, //const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

////////////////////////////////
	nNumOfVecs_Normal, //const int nNumOfVecs_Norf,
	nNumOfVecs_Malignant, //const int nNumOfVecs_Malf,

	nAreaOf_NorImagesArr, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Norf]
	nAreaOf_MalImagesArr, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malf]

	nAllBinsAll_Colors_NorImagesArr, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	nAllBinsAll_Colors_MalImagesArr, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

	fAllBinsAll_ColorsAver_NorImagesArr, //const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	fAllBinsAll_ColorsAver_MalImagesArr, //const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
/////////////////////////////////////////////
	l_Seed, //long &l_Seedf, //must be already specified

	&sBoundaries_ForBestSepar, //BOUNDARIES_OF_INTERVALS *sBoundaries_ForBestSeparf,
	fBestBinSeparabilityBy_2_Intervals); //float &fBestBinSeparabilityBy_2_Intervalsf,

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'main' by 'SeparabilityOf_2_Color_Intervals'");

		printf("\n\n Press any key to exit");	getchar(); exit(1);

		delete[] nAllBinsAll_Colors_NorImagesArr;
		delete[] nAllBinsAll_Colors_MalImagesArr;

		delete[] fAllBinsAll_ColorsAver_NorImagesArr;
		delete[] fAllBinsAll_ColorsAver_MalImagesArr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

printf("\n\n After 'SeparabilityOf_2_Color_Intervals': fBestBinSeparabilityBy_2_Intervals = %E", fBestBinSeparabilityBy_2_Intervals);

fprintf(fout, "\n\n After 'SeparabilityOf_2_Color_Intervals': fBestBinSeparabilityBy_2_Intervals = %E", fBestBinSeparabilityBy_2_Intervals);

	
		//fprintf(fout, "\n nPositOfBins_InNonZeroNeg_Images[%d] = %d (related to nBinSizeForColorSepar)", iBin, nPositOfBins_InNonZeroNeg_Images[iBin]);
		//nPositOfBins_InNonZeroNeg_LowCur = nPositOfBins_InNonZeroNeg_Images[iBin];
		for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)
		{
			nRedBin_Low = sBoundaries_ForBestSepar.nRedIntervals_Low[iIntervf];
			nRedBin_High = sBoundaries_ForBestSepar.nRedIntervals_High[iIntervf];

			if (nRedBin_Low < 0 || nRedBin_Low >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nRedBin_Low = %d, nNumOfBins_OneColor = %d, iIntervf = %d", 
					nRedBin_Low, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nRedBin_Low < 0 || nRedBin_Low >= nNumOfBins_OneColor)

			if (nRedBin_High < 0 || nRedBin_High >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nRedBin_High = %d, nNumOfBins_OneColor = %d, iIntervf = %d",
					nRedBin_High, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nRedBin_High < 0 || nRedBin_High >= nNumOfBins_OneColor)

			nGreenBin_Low = sBoundaries_ForBestSepar.nGreenIntervals_Low[iIntervf];
			nGreenBin_High = sBoundaries_ForBestSepar.nGreenIntervals_High[iIntervf];

			if (nGreenBin_Low < 0 || nGreenBin_Low >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nGreenBin_Low = %d, nNumOfBins_OneColor = %d, iIntervf = %d",
					nGreenBin_Low, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nGreenBin_Low < 0 || nGreenBin_Low >= nNumOfBins_OneColor)

			if (nGreenBin_High < 0 || nGreenBin_High >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nGreenBin_High = %d, nNumOfBins_OneColor = %d, iIntervf = %d",
					nGreenBin_High, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nGreenBin_High < 0 || nGreenBin_High >= nNumOfBins_OneColor)
/////////////////////////////////////////////

			nBlueBin_Low = sBoundaries_ForBestSepar.nBlueIntervals_Low[iIntervf];
			nBlueBin_High = sBoundaries_ForBestSepar.nBlueIntervals_High[iIntervf];

			if (nBlueBin_Low < 0 || nBlueBin_Low >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nBlueBin_Low = %d, nNumOfBins_OneColor = %d, iIntervf = %d",
					nBlueBin_Low, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nBlueBin_Low < 0 || nBlueBin_Low >= nNumOfBins_OneColor)

			if (nBlueBin_High < 0 || nBlueBin_High >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'main': nBlueBin_High = %d, nNumOfBins_OneColor = %d, iIntervf = %d",
					nBlueBin_High, nNumOfBins_OneColor, iIntervf);

				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] nAllBinsAll_Colors_NorImagesArr;
				delete[] nAllBinsAll_Colors_MalImagesArr;

				delete[] fAllBinsAll_ColorsAver_NorImagesArr;
				delete[] fAllBinsAll_ColorsAver_MalImagesArr;
				return UNSUCCESSFUL_RETURN;
			} //if (nBlueBin_High < 0 || nBlueBin_High >= nNumOfBins_OneColor)
/////////////////////////////////////
	
			printf("\n\n The color intensity intervals for NonZero Malignant images (related to nBinSizeForColorSepar): for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d",
				nRedBin_Low*nBinSizeForColorSepar, nRedBin_High*nBinSizeForColorSepar,

				nGreenBin_Low*nBinSizeForColorSepar, nGreenBin_High*nBinSizeForColorSepar,

				nBlueBin_Low*nBinSizeForColorSepar, nBlueBin_High*nBinSizeForColorSepar);

			fprintf(fout, "\n\n nRedBin_Low = %d, nRedBin_High = %d, nGreenBin_Low = %d, nGreenBin_High = %d, nBlueBin_Low = %d, nBlueBin_High = %d",
				nRedBin_Low, nRedBin_High, nGreenBin_Low, nGreenBin_High, nBlueBin_Low, nBlueBin_High);

			fprintf(fout, "\n\n The color intensity intervals for NonZero Malignant images (related to nBinSizeForColorSepar): for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d",
				nRedBin_Low*nBinSizeForColorSepar, nRedBin_High*nBinSizeForColorSepar,

				nGreenBin_Low*nBinSizeForColorSepar, nGreenBin_High*nBinSizeForColorSepar,

				nBlueBin_Low*nBinSizeForColorSepar, nBlueBin_High*nBinSizeForColorSepar);
		
		} // for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)

printf("\n\n The end of 'main': fBestBinSeparabilityBy_2_Intervals = %E", fBestBinSeparabilityBy_2_Intervals);

#endif //#if 0
delete[] nAllBinsAll_Colors_NorImagesArr;
delete[] nAllBinsAll_Colors_MalImagesArr;

delete[] fAllBinsAll_ColorsAver_NorImagesArr;
delete[] fAllBinsAll_ColorsAver_MalImagesArr;

printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

	return SUCCESSFUL_RETURN;
} //int main()
/////////////////////////////////////


//printf("\n\n Please press any key:"); getchar();