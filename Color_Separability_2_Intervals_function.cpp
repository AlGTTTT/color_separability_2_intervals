
//delete 'return SUCCESSFUL_RETURN;' after //premature exit for testing!

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
//#include "Multifractal_SelectedFeas_2.h"
#include "Color_Separability_2_Intervals.h"

using namespace imago;

FILE *fout_lr;
FILE *fout;
FILE *fout_features;

int
	nNumOfOneFea_Spectrum_AdjustedGlob = -1,
		nNumOfSpectrumFea_Glob = -1,

	nNumOfBinCur_Glob,
	iFea_Glob,

	nNumfPixelsWithIntensityLessThanZero_AfterConvolutionInOneDirection_Glob = 0,
	nNumfPixelsWithIntensityMoreThan255_AfterConvolutionInOneDirection_Glob = 0,

	nIndexForRandSelecCombin_Glob,

	iIterForCuckSear_Glob, 
	nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob = 0,
	iIntensity_Interval_1_Glob,
	iIntensity_Interval_2_Glob;

float
	fFuncValueBest_Glob = -fLarge,
	fPercentageOfPixelsWithIntensityOffTheLimits_Glob;

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.cpp"
#include "Color_Separability_2_Intervals_FunctOnly.cpp"

////////////////////////////////////////////////

int Reading_AColorImage(
	const Image& image_in,

	COLOR_IMAGE *sColor_Image)
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

#ifdef USING_HISTOGRAM_EQUALIZATION
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);
#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;
	////////////////////////////////////////////////////////////////////////

//initialization
	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n  'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
		getchar(); exit(1);

		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n An error in reading the image height:nImageHeight = %d > nWidMax = %d", nImageHeight, nWidMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image height: nImageHeight = %d > nWidMax = %d...", nImageHeight, nWidMax);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	//Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	//COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		//&sColor_Image); // COLOR_IMAGE *sColor_Imagef);
		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Reading_AColorImage':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_AColorImage':");
		getchar(); exit(1);

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
/////////////////////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int Reading_AColorImage(...
////////////////////////////////////////////////////////////////////

int NumOfPixelsIn_ColorBins_OneImage(
	const int nImageWidthf,
	const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef, //[]

	int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
	int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
	int nNumOfPixelsIn_BlueBinsArrf[],
	
	int nNumOfPixelsIn_CombinedColorBins_OneImageArrf[]) //[nNumOfBins_OneImage_AllColors]
{
	int
		nIndexOfPixelCurf,

		nIndexOfBinCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,

		iFeaf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedBinf,
		nGreenBinf,
		nBlueBinf,

		iWidf,
		iLenf;
////////////////////////////////
	for (iFeaf = 0; iFeaf < nNumOfBins_OneColor; iFeaf++)
	{
		nNumOfPixelsIn_RedBinsArrf[iFeaf] = 0;
		nNumOfPixelsIn_GreenBinsArrf[iFeaf] = 0;
		nNumOfPixelsIn_BlueBinsArrf[iFeaf] = 0;
	} //for (iFeaf = 0; iFeaf < nNumOfBins_OneColor; iFeaf++)
//////////////////////////

	for (iFeaf = 0; iFeaf < nNumOfBins_OneImage_AllColors; iFeaf++)
	{
		nNumOfPixelsIn_CombinedColorBins_OneImageArrf[iFeaf] = 0;
	} //for (iFeaf = 0; iFeaf < v; iFeaf++)
/////////////////////////////

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nRedBinf = nRedCurf / nBinSizeForColorSepar;
			nGreenBinf = nGreenCurf / nBinSizeForColorSepar;
			nBlueBinf = nBlueCurf / nBinSizeForColorSepar;

			if (nRedBinf < 0 || nRedBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins_OneImage': nRedBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d", 
					nRedBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nRedBinf < 0 || nRedBinf >= nNumOfBins_OneColor)

			if (nGreenBinf < 0 || nRedBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins_OneImage': nGreenBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d",
					nGreenBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nGreenBinf < 0 || nGreenBinf >= nNumOfBins_OneColor)

			if (nBlueBinf < 0 || nBlueBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins_OneImage': nBlueBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d",
					nBlueBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nBlueBinf < 0 || nBlueBinf >= nNumOfBins_OneColor)

			nNumOfPixelsIn_RedBinsArrf[nRedBinf] += 1;
			nNumOfPixelsIn_GreenBinsArrf[nGreenBinf] += 1;
			nNumOfPixelsIn_BlueBinsArrf[nBlueBinf] += 1;

			nIndexOfBinCurf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) +( nBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));

			if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins_OneImage': nIndexOfBinCurf = %d >= nNumOfBins_OneImage_AllColors = %d, iLenf = %d, iWidf = %d",
					nIndexOfBinCurf, nNumOfBins_OneImage_AllColors, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)

			nNumOfPixelsIn_CombinedColorBins_OneImageArrf[nIndexOfBinCurf] += 1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_ColorBins_OneImage(...

////////////////////////////////////
int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatMax; // -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;

	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
		{
			return 0.0; //instead of 1.0
			//return SUCCESSFUL_RETURN; -- should return a float number
		} //
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(
//////////////////////////////////////////////////////////

int Initializing_Grayscale_Image(
	//const int nImageWidthf,
	//const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef,
	GRAYSCALE_IMAGE *sGrayscale_Imagef)
{
	int
		nIndexOfPixelCurf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,
		iWidf,
		iLenf,

		nRedf,
		nGreenf,
		nBluef,

		nImageSizeCurf = nImageWidthf * nImageLengthf;

	sGrayscale_Imagef->nWidth = nImageWidthf;
	sGrayscale_Imagef->nLength = nImageLengthf;

	//printf("\n Initializing_Grayscale_Image: 1, please press any key"); getchar();

	////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n ///////////////////////////////////////////////////////////////////");
	printf("\n 'Initializing_Grayscale_Image': nImageWidthf = %d, nImageLengthf = %d\n", nImageWidthf, nImageLengthf);

	//fprintf(fout_lr, "\n\n ///////////////////////////////////////////////////////////////////");
	//fprintf(fout_lr, "\n 'Initializing_Grayscale_Image':\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	sGrayscale_Imagef->nPixelArr = new int[nImageSizeCurf];

	if (sGrayscale_Imagef->nPixelArr == nullptr)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		//fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		printf("\n\n Please press any key:"); getchar();
		//#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (sGrayscale_Imagef->nPixelArr == nullptr)

	//printf("\n Initializing_Grayscale_Image: 2, please press any key"); getchar();

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//printf("\n iWidf = %d, nImageWidthf = %d", iWidf, nImageWidthf); getchar();
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			//the average of colors
			sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef) / 3;
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	//printf("\n Initializing_Grayscale_Image: the end, please press any key"); getchar();
	return SUCCESSFUL_RETURN;
}//int Initializing_Grayscale_Image(...
//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////

void Initializing_AFloatVec_To_Zero(
	const int nDimf,
	float fVecArrf[]) //[nDimf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fVecArrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
}//void Initializing_AFloatVec_To_Zero(
////////////////////////////////////////////////////////////////////////

int	Converting_All_Feas_To_ARange_0_1(
	const int nDimf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	int &nNumOfFeasWithUnusualValuesf,
	int &nNumOfFeasWith_TheSameValuesf,

	int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
	float fFeas_InitMinArrf[], //[nDimf]
	float fFeas_InitMaxArrf[], //[nDimf]

	float fArr1stf[], //[nDimf*nNumVec1stf]
	float fArr2ndf[]) //[nDimf*nNumVec2ndf]
{

	int
		nNumOfFeasFilledf = 0,

		nIndexf,
		nTempf,
		iVecf,
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	nNumOfFeasWithUnusualValuesf = 0;
	nNumOfFeasWith_TheSameValuesf = 0;
	//////////////////////////////////////

	float *fFeaRangeArrf;
	fFeaRangeArrf = new float[nDimf];

	if (fFeaRangeArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeaRangeArrf' in 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit"); fflush(fout); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeaRangeArrf == nullptr)
	
		fprintf(fout, "\n\n 'Converting_All_Feas_To_ARange_0_1': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d", nDimf, nNumVec1stf, nNumVec2ndf);
		fflush(fout);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nFeaTheSameOrNotArrf[iFeaf] = 0; //not the same initially
		fFeaRangeArrf[iFeaf] = 0.0;

		fFeas_InitMinArrf[iFeaf] = fLarge;
		fFeas_InitMaxArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//printing all fea vecs+

	/*
	fprintf(fout, "\n\n Normal:");

for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
{
	fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1': 1 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr1stf[nIndexf];

		fprintf(fout, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

//fprintf(fout, "\n\n Malignant:");

for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
{
	fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1': 2 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{

		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr2ndf[nIndexf];

		fprintf(fout, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
*/

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr1stf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//fprintf(fout, "\n 1: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout, "\n\n 1: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout, "\n\n 1: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])

		} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
//////////////////////////////////////////////////////////
		//fprintf(fout, "\n\n");
		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr2ndf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//	fprintf(fout, "\n 2: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//	fprintf(fout, "\n\n 2: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)

			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;

				if (iFeaf == nNumOfSelectedFea)
				{
					//		fprintf(fout, "\n\n 2: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
		} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)
		{
			nNumOfFeasWithUnusualValuesf += 1;
			//printf("\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
			//fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %d", iFeaf, fFeas_InitMinArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)

		if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
		{
			if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)
			{
				nNumOfFeasWithUnusualValuesf += 1;
				//printf("\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				//fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %d", iFeaf, fFeas_InitMaxArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)

		}//if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
////////////////////////////////////////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fRangeCurf = fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf];

		if (iFeaf == nNumOfSelectedFea)
		{
			//		printf( "\n\n fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
				//		fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout, "\n 'Converting_All_Feas_To_ARange_0_1': fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

		} //if (iFeaf == nNumOfSelectedFea)

		if (fRangeCurf < feps)
		{
			nFeaTheSameOrNotArrf[iFeaf] = 1; //the same 
			nNumOfFeasWith_TheSameValuesf += 1;

			fprintf(fout, "\n The next nNumOfFeasWith_TheSameValuesf = %d,  (fFeas_InitMaxArrf[%d] = %E - fFeas_InitMinArrf[%d]) = %E = %E",
				nNumOfFeasWith_TheSameValuesf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf], fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf]);

		} //if (fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf] < feps)
		else
		{
			fFeaRangeArrf[iFeaf] = fRangeCurf;
		} //else

	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	printf("\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);
	fprintf(fout, "\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

	fflush(fout);
	/////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 
		{
			//	fprintf(fout, "\n");
			for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr1stf[nIndexf] = (fArr1stf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

							//fprintf(fout, "\n  'Converting_All_Feas_To_ARange_0_1' 1: iFeaf = %d, iVecf = %d, fArr1stf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
								//iFeaf, iVecf,nIndexf,fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)

				if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					fflush(fout);

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;
					return UNSUCCESSFUL_RETURN;
				}//if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)

			} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

			//fprintf(fout, "\n");
			for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr2ndf[nIndexf] = (fArr2ndf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;

					return UNSUCCESSFUL_RETURN;
				}//if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout, "\n  'Converting_All_Feas_To_ARange_0_1' 2: iFeaf = %d, iVecf = %d, fArr2ndf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//iFeaf, iVecf, nIndexf, fArr2ndf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)
			} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

			fflush(fout);
		} //if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	delete[] fFeaRangeArrf;

	return SUCCESSFUL_RETURN;
} // int Converting_All_Feas_To_ARange_0_1(...
////////////////////////////////////////////////////////////////////////////////////////////

int ConvertingVecs_ToBest_SeparableFeas(
	const float fLargef,
	const  float fepsf,

	const int nDimf,
	const int nDimSelecMaxf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	const float fPrecisionOf_G_Const_Searchf,

	const  float fBorderBelf,
	const  float fBorderAbof,

	const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
	const float fEfficBestSeparOfOneFeaAcceptMaxf,

	float fArr1stf[], //[nDimf*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	float fArr2ndf[], //[nDimf*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

	int &nDimSelecf,

	float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
	float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

	float &fRatioBestMinf,

	int &nPosOneFeaBestMaxf,

	int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

	float &fNumArr1stSeparBestMaxf,
	float &fNumArr2ndSeparBestMaxf,

	float fRatioBestArrf[], //nDimSelecMaxf

	int nPosFeaSeparBestArrf[])	//nDimSelecMaxf
{
	int	Converting_All_Feas_To_ARange_0_1(
		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		int &nNumOfFeasWithUnusualValuesf,
		int &nNumOfFeasWith_TheSameValuesf,

		int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		float fFeas_InitMinArrf[], //[nDimf]
		float fFeas_InitMaxArrf[], //[nDimf]

		float fArr1stf[], //[nDimf*nNumVec1stf]
		float fArr2ndf[]); //[nDimf*nNumVec2ndf]

	int SelectingBestFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

		int &nDimSelecf,
//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

		float &fRatioBestMinf,

		int &nPosOneFeaBestMaxf,

		int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestMaxf,
		float &fNumArr2ndSeparBestMaxf,

		float fRatioBestArrf[], //nDimSelecMaxf

		int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

//////////////////////////////////////////
	int
		nResf,
		iFeaf,
		iVecf,
		nIndexf,
		nTempf,
		nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf;

	//nFeaTheSameOrNotArrf[nDim]; //[nDimf] // 1-- the same, 0-- not

	float
		fFeaCurf;
	//fFeas_InitMinArrf[nDim],
	//fFeas_InitMaxArrf[nDim];

	fprintf(fout, "\n\n ConvertingVecs_ToBest_SeparableFeas': begin");
	fflush(fout);

////////////////////////////////////////////
	int *nFeaTheSameOrNotArrf = new int[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNotArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotArrf == nullptr)

	float *fFeas_InitMinArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMinArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMinArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMinArrf == nullptr)

	float *fFeas_InitMaxArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMaxArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMaxArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMaxArrf == nullptr)


/////////////////////////////////////////
	printf("\n\n ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);

	fprintf(fout, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);
	fflush(fout);

	/*
		fprintf(fout, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': entry data");
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			fprintf(fout, "\n1, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr1stf[nIndexf];

				fprintf(fout, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			fprintf(fout, "\n0, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr2ndf[nIndexf];

				fprintf(fout, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		fflush(fout);
	*/
	//printf("\n\n Please press any key:"); getchar();

	nResf = Converting_All_Feas_To_ARange_0_1(
		nDimf, //const int nDimf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		nNumOfFeasWithUnusualValuesf, //int &nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf, //int &nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNotArrf, //int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		fFeas_InitMinArrf, //float fFeas_InitMinArrf[], //[nDimf]
		fFeas_InitMaxArrf, //float fFeas_InitMaxArrf[], //[nDimf]

		fArr1stf, //float fArr1stf[], //[nDimf*nNumVec1stf]  //0 -- 1 now
		fArr2ndf); // float fArr2ndf[]); //[nDimf*nNumVec2ndf]  //0 -- 1 now

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fprintf(fout,"\n\n ConvertingVecs_ToBest_SeparableFeas': after 'Converting_All_Feas_To_ARange_0_1'");
	fflush(fout);

	//printf("\n\n Please press any key:"); getchar();

	nResf = SelectingBestFeas(
		fLargef, //const float fLargef,
		fepsf, //const  float fepsf,

		nDimf, //const int nDimf,
		nDimSelecMaxf, //const int nDimSelecMaxf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMaxf, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1
		///////////////////////////////////

		nDimSelecf, //int &nDimSelecf,

		fRatioBestMinf, //float &fRatioBestMinf,

		nPosOneFeaBestMaxf, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMaxf, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestMaxf, //float &fNumArr1stSeparBestMaxf,
		fNumArr2ndSeparBestMaxf, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArrf, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArrf); // int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

	fprintf(fout, "\n\n ConvertingVecs_ToBest_SeparableFeas': after 'SelectingBestFeas, nResf = %d'", nResf);
	fflush(fout);


	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		fprintf(fout_lr, "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar(); // exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	else if (nResf == SUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': the number of selected features  nDimSelecf = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax' results to get a larger number of the selected features; the range is (-2.0, -1.0) ");
		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':  the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);
		fprintf(fout_lr, "\n\n  Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//else if (nResf == SUCCESSFUL_RETURN)
	else if (nResf == 2)
	{
		printf("\n\n After 'SelectingBestFeas': the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);

		fprintf(fout_lr, "\n\n  Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//else if (nResf == 2)

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	printf("\n\n After 'SelectingBestFeas': fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, initial nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
	//printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_lr, "\n\n After 'SelectingBestFeas':fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, iniatal nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/*
	if (nDimSelecf > nDimSelecMax)
	{
		printf("\n\n  Changing nDimSelecf = %d to nDimSelecMax = %d", nDimSelecf, nDimSelecMax);

	#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nDimSelecf = nDimSelecMax;
	}//if (nDimSelec > nDimSelecMax)
	*/

	fprintf(fout, "\n\n //////////////////////////////////////////////////////////////////////");

	//for (int iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)
	{
		printf("\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
		//fprintf(fout_lr, "\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			//iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);

		fprintf(fout, "\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
	} //for (iFea = 0; iFea < nDimSelecMax; iFea++)


	//printf("\n\n Before 'Converting_Arr_To_Selec': please press any key"); fflush(fout); getchar();
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////
	fprintf(fout, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (normal)");
	fflush(fout);

	//float *fVecTrainSelec_1st = new float[nNumVecTrain_1st*nDimSelecf]; //0 -- 1 //[nDimSelecMaxf]
	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Normal, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr1stf, //const float fVecArr[],[nProdTrain_1st]
		fSelecArr1stf); //float fVecSelecArr[]) //[nNumVecTrain_1st*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////
	fprintf(fout, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (malignant)");
	fflush(fout);

	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Malignant, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr2ndf, //const float fVecArr[],[nProdTrain_2nd]
		fSelecArr2ndf); //float fVecSelecArr[]) //[nNumVecTrain_2nd*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n The end in 'ConvertingVecs_ToBest_SeparableFeas'");
	//printf("\n\n Please press any key"); getchar();

	delete[] fFeas_InitMinArrf;
	delete[] fFeas_InitMaxArrf;

	delete[] nFeaTheSameOrNotArrf;

	return SUCCESSFUL_RETURN;
} //int ConvertingVecs_ToBest_SeparableFeas(...
////////////////////////////////////////////////////////////////////////////////////////////

int	Converting_One_Fea_To_ARange_0_1(
	const int nDim1f,
	const int nDim2f,

	int &nFeaTheSameOrNotf,
	float &fFeaMinf,
	float &fFeaMaxf,

	float fOneDimArr1stf[], //[nDim1f]
	float fOneDimArr2ndf[]) //[nDim2f]
{

	int
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	//////////////////////////////////////

		//fprintf(fout, "\n\n 'Converting_One_Fea_To_ARange_0_1': nDim1f = %d, nDim2f = %d", nDim1f, nDim2f);
	nFeaTheSameOrNotf = 0; //not the same initially

	fFeaMinf = fLarge;
	fFeaMaxf = -fLarge;

	for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
	{
		fFeaCurf = fOneDimArr1stf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

//////////////////////////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
	{
		fFeaCurf = fOneDimArr2ndf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
////////////////////////////////////////////////////////////

	fRangeCurf = fFeaMaxf - fFeaMinf;

	if (fRangeCurf < feps)
	{
		nFeaTheSameOrNotf = 1;

		return (-2);
	} //if (fRangeCurf < feps)

/////////////////////////////////////
	if (nFeaTheSameOrNotf == 0)  //the feas are different 
	{
		//	fprintf(fout, "\n");
		for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
		{
			fOneDimArr1stf[iFeaf] = (fOneDimArr1stf[iFeaf] - fFeaMinf) / fRangeCurf;

			if (fOneDimArr1stf[iFeaf] < -feps || fOneDimArr1stf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr1stf[%d] = %E is out of the range", iFeaf, fOneDimArr1stf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr1stf[nIndexf] < -feps || fOneDimArr1stf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
////////////////////////////////////////////
		for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
		{
			fOneDimArr2ndf[iFeaf] = (fOneDimArr2ndf[iFeaf] - fFeaMinf) / fRangeCurf;


			if (fOneDimArr2ndf[iFeaf] < -feps || fOneDimArr2ndf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr2ndf[%d] = %E is out of the range", iFeaf, fOneDimArr2ndf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr2ndf[nIndexf] < -feps || fOneDimArr2ndf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

		fflush(fout);
	} //if (nFeaTheSameOrNotf == 0)  //the feas are different 

	return SUCCESSFUL_RETURN;
} // int Converting_One_Fea_To_ARange_0_1(...
//////////////////////////////////////////////////////////////

/*
nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot * nNumOfBins_OneImage_AllColors);

nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin];

//[nNumOfBins_AllColorsForAll_NorImages = nNumOfBins_OneImage_AllColors * nNumOfVecs_Normal
fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBins_OneImageArr[nIndexOfBin]) / (float)(nImageAreaCur);

*/
int Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
	const int nDimf, // == nNumOfBins_OneImage_AllColors

	const int nNumOfVecsTotf,
	const int nNumOfOneSelecFeaf,

	const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

	float fFeas_OneVec_Arrf[]) //[nNumOfVecsTotf]
{
	int
		nIndexf,
		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,

		iVecf;
////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneSelecFeaf = %d", 
		nDimf, nNumOfVecsTotf, nNumOfOneSelecFeaf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nIndexf = nNumOfOneSelecFeaf + (iVecf*nDimf);
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
			//	fprintf(fout, "\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nNumOfOneSelecFeaf = %d, iVecf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneSelecFeaf, iVecf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fFeas_OneVec_Arrf[iVecf] = fFeas_All_Arrf[nIndexf];
	} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs{...
///////////////////////////////////////////////////////

int Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
	const int nDimf,
	const int nNumOfVecsTotf,
	const int nNumOfOneSelecFeaf,

	const int nFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

	int nFeas_OneVec_Arrf[]) //[nNumOfVecsTotf]
{
	int
		nIndexf,
		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,

		iVecf;
	////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneSelecFeaf = %d",
		nDimf, nNumOfVecsTotf, nNumOfOneSelecFeaf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nIndexf = nNumOfOneSelecFeaf + (iVecf*nDimf);
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
			//	fprintf(fout, "\n 'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nNumOfOneSelecFeaf = %d, iVecf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneSelecFeaf, iVecf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nFeas_OneVec_Arrf[iVecf] = nFeas_All_Arrf[nIndexf];
	} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs{...
//////////////////////////////////////////////////////////////////

int SeparabilityOf_2_Color_Intervals(

	const int nBinSizeForColorSeparCurf, //== 'nBinSizeForColorSepar'

	const int nNumOfBins_OneImage_AllColorsf,
	////////////////////////////////////
	//for NonZero images
	const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

	const int nNumOf_NonZeroMalImagesMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

	///////////////////////////////

	const int nNumOfVecs_Norf,
	const int nNumOfVecs_Malf,
//////////////////////////////////////////////////////////
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Norf]
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malf]

	const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

	const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
///////////////////////////////////////////////////

	long &l_Seedf, //must be already specified
/////////////////////////////////////////////
	BOUNDARIES_OF_INTERVALS *sBoundaries_ForBestSeparf,

	float &fBestBinSeparabilityBy_2_Intervalsf)
{

/*
for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
{
	nAllBinsAll_Colors_NorImagesArr[iFea] = 0;

	fAllBinsAll_ColorsAver_NorImagesArr[iFea] = 0.0;
} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
//////////////////////////////

	for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
	{
		nIndexOfBin = iBin + nNumOfProcessedFiles_NormalTot * nNumOfBins_OneImage_AllColors;

		nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin];

		fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBins_OneImageArr[nIndexOfBin]) / (float)(nImageAreaCur);
	} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
*/

	void InitOfFeasOfWholeRegionAndLocalRadii(
		const int nNumOfFeasf,

		const int nLocalRadiusOfWholeRegionf,
		const int nLocalRadiusOfNeighf,

		int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		int nLocalRadiusOfFeaNeighArrf[]); //[nNumOfFeasf]

	int OptByCuckSear_ColorSeparability(
		const int nLargef,
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

		const int nNumOfItersForCuckSearf,

		const int nNumOfRandItersforRestartf,
		const int nNumOfRandItersforLocalPerturbf,
		const int nNumOfRandInitializingLoopsMaxf,
		////////////////////////////////////////////////////
		//for Func_BySeparabilityOf_2_ColorIntervals
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

///////////////////////////////////////////
			long &l_Seedf, //must be already specified

			//const int nFeaInitArrf[],   //[nNumOfFeasf]
			/////////////////////////////////////////////////////////////
			float &fFuncValueFinalBestf,
			int nFeaFinalBestArrf[]);   //[nNumOfFeasf]

	int Copying_Adjusting_OneDimFeaVec_To_Intervals(
		const int nNumOfFeasf,

		int nOneDimArrf[], //[nNumOfFeasf]

		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

//////////////////////////////
	int
		nResf,

		nFeaByRatioTheSameOrNotf, //int &nFeaTheSameOrNotf,
		nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		nSeparDiffDirectionBest_HiRef, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBest_HiRef, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		nRedBinf,
		nGreenBinf,
		nBlueBinf,

		iVecf,

		nNumOfNonZeroBinsIn_NorImagesf,
		nNumOfNonZeroBinsIn_MalImagesf,

		nSumOfPixelsIn_NonZeroNorImagesf,
		nSumOfPixelsIn_NonZeroMalImagesf,
//////////////////////////////////////////

		nNumOfPixelsIn_2_ColorIntervals_OneImagef,

		nNumOfProcessedFiles_NormalTotf,
		nNumOfProcessedFiles_MalignantTotf,
////////////////////////
		nNumOfPixelsIn_ColorIntervals_Normal_Images_Arrf[nNumOfVecs_Normal], //[]
		nNumOfPixelsIn_ColorIntervals_Malignant_Images_Arrf[nNumOfVecs_Malignant], //[nNumOfVecs_Malignant]

		nBinsNor_OneDimf[nNumOfVecs_Normal],
		nBinsMal_OneDimf[nNumOfVecs_Malignant],

//////////////////////////////////////
			nFeaWholeRegionMinArrf[nNumOfFeas], //[nNumOfFeasf]
			nFeaWholeRegionMaxArrf[nNumOfFeas], //[nNumOfFeasf]

			nLocalRadiusOfFeaNeighArrf[nNumOfFeas], //[nNumOfFeasf]

		nFeaFinalBestArrf[nNumOfFeas],
		iBinOfAllColorsf;

	float
		fFeaByRatioMinf, //float &fFeaMinf,
		fFeaByRatioMaxf, //float &fFeaMaxf,
///////////////////////////////////////
		fFuncValueFinalBestf,

		fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaf,

		fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestf,

		fNumArr1stSeparBestf, //float &fNumArr1stSeparBestf,
		fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestf,

		fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestf,
		fRatioOfSeparOfArr1stAndArr2ndBestf, // float &fRat

		fRatioBest_Minf = fLarge,
///////////////////////////////////////////

		fRatioOfSumsOfPixelsOfNonZero_MalToNorf,
		////////////////////////////////////////////////

		fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf,
		fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf = -fLarge,

		fNumOfPixelsIn_ColorIntervals_Normal_Images_Normalized_Arrf[nNumOfVecs_Normal], //[nNumOfVecs_Normal]
		fNumOfPixelsIn_ColorIntervals_Malignant_Images_Normalized_Arrf[nNumOfVecs_Malignant], //[nNumOfVecs_Malignant]

		fBinsNor_OneDimf[nNumOfVecs_Normal],
		fBinsMal_OneDimf[nNumOfVecs_Malignant];
////////////////////////////////////////

	BOUNDARIES_OF_INTERVALS sBoundaries_Of_Intervalsf;
/////////////////////////////////////
	InitOfFeasOfWholeRegionAndLocalRadii(
		nNumOfFeas, //const int nNumOfFeasf,

		nLocalRadiusOfWholeRegion, //const int nLocalRadiusOfWholeRegionf,
		nLocalRadiusOfNeigh, //const int nLocalRadiusOfNeighf,

		nFeaWholeRegionMinArrf, //int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		nFeaWholeRegionMaxArrf, //int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		nLocalRadiusOfFeaNeighArrf); // int nLocalRadiusOfFeaNeighArrf[]) //[nNumOfFeasf]

	nResf = OptByCuckSear_ColorSeparability(
		nLarge, //const int nLargef,

		nNumOfFeas, //const int nNumOfFeasf,
		nNumOfSelecCombins, //const int nNumOfSelecCombinsf,

		nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

		nNumOfItersForCuckSear, //const int nNumOfItersForCuckSearf,

		nNumOfRandItersforRestart, //const int nNumOfRandItersforRestartf,
		nNumOfRandItersforLocalPerturb, //const int nNumOfRandItersforLocalPerturbf,
		nNumOfRandInitializingLoopsMax, //const int nNumOfRandInitializingLoopsMaxf,
		////////////////////////////////////////////////////
		//for Func_BySeparabilityOf_2_ColorIntervals
		nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

///////////////////////////////////////////
		l_Seedf, //long &l_Seedf, //must be already specified

			//const int nFeaInitArrf[],   //[nNumOfFeasf]
			/////////////////////////////////////////////////////////////
			fFuncValueFinalBestf, //float &fFuncValueFinalBestf,

			nFeaFinalBestArrf); // int nFeaFinalBestArrf[]);   //[nNumOfFeasf]
/////////////////////////////////////////
	fBestBinSeparabilityBy_2_Intervalsf = fFuncValueFinalBestf;
	//BOUNDARIES_OF_INTERVALS *sBoundaries_ForBestSeparf,

	nResf = Copying_Adjusting_OneDimFeaVec_To_Intervals(
		nNumOfFeas, //const int nNumOfFeasf,

		nFeaFinalBestArrf, //int nOneDimArrf[], //[nNumOfFeasf]

		sBoundaries_ForBestSeparf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	return SUCCESSFUL_RETURN;
}// int SeparabilityOf_2_Color_Intervals(...
/////////////////////////////////////////////

int Converting_nBinOfAllColorsf_To_SeparateColors(
	const int nBinOfAllColorsf,
	int &nRedBinf,
	int &nGreenBinf,
	int &nBlueBinf)
{
/*
			nIndexOfBinCurf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) +( nBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));

*/
	int
		nIndexOfBin_Verifiedf,

		nResidualf;

	if (nBinOfAllColorsf < 0 || nBinOfAllColorsf >= nNumOfBins_OneImage_AllColors)
	{
		printf("\n\n An error in 'Converting_nBinOfAllColorsf_To_SeparateColors': nBinOfAllColorsf = %d >= nNumOfBins_OneImage_AllColors = %d",
			nBinOfAllColorsf, nNumOfBins_OneImage_AllColors);
		printf("\n\n Please press any key to exit");			getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nBinOfAllColorsf < 0 || nBinOfAllColorsf >= nNumOfBins_OneImage_AllColors)

	nBlueBinf = nBinOfAllColorsf / (nNumOfBins_OneColor*nNumOfBins_OneColor); // nSquareOfNumOfBins_OneColor;

	nResidualf = nBinOfAllColorsf - (nBlueBinf*nNumOfBins_OneColor*nNumOfBins_OneColor); //nSquareOfNumOfBins_OneColor);

	nGreenBinf = nResidualf / nNumOfBins_OneColor;

	nRedBinf = nResidualf - (nGreenBinf*nNumOfBins_OneColor);

	nIndexOfBin_Verifiedf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) + (nBlueBinf * nNumOfBins_OneColor*nNumOfBins_OneColor); //nSquareOfNumOfBins_OneColor);

	if (nIndexOfBin_Verifiedf != nBinOfAllColorsf)
	{
		printf("\n\n An error in 'Converting_nBinOfAllColorsf_To_SeparateColors': nBinOfAllColorsf = %d != nIndexOfBin_Verifiedf = %d",
			nBinOfAllColorsf, nIndexOfBin_Verifiedf);
		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nIndexOfBin_Verifiedf != nBinOfAllColorsf)

	return SUCCESSFUL_RETURN;
} //int Converting_nBinOfAllColorsf_To_SeparateColors(...
////////////////////////////////////////////////

void NextLeftBoundary_OneColor(
	const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

	int nValidIntervalsArrf[], //nNumOfColorIntervals
	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int &nIntervalOfNextLeftBoundaryf,

	int &nPositOfNextLeftBoundaryf)
{
	int
		nPositMinCurf,
		iIntervalf;
/////////////////////////////////////
	nPositOfNextLeftBoundaryf = nLarge;
	nIntervalOfNextLeftBoundaryf = -1;

	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		if (nValidIntervalsArrf[iIntervalf] == 0)
			continue;

		if (nColorf == 0) //Red
		{
			nPositMinCurf = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf];

			if (nPositMinCurf < nPositOfNextLeftBoundaryf)
			{
				nPositOfNextLeftBoundaryf = nPositMinCurf;
				nIntervalOfNextLeftBoundaryf = iIntervalf;
			} //

		} //if (nColorf == 0) //Red
		else if (nColorf == 1) //Green
		{
			nPositMinCurf = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf];

			if (nPositMinCurf < nPositOfNextLeftBoundaryf)
			{
				nPositOfNextLeftBoundaryf = nPositMinCurf;
				nIntervalOfNextLeftBoundaryf = iIntervalf;

			} //
		} //if (nColorf == 1) //Green
		else if (nColorf == 2) //Blue
		{
			nPositMinCurf = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf];

			if (nPositMinCurf < nPositOfNextLeftBoundaryf)
			{
				nPositOfNextLeftBoundaryf = nPositMinCurf;
				nIntervalOfNextLeftBoundaryf = iIntervalf;
			} //
		} //if (nColorf == 2) //Blue

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
///////////////////////////////////////

	if (nIntervalOfNextLeftBoundaryf >= 0)
	{
//the left interval has been found
	//	nValidIntervalsArrf[nIntervalOfNextLeftBoundaryf] = 0;

#ifdef PRINTING_THE_DETAILS
		//fprintf(fout, "\n\n 'NextLeftBoundary_OneColor': nColorf = %d, nPositOfNextLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d, nValidIntervalsArrf[%d] == 0",
			//nColorf, nPositOfNextLeftBoundaryf, nIntervalOfNextLeftBoundaryf, nIntervalOfNextLeftBoundaryf);
		fprintf(fout, "\n\n 'NextLeftBoundary_OneColor': nColorf = %d, nPositOfNextLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d",
			nColorf, nPositOfNextLeftBoundaryf, nIntervalOfNextLeftBoundaryf, nIntervalOfNextLeftBoundaryf);
#endif //#ifdef PRINTING_THE_DETAILS
	} //if (nIntervalOfNextLeftBoundaryf >= 0)

}//void NextLeftBoundary_OneColor(
///////////////////////////////////////////////////

int NextRightBoundary_OneColor(
	const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

	const int nPositOfLeftBoundaryf,

	const int nIntervalOfNextLeftBoundaryf,

	int nValidIntervalsArrf[], //nNumOfColorIntervals
	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int &nIntervalOfNextRightBoundaryf,
	int &nPositOfNextRightBoundaryf)
{
	int
		nPositOfNextRightBoundaryInitf,
		nPositOfNextRightBoundaryPrevf = -1,
		nPositMinCurf,
		nPositMaxCurf,

		nRightBoundaryFoundf = 0, //not yet
		
		iIterForNextRightBoundaryf,
		iIntervalf;
/////////////////////////////////
	if (nPositOfLeftBoundaryf < 0 || nPositOfLeftBoundaryf >= nNumOfBins_OneColor)
	{
		printf("\n\n An error in 'NextRightBoundary_OneColor': nPositOfLeftBoundaryf = %d, nNumOfBins_OneColor = %d",
			nPositOfLeftBoundaryf, nNumOfBins_OneColor);

		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nPositOfLeftBoundaryf < 0 || nPositOfLeftBoundaryf >= nNumOfBins_OneColor)
////////////////////////////
	if (nIntervalOfNextLeftBoundaryf < 0 || nIntervalOfNextLeftBoundaryf >= nNumOfColorIntervals)
	{
		printf("\n\n An error in 'NextRightBoundary_OneColor': nIntervalOfNextLeftBoundaryf = %d, nNumOfColorIntervals = %d",
			nIntervalOfNextLeftBoundaryf, nNumOfColorIntervals);

		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nIntervalOfNextLeftBoundaryf < 0 || nIntervalOfNextLeftBoundaryf >= nNumOfColorIntervals)
///////////////////////////////////////

	if (nColorf == 0) //Red
	{
		nPositOfNextRightBoundaryInitf = sBoundaries_Of_Intervalsf->nRedIntervals_High[nIntervalOfNextLeftBoundaryf];
	} //if (nColorf == 0) //Red
	else if (nColorf == 1) //Green
	{
		nPositOfNextRightBoundaryInitf = sBoundaries_Of_Intervalsf->nGreenIntervals_High[nIntervalOfNextLeftBoundaryf];
	} //if (nColorf == 1) //Green
	else if (nColorf == 2) //Blue
	{
		nPositOfNextRightBoundaryInitf = sBoundaries_Of_Intervalsf->nBlueIntervals_High[nIntervalOfNextLeftBoundaryf];
	} //if (nColorf == 2) //Blue

	nPositOfNextRightBoundaryf = nPositOfNextRightBoundaryInitf;
	nIntervalOfNextRightBoundaryf = nIntervalOfNextLeftBoundaryf;

#ifdef PRINTING_THE_DETAILS
	fprintf(fout, "\n\n 'NextRightBoundary_OneColor': nColorf = %d, nPositOfNextRightBoundaryf == nPositOfNextRightBoundaryInitf = %d, nIntervalOfNextRightBoundaryf == nIntervalOfNextLeftBoundaryf = %d",
		 nColorf, nPositOfNextRightBoundaryInitf, nIntervalOfNextLeftBoundaryf, iIntervalf);
#endif //#ifdef PRINTING_THE_DETAILS
	   	///////////////////////////

/*
//printing 
	/////////////////////////
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
	//	if (nValidIntervalsArrf[iIntervalf] == 0)
		//	continue;

		if (nColorf == 0) //Red
		{
			nPositMinCurf = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf];

			nPositMaxCurf = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf];

#ifdef PRINTING_THE_DETAILS
			fprintf(fout, "\n\n 'NextRightBoundary_OneColor': iIntervalf = %d, nValidIntervalsArrf[%d] = %d", iIntervalf, iIntervalf,nValidIntervalsArrf[iIntervalf]);

			fprintf(fout, "\n sBoundaries_Of_Intervalsf->nRedIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nRedIntervals_High[%d] = %d",
				iIntervalf,sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf], iIntervalf,sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf]);
#endif //#ifdef PRINTING_THE_DETAILS

		} //if (nColorf == 0) //Red
	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
*/
////////////////////////////
	for (iIterForNextRightBoundaryf = 0; iIterForNextRightBoundaryf <= nNumOfColorIntervals; iIterForNextRightBoundaryf++)
	{
		for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
		{
			if (nValidIntervalsArrf[iIntervalf] == 0)
			{
#ifdef PRINTING_THE_DETAILS
				fprintf(fout, "\n\n 'NextRightBoundary_OneColor': skipping iIntervalf = %d, nColorf = %d, nPositOfLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d, nValidIntervalsArrf[%d] == 0",
					iIntervalf, nColorf, nPositOfLeftBoundaryf, nIntervalOfNextLeftBoundaryf, iIntervalf);
#endif //#ifdef PRINTING_THE_DETAILS

				continue;
			} //if (nValidIntervalsArrf[iIntervalf] == 0)

			if (nColorf == 0) //Red
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf];
			} //if (nColorf == 0) //Red
			else if (nColorf == 1) //Green
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf];
			} //if (nColorf == 1) //Green
			else if (nColorf == 2) //Blue
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf];
			} //if (nColorf == 2) //Blue
	//////////////////////////

#ifdef PRINTING_THE_DETAILS
			fprintf(fout, "\n\n 'NextRightBoundary_OneColor': iIterForNextRightBoundaryf = %d, iIntervalf = %d, nColorf = %d, nPositMinCurf = %d, nPositMaxCurf = %d, nPositOfLeftBoundaryf = %d",
				iIterForNextRightBoundaryf,iIntervalf, nColorf, nPositMinCurf, nPositMaxCurf, nPositOfLeftBoundaryf);
#endif //#ifdef PRINTING_THE_DETAILS

			if (nPositMaxCurf >= nPositOfLeftBoundaryf && nPositMaxCurf >= nPositOfNextRightBoundaryf & nPositMinCurf <= nPositOfNextRightBoundaryf)
			{
				nPositOfNextRightBoundaryf = nPositMaxCurf;
				nIntervalOfNextRightBoundaryf = iIntervalf;

				nRightBoundaryFoundf = 1;

#ifdef PRINTING_THE_DETAILS
				fprintf(fout, "\n 'NextRightBoundary_OneColor': iIntervalf = %d, nColorf = %d, a new nPositOfNextRightBoundaryf = %d, nPositOfLeftBoundaryf = %d, a new nIntervalOfNextRightBoundaryf = %d",
					iIntervalf, nColorf, nPositOfNextRightBoundaryf, nPositOfLeftBoundaryf, nIntervalOfNextRightBoundaryf);
#endif //#ifdef PRINTING_THE_DETAILS

			} //if (nPositMaxCurf >= nPositOfLeftBoundaryf && nPositMaxCurf >= nPositOfNextRightBoundaryf & nPositMinCurf <= nPositOfNextRightBoundaryf)

		} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
		//////////////////////////////////////////////

	/*
		if (nRightBoundaryFoundf == 0)
		{
	//the right boundary has not been found
			printf("\n\n An error in 'NextRightBoundary_OneColor': nRightBoundaryFoundf == 0, nColorf = %d, nPositOfLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d, nNumOfColorIntervals = %d",
				nColorf, nPositOfLeftBoundaryf, nIntervalOfNextLeftBoundaryf, nNumOfColorIntervals);

			fprintf(fout,"\n\n An error in 'NextRightBoundary_OneColor': nRightBoundaryFoundf == 0, nColorf = %d, nPositOfLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d, nNumOfColorIntervals = %d",
				nColorf, nPositOfLeftBoundaryf, nIntervalOfNextLeftBoundaryf, nNumOfColorIntervals);

			printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nRightBoundaryFoundf == 0)
	*/
		nValidIntervalsArrf[nIntervalOfNextRightBoundaryf] = 0;
#ifdef PRINTING_THE_DETAILS
		fprintf(fout, "\n\n 'NextRightBoundary_OneColor': nColorf = %d, a new nValidIntervalsArrf[%d] == 0",
			nColorf, nIntervalOfNextRightBoundaryf);
#endif //#ifdef PRINTING_THE_DETAILS

		//////////////////////////////////////////

		//adjusting 'nValidIntervalsArrf[]'
		for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
		{
			if (nValidIntervalsArrf[iIntervalf] == 0)
				continue;

			if (nColorf == 0) //Red
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf];
			} //if (nColorf == 0) //Red
			else if (nColorf == 1) //Green
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf];
			} //if (nColorf == 1) //Green
			else if (nColorf == 2) //Blue
			{
				nPositMinCurf = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf];

				nPositMaxCurf = sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf];
			} //if (nColorf == 2) //Blue
	//////////////////////////

			if (nPositMaxCurf >= nPositOfLeftBoundaryf && nPositMaxCurf <= nPositOfNextRightBoundaryf & nPositMinCurf <= nPositOfNextRightBoundaryf)
			{
				//nPositOfNextRightBoundaryf = nPositMaxCurf;
				nValidIntervalsArrf[iIntervalf] = 0;
			} //if (nPositMaxCurf >= nPositOfLeftBoundaryf && nPositMaxCurf >= nPositOfNextRightBoundaryf & nPositMinCurf <= nPositOfNextRightBoundaryf)

		} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
////////////////////////////////////

		if (nPositOfNextRightBoundaryf > nPositOfNextRightBoundaryPrevf)
		{
			nPositOfNextRightBoundaryPrevf = nPositOfNextRightBoundaryf;
		} //if (nPositOfNextRightBoundaryf > nPositOfNextRightBoundaryPrevf)
		else if (nPositOfNextRightBoundaryPrevf == nPositOfNextRightBoundaryf)
		{
	//there is no new right boundary
			break;
		} //else if (nPositOfNextRightBoundaryPrevf == nPositOfNextRightBoundaryf)
		else if (nPositOfNextRightBoundaryPrevf > nPositOfNextRightBoundaryf)
		{
			printf("\n\n An error in 'NextRightBoundary_OneColor': nPositOfNextRightBoundaryf = %d > nPositOfNextRightBoundaryPrevf = %d, iIterForNextRightBoundaryf = %d, nNumOfColorIntervals = %d",
				nPositOfNextRightBoundaryf,nPositOfNextRightBoundaryPrevf, iIterForNextRightBoundaryf, nNumOfColorIntervals);

			printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //

	} //for (iIterForNextRightBoundaryf = 0; iIterForNextRightBoundaryf <= nNumOfColorIntervals; iIterForNextRightBoundaryf++)
//////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
}//int NextRightBoundary_OneColor(...
/////////////////////////////////////////////////////////////

int NonOverlapping_OneColorIntervals(

	const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue
	BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int &nNumOf_NonOverlapping_OneColorIntervalsf)
{
	void NextLeftBoundary_OneColor(
			const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

			int nValidIntervalsArrf[], //nNumOfColorIntervals
			const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			int &nIntervalOfNextLeftBoundaryf,

			int &nPositOfNextLeftBoundaryf);

	int NextRightBoundary_OneColor(
		const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

		const int nPositOfLeftBoundaryf,

		const int nIntervalOfNextLeftBoundaryf,

		int nValidIntervalsArrf[], //nNumOfColorIntervals
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		int &nIntervalOfNextRightBoundaryf,
		int &nPositOfNextRightBoundaryf);

//////////////////////////////
	int
		nResf,
		nPositOfNextLeftBoundaryf,
		nPositOfNextRightBoundaryf, 

		nIntervalOfNextLeftBoundaryf,
		nIntervalOfNextRightBoundaryf,
		nValidIntervalsArrf[nNumOfColorIntervals],

		iLeftBoundaryf,
		iIntervalf;
//////////////////
	nNumOf_NonOverlapping_OneColorIntervalsf = 0;

	if (nColorf == 0) //Red
	{
		sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = 0;
	} //if (nColorf == 0) //Red
	else if (nColorf == 1) //Green
	{
		sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = 0;
	} //if (nColorf == 1) //Green
	else if (nColorf == 2) //Blue
	{
		sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = 0;
	} //if (nColorf == 2) //Blue

//////////////////////
//all intervals are assumed to be NonOverlapping initially
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		nValidIntervalsArrf[iIntervalf] = 1;

		if (nColorf == 0) //Red
		{
			sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iIntervalf] = -1;
			sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iIntervalf] = -1;
		} //if (nColorf == 0) //Red
		else if (nColorf == 1) //Green
		{
			sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iIntervalf] = -1;
			sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iIntervalf] = -1;
		} //if (nColorf == 1) //Green
		else if (nColorf == 2) //Blue
		{
			sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iIntervalf] = -1;
			sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iIntervalf] = -1;
		} //if (nColorf == 2) //Blue

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
/////////////////////////////////////////////////

	for (iLeftBoundaryf = 0; iLeftBoundaryf < nNumOfColorIntervals; iLeftBoundaryf++)
	{
		//finding the next lowest boundary
		NextLeftBoundary_OneColor(
			nColorf, //const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

			nValidIntervalsArrf, // int nValidIntervalsArrf[], //nNumOfColorIntervals
			sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nIntervalOfNextLeftBoundaryf, //int &nIntervalOfNextLeftBoundaryf,
			nPositOfNextLeftBoundaryf); // int &nPositOfNextLeftBoundaryf);

		if (nPositOfNextLeftBoundaryf == nLarge)
		{
//can not find a left boundary
			if (nNumOf_NonOverlapping_OneColorIntervalsf == 0 || nNumOf_NonOverlapping_OneColorIntervalsf > nNumOfColorIntervals)
			{
				printf("\n\n An error in 'NonOverlapping_OneColorIntervals': nNumOf_NonOverlapping_OneColorIntervalsf = %d, nNumOfColorIntervals = %d, iLeftBoundaryf = %d",
					nNumOf_NonOverlapping_OneColorIntervalsf, nNumOfColorIntervals, iLeftBoundaryf);
				printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (nNumOf_NonOverlapping_OneColorIntervalsf == 0 || nNumOf_NonOverlapping_OneColorIntervalsf > nNumOfColorIntervals)

			return SUCCESSFUL_RETURN;
		} //if (nPositOfNextLeftBoundaryf == nLarge)
///////////////////////////////////////////////////

		nResf = NextRightBoundary_OneColor(
			nColorf, //const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue

			nPositOfNextLeftBoundaryf, //const int nPositOfLeftBoundaryf,

			nIntervalOfNextLeftBoundaryf, //const int nIntervalOfNextLeftBoundaryf,

			nValidIntervalsArrf, //int nValidIntervalsArrf[], //nNumOfColorIntervals
			sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nIntervalOfNextRightBoundaryf, //int &nIntervalOfNextRightBoundaryf,

			nPositOfNextRightBoundaryf); // int &nPositOfNextRightBoundaryf);

		if (nResf == SUCCESSFUL_RETURN)
		{
			if (nPositOfNextRightBoundaryf < nPositOfNextLeftBoundaryf || nPositOfNextRightBoundaryf < 0 || nPositOfNextRightBoundaryf > nNumOfBins_OneColor - 1)
			{
				printf("\n\n An error in 'NonOverlapping_OneColorIntervals': nPositOfNextRightBoundaryf = %d < nPositOfNextLeftBoundaryf = %d, iLeftBoundaryf = %d",
					nPositOfNextRightBoundaryf, nPositOfNextLeftBoundaryf, iLeftBoundaryf);
				printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (nPositOfNextRightBoundaryf < nPositOfNextLeftBoundaryf || nPositOfNextRightBoundaryf < 0 || nPositOfNextRightBoundaryf > nNumOfBins_OneColor - 1)

//the next nonoverlapping color interval has been found
			nNumOf_NonOverlapping_OneColorIntervalsf += 1;

			if (nColorf == 0) //Red
			{
				sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = nNumOf_NonOverlapping_OneColorIntervalsf;

				sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[nNumOf_NonOverlapping_OneColorIntervalsf - 1] = nPositOfNextLeftBoundaryf;
				sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[nNumOf_NonOverlapping_OneColorIntervalsf  - 1] = nPositOfNextRightBoundaryf;

			} //if (nColorf == 0) //Red
			else if (nColorf == 1) //Green
			{
				sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = nNumOf_NonOverlapping_OneColorIntervalsf;

				sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[nNumOf_NonOverlapping_OneColorIntervalsf - 1] = nPositOfNextLeftBoundaryf;
				sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[nNumOf_NonOverlapping_OneColorIntervalsf - 1] = nPositOfNextRightBoundaryf;
			} //if (nColorf == 1) //Green
			else if (nColorf == 2) //Blue
			{
				sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = nNumOf_NonOverlapping_OneColorIntervalsf;

				sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[nNumOf_NonOverlapping_OneColorIntervalsf - 1] = nPositOfNextLeftBoundaryf;
				sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[nNumOf_NonOverlapping_OneColorIntervalsf - 1] = nPositOfNextRightBoundaryf;
			} //if (nColorf == 2) //Blue		} //if (nResf == SUCCESSFUL_RETURN)
		} //if (nResf == SUCCESSFUL_RETURN)
		else
		{
			printf("\n\n An error in 'NonOverlapping_OneColorIntervals' by 'NextRightBoundary_OneColor': a right boundary has not been found for an existing left boundary");
			printf("\n nPositOfNextLeftBoundaryf = %d, iLeftBoundaryf = %d, nIntervalOfNextLeftBoundaryf = %d",
				nPositOfNextLeftBoundaryf, iLeftBoundaryf, nIntervalOfNextLeftBoundaryf);

			printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //else

	} //for (iLeftBoundaryf = 0; iLeftBoundaryf < nNumOfColorIntervals; iLeftBoundaryf++)

	return SUCCESSFUL_RETURN;
}//int NonOverlapping_OneColorIntervals(..


/////////////////////////////////////////////////////

int NonOverlapping_3ColorIntervals(
	BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
{
	int NonOverlapping_OneColorIntervals(

		const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue
		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		int &nNumOf_NonOverlapping_OneColorIntervalsf);
		///////////////////
int
		iColorf,
		nNumOf_NonOverlapping_OneColorIntervalsf,
		nResf;

for (iColorf = 0; iColorf < 3; iColorf++)
{
	nResf = NonOverlapping_OneColorIntervals(

		iColorf, //const int nColorf, // 0-- Red, 1-- Green, 2 -- Blue
		sBoundaries_Of_Intervalsf, //BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		nNumOf_NonOverlapping_OneColorIntervalsf); // int &nNumOf_NonOverlapping_OneColorIntervalsf);
	///////////////////
} //for (iColorf = 0; iColorf < 3; iColorf++)


	return SUCCESSFUL_RETURN;
} //int NonOverlapping_3ColorIntervals(...

///////////////////////////////////////////////////
int NumOfPixelsIn_2_ColorIntervals_OneImage(
	const int nClassf, //1--normal, 0 -- abnormal
	const int nNumOfVecsTotf,
	const int nAreasOfAllImagesArrf[], //nNumOfVecsTotf,

	const int nPositOfVecf, 
	//const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_ImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

	float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf) //
{
	int Printing_Boundaries_Of_Intervals(
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

//nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;
	int
		nResf,
		nIndexOfBinCurf,
		nIndexOfBin_AllVecsf,

		iRedBinf,
		iGreenBinf,
		iBlueBinf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nAreaf = nAreasOfAllImagesArrf[nPositOfVecf],

		nRedMinArrf[nNumOfColorIntervals],
		nRedMaxArrf[nNumOfColorIntervals],

		nGreenMinArrf[nNumOfColorIntervals],
		nGreenMaxArrf[nNumOfColorIntervals],

		nBlueMinArrf[nNumOfColorIntervals],
		nBlueMaxArrf[nNumOfColorIntervals],

		iIntervf,
		iIntervRedf,
		iIntervGreenf,
		iIntervBluef;
	////////////////////////////////////////////////

	if (nClassf == 1)
	{
		if (nNumOfVecsTotf != nNumOfVecs_Normal || nPositOfVecf < 0 || nPositOfVecf >= nNumOfVecs_Normal || nAreaf <= 0 || nAreaf > nLarge)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfVecsTotf = %d != nNumOfVecs_Normal = %d || nPositOfVecf = %d, nAreaf = %d",
				nNumOfVecsTotf, nNumOfVecs_Normal, nPositOfVecf, nAreaf);

			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfVecsTotf = %d != nNumOfVecs_Normal = %d || nPositOfVecf = %d, nAreaf = %d",
				nNumOfVecsTotf, nNumOfVecs_Normal, nPositOfVecf, nAreaf);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nNumOfVecsTotf != nNumOfVecs_Normal || nPositOfVecf < 0 || nPositOfVecf >= nNumOfVecs_Normal)
	}//if (nClassf == 1)
	else if (nClassf == 0)
	{
		if (nNumOfVecsTotf != nNumOfVecs_Malignant || nPositOfVecf < 0 || nPositOfVecf >= nNumOfVecs_Malignant || nAreaf <= 0 || nAreaf > nLarge)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfVecsTotf = %d != nNumOfVecs_Malignant = %d || nPositOfVecf = %d",
				nNumOfVecsTotf, nNumOfVecs_Malignant, nPositOfVecf);

			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfVecsTotf = %d != nNumOfVecs_Malignant = %d || nPositOfVecf = %d",
				nNumOfVecsTotf, nNumOfVecs_Malignant, nPositOfVecf);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nNumOfVecsTotf != nNumOfVecs_Malignant || nPositOfVecf < 0 || nPositOfVecf >= nNumOfVecs_Malignant || nAreaf <= 0 || nAreaf > nLarge)

	} // else if (nClassf == 0)
	else
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nClassf = %d, nPositOfVecf = %d",	nClassf, nPositOfVecf);
		fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nClassf = %d, nPositOfVecf = %d", nClassf, nPositOfVecf);

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //
	/////////////////////////////////////////

#ifdef PRINTING_THE_DETAILS
	fprintf(fout, "\n\n Beginning of 'NumOfPixelsIn_2_ColorIntervals_OneImage': iIterForCuckSear_Glob = %d, nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob = %d",
		iIterForCuckSear_Glob, nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob);
	fprintf(fout, "\n nClassf = %d, nPositOfVecf = %d, nAreaf = %d", nClassf, nPositOfVecf, nAreaf);

	nResf = Printing_Boundaries_Of_Intervals(
		sBoundaries_Of_Intervalsf); // const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

#endif //#ifdef PRINTING_THE_DETAILS
/////////////////////////////////////////////////////////
//initialiazation 
	for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)
	{
		nRedMinArrf[iIntervf] = -1;
		nRedMaxArrf[iIntervf] = -1;

		nGreenMinArrf[iIntervf] = -1;
		nGreenMaxArrf[iIntervf] = -1;

		nBlueMinArrf[iIntervf] = -1;
		nBlueMaxArrf[iIntervf] = -1;
	} //for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)
///////////////////////////////////////////////////
	nNumOfPixelsIn_2_ColorIntervals_OneImagef = 0;
	fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf = 0.0;

#ifdef PRINTING_THE_DETAILS
	fprintf(fout, "\n\n Red: sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals);
#endif //#ifdef PRINTING_THE_DETAILS
	/////////////////////////////

	if (sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals > nNumOfColorIntervals)
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d", 
			sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals, nNumOfColorIntervals);

		fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d",
			sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals, nNumOfColorIntervals);

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals > nNumOfColorIntervals)

///////////////////////////////////////////////
	for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iIntervf++)
	{
/*
		nRedMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervf];
		nRedMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervf];
*/
		nRedMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iIntervf];
		nRedMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iIntervf];

#ifdef PRINTING_THE_DETAILS
		fprintf(fout, "\n nRedMinArrf[%d] = %d, nRedMaxArrf[%d] = %d",
			iIntervf,nRedMinArrf[iIntervf], iIntervf,nRedMaxArrf[iIntervf]);
#endif //#ifdef PRINTING_THE_DETAILS
		
		if (nRedMinArrf[iIntervf] < 0 || nRedMinArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMinArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nRedMinArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMinArrf[%d] = %d, nNumOfBins_OneColor = %d", 
				iIntervf, nRedMinArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nRedMinArrf[iIntervf] < 0 || nRedMinArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nRedMaxArrf[iIntervf] < 0 || nRedMaxArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMaxArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nRedMaxArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMaxArrf[%d] = %d, nNumOfBins_OneColor = %d",
				iIntervf, nRedMaxArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nRedMaxArrf[iIntervf] < 0 || nRedMaxArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nRedMinArrf[iIntervf] > nRedMaxArrf[iIntervf])
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMinArrf[%d] = %d > nRedMaxArrf[iIntervf] = %d",
				iIntervf, nRedMinArrf[iIntervf], nRedMaxArrf[iIntervf]);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nRedMinArrf[%d] = %d > nRedMaxArrf[iIntervf] = %d",
				iIntervf, nRedMinArrf[iIntervf], nRedMaxArrf[iIntervf]);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nRedMinArrf[iIntervf] > nRedMaxArrf[iIntervf])

	} //for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iIntervf++)
/////////////////////////////////////////////////////////////

#ifdef PRINTING_THE_DETAILS
	fprintf(fout, "\n\n Green: sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals);
#endif //#ifdef PRINTING_THE_DETAILS

	if (sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals > nNumOfColorIntervals)
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d",
			sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals, nNumOfColorIntervals);

		fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d",
			sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals, nNumOfColorIntervals);

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals > nNumOfColorIntervals)

	for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iIntervf++)
	{
		/*
				nGreenMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervf];
				nGreenMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervf];
		*/

		nGreenMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iIntervf];
		nGreenMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iIntervf];

#ifdef PRINTING_THE_DETAILS
		fprintf(fout, "\n nGreenMinArrf[%d] = %d, nGreenMaxArrf[%d] = %d",
			iIntervf, nGreenMinArrf[iIntervf], iIntervf, nGreenMaxArrf[iIntervf]);
#endif //#ifdef PRINTING_THE_DETAILS

		if (nGreenMinArrf[iIntervf] < 0 || nGreenMinArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMinArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nGreenMinArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMinArrf[%d] = %d, nNumOfBins_OneColor = %d",
				iIntervf, nGreenMinArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nGreenMinArrf[iIntervf] < 0 || nGreenMinArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nGreenMaxArrf[iIntervf] < 0 || nGreenMaxArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMaxArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nGreenMaxArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMaxArrf[%d] = %d, nNumOfBins_OneColor = %d",
				iIntervf, nGreenMaxArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nGreenMaxArrf[iIntervf] < 0 || nGreenMaxArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nGreenMinArrf[iIntervf] > nGreenMaxArrf[iIntervf])
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMinArrf[%d] = %d > nGreenMaxArrf[iIntervf] = %d",
				iIntervf, nGreenMinArrf[iIntervf], nGreenMaxArrf[iIntervf]);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nGreenMinArrf[%d] = %d > nGreenMaxArrf[iIntervf] = %d",
				iIntervf, nGreenMinArrf[iIntervf], nGreenMaxArrf[iIntervf]);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nGreenMinArrf[iIntervf] > nGreenMaxArrf[iIntervf])

	} //for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iIntervf++)
/////////////////////////////////////////////////////////////////////////////

#ifdef PRINTING_THE_DETAILS
	fprintf(fout, "\n\n Blue: sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals);
#endif //#ifdef PRINTING_THE_DETAILS

	if (sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals > nNumOfColorIntervals)
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d",
			sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals, nNumOfColorIntervals);

		fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = %d, nNumOfColorIntervals = %d",
			sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals, nNumOfColorIntervals);

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals < 1 || sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals > nNumOfColorIntervals)

	for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iIntervf++)
	{
		/*
				nBlueMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervf];
				nBlueMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervf];
		*/
		nBlueMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iIntervf];
		nBlueMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iIntervf];

#ifdef PRINTING_THE_DETAILS
		fprintf(fout, "\n nBlueMinArrf[%d] = %d, nBlueMaxArrf[%d] = %d",
			iIntervf, nBlueMinArrf[iIntervf], iIntervf, nBlueMaxArrf[iIntervf]);
#endif //#ifdef PRINTING_THE_DETAILS

		if (nBlueMinArrf[iIntervf] < 0 || nBlueMinArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMinArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nBlueMinArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMinArrf[%d] = %d, nNumOfBins_OneColor = %d",
				iIntervf, nBlueMinArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nBlueMinArrf[iIntervf] < 0 || nBlueMinArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nBlueMaxArrf[iIntervf] < 0 || nBlueMaxArrf[iIntervf] >= nNumOfBins_OneColor)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMaxArrf[%d] = %d, nNumOfBins_OneColor = %d", iIntervf, nBlueMaxArrf[iIntervf], nNumOfBins_OneColor);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMaxArrf[%d] = %d, nNumOfBins_OneColor = %d",
				iIntervf, nBlueMaxArrf[iIntervf], nNumOfBins_OneColor);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nBlueMaxArrf[iIntervf] < 0 || nBlueMaxArrf[iIntervf] >= nNumOfBins_OneColor)

		if (nBlueMinArrf[iIntervf] > nBlueMaxArrf[iIntervf])
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMinArrf[%d] = %d > nBlueMaxArrf[iIntervf] = %d",
				iIntervf, nBlueMinArrf[iIntervf], nBlueMaxArrf[iIntervf]);
			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nBlueMinArrf[%d] = %d > nBlueMaxArrf[iIntervf] = %d",
				iIntervf, nBlueMinArrf[iIntervf], nBlueMaxArrf[iIntervf]);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nBlueMinArrf[iIntervf] > nBlueMaxArrf[iIntervf])

	} //for (iIntervf = 0; iIntervf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iIntervf++)
///////////////////////////////////////////////
/*
//#define nNumOfBins_OneImage_AllColors (nNumOfBins_OneColor*nNumOfBins_OneColor*nNumOfBins_OneColor)

nIndexOfBinCurf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) +( nBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));
 //if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)

nNumOfPixelsIn_CombinedColorBins_OneImageArrf[nIndexOfBinCurf] += 1;
*/

	for (iIntervRedf = 0; iIntervRedf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iIntervRedf++)
	{
/*
	nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;

	//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
	nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBins_OneImageArr[iBin];

*/

		for (iRedBinf = nRedMinArrf[iIntervRedf]; iRedBinf < nRedMaxArrf[iIntervRedf]; iRedBinf++)
		{
//////////////////////////
			for (iIntervGreenf = 0; iIntervGreenf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iIntervGreenf++)
			{

				for (iGreenBinf = nGreenMinArrf[iIntervGreenf]; iGreenBinf < nGreenMaxArrf[iIntervGreenf]; iGreenBinf++)
				{

					for (iIntervBluef = 0; iIntervBluef < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iIntervBluef++)
					{

						for (iBlueBinf = nBlueMinArrf[iIntervBluef]; iBlueBinf < nBlueMaxArrf[iIntervBluef]; iBlueBinf++)
						{
							nIndexOfBinCurf = iRedBinf + (iGreenBinf* nNumOfBins_OneColor) + (iBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));

							if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)
							{
								printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBinCurf = %d >= nNumOfBins_OneImage_AllColors = %d || ..., nPositOfVecf = %d",
									nIndexOfBinCurf, nNumOfBins_OneImage_AllColors, nPositOfVecf);
								fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBinCurf = %d >= nNumOfBins_OneImage_AllColors = %d || ..., nPositOfVecf = %d",
									nIndexOfBinCurf, nNumOfBins_OneImage_AllColors, nPositOfVecf);


								printf("\n\n Please press any key to exit"); fflush(fout); getchar(); exit(1);

								return UNSUCCESSFUL_RETURN;
							}//if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)
		/////////////////////////////////

							//nIndexOfBinCurf == iBin previously; nIndexOfBin_AllVecsf = nIndexOfBin  
		//	nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;

							nIndexOfBin_AllVecsf = nIndexOfBinCurf + nPositOfVecf * nNumOfBins_OneImage_AllColors;

							if (nClassf == 1)
							{
								if (nIndexOfBin_AllVecsf < 0 || nIndexOfBin_AllVecsf >= nNumOfBins_AllColorsForAll_NorImages)
								{
									printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBin_AllVecsf = %d >= nNumOfBins_AllColorsForAll_NorImages = %d, nIndexOfBinCurf = %d, nPositOfVecf = %d",
										nIndexOfBin_AllVecsf, nNumOfBins_AllColorsForAll_NorImages, nIndexOfBinCurf, nPositOfVecf);

									fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBin_AllVecsf = %d >= nNumOfBins_AllColorsForAll_NorImages = %d, nIndexOfBinCurf = %d, nPositOfVecf = %d",
										nIndexOfBin_AllVecsf, nNumOfBins_AllColorsForAll_NorImages, nIndexOfBinCurf, nPositOfVecf);

									printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

									return UNSUCCESSFUL_RETURN;
								} //if (nIndexOfBin_AllVecsf < 0 || nIndexOfBin_AllVecsf >= nNumOfOneColorBinsInAll_NorImages)
							}//if (nClassf == 1)
							else if (nClassf == 0)
							{
								if (nIndexOfBin_AllVecsf < 0 || nIndexOfBin_AllVecsf >= nNumOfBins_AllColorsForAll_MalImages)
								{
									printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBin_AllVecsf = %d >= nNumOfBins_AllColorsForAll_MalImages = %d, nIndexOfBinCurf = %d, nPositOfVecf = %d",
										nIndexOfBin_AllVecsf, nNumOfBins_AllColorsForAll_MalImages, nIndexOfBinCurf, nPositOfVecf);

									fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBin_AllVecsf = %d >= nNumOfBins_AllColorsForAll_MalImages = %d, nIndexOfBinCurf = %d, nPositOfVecf = %d",
										nIndexOfBin_AllVecsf, nNumOfBins_AllColorsForAll_MalImages, nIndexOfBinCurf, nPositOfVecf);

									printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

									return UNSUCCESSFUL_RETURN;
								} //if (nIndexOfBin_AllVecsf < 0 || nIndexOfBin_AllVecsf >= nNumOfOneColorBinsInAll_MalImages)

							} // else if (nClassf == 0)

							if (nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf] < 0 || nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf] > nAreaf)
							{
								printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nAllBinsAll_Colors_ImagesArrf[%d] = %d, nAreaf = %d, nPositOfVecf = %d, nClassf = %d",
									nIndexOfBin_AllVecsf, nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf], nAreaf, nPositOfVecf, nClassf);
								printf("\n iRedBinf = %d, iGreenBinf = %d, iBlueBinf = %d, nIndexOfBin_AllVecsf = %d, nIndexOfBinCurf = %d",
									iRedBinf, iGreenBinf, iBlueBinf, nIndexOfBin_AllVecsf, nIndexOfBinCurf);


								fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nIndexOfBin_AllVecsf = %d >= nNumOfBins_AllColorsForAll_MalImages = %d, nIndexOfBinCurf = %d, nPositOfVecf = %d",
									nIndexOfBin_AllVecsf, nNumOfBins_AllColorsForAll_MalImages, nIndexOfBinCurf, nPositOfVecf);

								fprintf(fout, "\n iRedBinf = %d, iGreenBinf = %d, iBlueBinf = %d, nIndexOfBin_AllVecsf = %d, nIndexOfBinCurf = %d",
									iRedBinf, iGreenBinf, iBlueBinf, nIndexOfBin_AllVecsf, nIndexOfBinCurf);

								nResf = Printing_Boundaries_Of_Intervals(
									sBoundaries_Of_Intervalsf); // const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

								printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

								return UNSUCCESSFUL_RETURN;
							}//if (nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf] < 0 || nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf] > nAreaf)
		////////////////////////////////////////////////////////////
							nNumOfPixelsIn_2_ColorIntervals_OneImagef += nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf];

							if (nNumOfPixelsIn_2_ColorIntervals_OneImagef < 0 || nNumOfPixelsIn_2_ColorIntervals_OneImagef > nAreaf)
							{
								printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfPixelsIn_2_ColorIntervals_OneImagef = %d, nAreaf = %d, nClassf = %d, nPositOfVecf = %d",
									nNumOfPixelsIn_2_ColorIntervals_OneImagef, nAreaf, nClassf, nPositOfVecf);

								printf("\n\n nAllBinsAll_Colors_ImagesArrf[%d] = %d, nAreaf = %d, nPositOfVecf = %d, nClassf = %d",
									nIndexOfBin_AllVecsf, nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf], nAreaf, nPositOfVecf, nClassf);
								printf("\n iIntervf = %d, iRedBinf = %d, iGreenBinf = %d, iBlueBinf = %d, nIndexOfBin_AllVecsf = %d, nIndexOfBinCurf = %d",
									iIntervf, iRedBinf, iGreenBinf, iBlueBinf, nIndexOfBin_AllVecsf, nIndexOfBinCurf);


								fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage': nNumOfPixelsIn_2_ColorIntervals_OneImagef = %d, nAreaf = %d, nClassf = %d, nPositOfVecf = %d",
									nNumOfPixelsIn_2_ColorIntervals_OneImagef, nAreaf, nClassf, nPositOfVecf);

								fprintf(fout, "\n\n nAllBinsAll_Colors_ImagesArrf[%d] = %d, nAreaf = %d, nPositOfVecf = %d, nClassf = %d",
									nIndexOfBin_AllVecsf, nAllBinsAll_Colors_ImagesArrf[nIndexOfBin_AllVecsf], nAreaf, nPositOfVecf, nClassf);

								fprintf(fout, "\n iIntervf = %d, iRedBinf = %d, iGreenBinf = %d, iBlueBinf = %d, nIndexOfBin_AllVecsf = %d, nIndexOfBinCurf = %d",
									iIntervf, iRedBinf, iGreenBinf, iBlueBinf, nIndexOfBin_AllVecsf, nIndexOfBinCurf);

								nResf = Printing_Boundaries_Of_Intervals(
									sBoundaries_Of_Intervalsf); // const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

								printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

							} //if (nNumOfPixelsIn_2_ColorIntervals_OneImagef < 0 || nNumOfPixelsIn_2_ColorIntervals_OneImagef > nAreaf)

						} //for (iBlueBinf = nBlueMinArrf[iIntervBluef]; iBlueBinf < nBlueMaxArrf[iIntervBluef]; iBlueBinf++)
					} //for (iIntervBluef = 0; iIntervBluef < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iIntervBluef++)

				} //for (iGreenBinf = nGreenMinArrf[iIntervGreenf]; iGreenBinf < nGreenMaxArrf[iIntervGreenf]; iGreenBinf++)

			} //for (iIntervGreenf = 0; iIntervGreenf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iIntervGreenf++)

		} //for (iRedBinf = nRedMinArrf[iIntervRedf]; iRedBinf < nRedMaxArrf[iIntervRedf]; iRedBinf++)

	} //for (iIntervRedf = 0; iIntervRedf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iIntervRedf++)
///////////////////////////////////////////////////////////

	if (nNumOfPixelsIn_2_ColorIntervals_OneImagef < 0 || nNumOfPixelsIn_2_ColorIntervals_OneImagef > nAreaf)
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage' (the end): nNumOfPixelsIn_2_ColorIntervals_OneImagef = %d, nAreaf = %d, nClassf = %d, nPositOfVecf = %d", 
			nNumOfPixelsIn_2_ColorIntervals_OneImagef, nAreaf,nClassf, nPositOfVecf);

		fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImage' (the end): nNumOfPixelsIn_2_ColorIntervals_OneImagef = %d, nAreaf = %d, nClassf = %d, nPositOfVecf = %d",
			nNumOfPixelsIn_2_ColorIntervals_OneImagef, nAreaf, nClassf, nPositOfVecf);

		nResf = Printing_Boundaries_Of_Intervals(
			sBoundaries_Of_Intervalsf); // const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfPixelsIn_2_ColorIntervals_OneImagef < 0 || nNumOfPixelsIn_2_ColorIntervals_OneImagef > nAreaf)

	fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf = (float)(nNumOfPixelsIn_2_ColorIntervals_OneImagef) / (float)(nAreaf);

	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_2_ColorIntervals_OneImage(...
//////////////////////////////////////////////////

int NumOfPixelsIn_2_ColorIntervals_All_Images(
	//const COLOR_IMAGE *sColor_Imagef, //[]
	const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
	const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

	float fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[], //[nNumOfVecs_Normal]
	float fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[]) //[nNumOfVecs_Malignant]
{
	int NumOfPixelsIn_2_ColorIntervals_OneImage(
		const int nClassf, //1--normal, 0 -- abnormal
		const int nNumOfVecsTotf,
		const int nAreasOfAllImagesArrf[], //nNumOfVecsTotf,

		const int nPositOfVecf,

		//const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_ImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages] or [nNumOfBins_AllColorsForAll_MalImages]

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

		float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

	int
		nResf,

		nNumOfPixelsIn_2_ColorIntervals_OneImagef,

		nClassf, 
		iVecf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf;

	float
		fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;
	////////////////////////////////////////////////

	for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
	{
		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[iVecf] = 0.0;

		nClassf = 1;
		nResf = NumOfPixelsIn_2_ColorIntervals_OneImage(
			nClassf, //const int nClassf, //1--normal, 0 -- abnormal
			nNumOfVecs_Normal, //const int nNumOfVecsTotf,
			nAreaOf_NorImagesArr, //const int nAreasOfAllImagesArrf[], //nNumOfVecsTotf,

			iVecf, //const int nPositOfVecf,

			//const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
			nAllBinsAll_Colors_NorImagesArr, //const int nAllBinsAll_Colors_ImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages] or [nNumOfBins_AllColorsForAll_MalImages]

			sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nNumOfPixelsIn_2_ColorIntervals_OneImagef, //int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

			fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf); // float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_All_Images' by 'NumOfPixelsIn_2_ColorIntervals_OneImage': iVecf = %d, nClassf = %d",iVecf, nClassf);

			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_All_Images' by 'NumOfPixelsIn_2_ColorIntervals_OneImage': iVecf = %d, nClassf = %d", iVecf, nClassf);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[iVecf] = fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;
	}//for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)

	for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
	{
		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[iVecf] = 0.0;

		nClassf = 0;
		nResf = NumOfPixelsIn_2_ColorIntervals_OneImage(
			nClassf, //const int nClassf, //1--normal, 0 -- abnormal
			nNumOfVecs_Malignant, //const int nNumOfVecsTotf,
			nAreaOf_MalImagesArr, //const int nAreasOfAllImagesArrf[], //nNumOfVecsTotf,

			iVecf, //const int nPositOfVecf,

			//const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
			nAllBinsAll_Colors_MalImagesArr, //const int nAllBinsAll_Colors_ImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages] or [nNumOfBins_AllColorsForAll_MalImages]

			sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nNumOfPixelsIn_2_ColorIntervals_OneImagef, //int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

			fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf); // float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_All_Images' by 'NumOfPixelsIn_2_ColorIntervals_OneImage': iVecf = %d, nClassf = %d", iVecf, nClassf);

			fprintf(fout, "\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_All_Images' by 'NumOfPixelsIn_2_ColorIntervals_OneImage': iVecf = %d, nClassf = %d", iVecf, nClassf);

			printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[iVecf] = fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;
	}//for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
///////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_2_ColorIntervals_All_Images(...
////////////////////////////////////////////////////////////////////////////

int Func_BySeparabilityOf_2_ColorIntervals(
	const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
	const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

	float &fBestBinSeparabilityBy_2_Intervalsf)

{
	int NumOfPixelsIn_2_ColorIntervals_All_Images(
		//const COLOR_IMAGE *sColor_Imagef, //[]
		const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

		float fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[], //[nNumOfVecs_Normal]
		float fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[]); //[nNumOfVecs_Malignant]

	int FindingBestSeparByRatioOfOneFea(
		const float fLargef,
		const int nDim1stf,
		const int nDim2ndf,

		const  float fepsf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1

		float &fEfficBestSeparOfOneFeaf,

		float &fPosSeparOfOneFeaBestf,

		int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestf,
		float &fNumArr2ndSeparBestf,

		float &fDiffSeparOfArr1stAndArr2ndBestf,
		float &fRatioOfSeparOfArr1stAndArr2ndBestf);
		/////////////////////////////////////////////////

	int	Converting_One_Fea_To_ARange_0_1(
		const int nDim1f,
		const int nDim2f,

		int &nFeaTheSameOrNotf,
		float &fFeaMinf,
		float &fFeaMaxf,

		float fOneDimArr1stf[], //[nDim1f]
		float fOneDimArr2ndf[]); //[nDim2f]

	int Printing_Boundaries_Of_Intervals_With_Effic(

		const float fEfficf,
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

/////////////////////////////////////////
	int
		nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		nFeaTheSameOrNotf,

		nResf;

	float
		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[nNumOfVecs_Normal],
		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[nNumOfVecs_Malignant],

		fEfficBestSeparOfOneFeaf,

		fPosSeparOfOneFeaBestf,

		fNumArr1stSeparBestf,
		fNumArr2ndSeparBestf,

		fFeaMinf,
		fFeaMaxf,

		fDiffSeparOfArr1stAndArr2ndBestf,
		fRatioOfSeparOfArr1stAndArr2ndBestf;
//////////////////////
	nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob += 1;

	nResf = NumOfPixelsIn_2_ColorIntervals_All_Images(
		nAreaOf_NorImagesArr, //const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
		nAreaOf_MalImagesArr, //const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

		sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		nAllBinsAll_Colors_NorImagesArr, //const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
		nAllBinsAll_Colors_MalImagesArr, //const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf, //float fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf[], //[nNumOfVecs_Normal]
		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf); // float fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf[]); //[nNumOfVecs_Malignant]
/////////////////////

	nResf = Converting_One_Fea_To_ARange_0_1(
		nNumOfVecs_Normal, //const int nDim1f,
		nNumOfVecs_Malignant, //const int nDim2f,

		nFeaTheSameOrNotf, //int &nFeaTheSameOrNotf,
		fFeaMinf, //float &fFeaMinf,
		fFeaMaxf, //float &fFeaMaxf,

		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf, //float fOneDimArr1stf[], //[nDim1f]
		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf); // float fOneDimArr2ndf[]); //[nDim2f]

	if (nFeaTheSameOrNotf == 1)
	{
		//printf( "\n\n 'Func_BySeparabilityOf_2_ColorIntervals': the same feas, nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob = %d, iIterForCuckSear_Glob = %d",
			//nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob, iIterForCuckSear_Glob);
		fprintf(fout, "\n\n 'Func_BySeparabilityOf_2_ColorIntervals': the same feas, nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob = %d, iIterForCuckSear_Glob = %d",
			nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob, iIterForCuckSear_Glob);

		fflush(fout);
		fBestBinSeparabilityBy_2_Intervalsf = fLarge; // no separability

		return SUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotf == 1)
///////////////////
	nResf = FindingBestSeparByRatioOfOneFea(
		fLarge, //const float fLargef,
		nNumOfVecs_Normal, //const int nDim1f,
		nNumOfVecs_Malignant, //const int nDim2f,

		feps, //const  float fepsf,

		fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

		//after normalization to (0,1)
		fNumOfPixelsIn_2_ColorIntervals_NorImages_Normalized_Arrf, //const float fArr1stf[], //0 -- 1
		fNumOfPixelsIn_2_ColorIntervals_MalImages_Normalized_Arrf, //const  float fArr2ndf[], //0 -- 1

		fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaf,

		fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestf,

		nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf, //float &fNumArr1stSeparBestf,
		fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestf,

		fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestf,
		fRatioOfSeparOfArr1stAndArr2ndBestf); // float &fRatioOfSeparOfArr1stAndArr2ndBestf)

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'Func_BySeparabilityOf_2_ColorIntervals' by 'FindingBestSeparByRatioOfOneFea', nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob = %d", 
				nNumFunc_BySeparabilityOf_2_ColorIntervalsCur_Glob);
			printf("\n\n Please press any key to exit");

			getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)
///////////////////

fBestBinSeparabilityBy_2_Intervalsf = fEfficBestSeparOfOneFeaf;


if (fBestBinSeparabilityBy_2_Intervalsf <= fEfficForPrinting)
{
//fout_features

	fprintf(fout, "\n\n 'Func_BySeparabilityOf_2_ColorIntervals': printing fBestBinSeparabilityBy_2_Intervalsf = %E < fEfficForPrinting = %E",
		fBestBinSeparabilityBy_2_Intervalsf,fEfficForPrinting);

	fprintf(fout, "\n fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E, nSeparRatioDirectionBestf = %d",
		fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf, nSeparRatioDirectionBestf);

	nResf = Printing_Boundaries_Of_Intervals_With_Effic(
		fBestBinSeparabilityBy_2_Intervalsf,
		sBoundaries_Of_Intervalsf); // const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

	fflush(fout);
} // if (fEfficBestSeparOfOneFeaf < fEfficForPrinting)

	return SUCCESSFUL_RETURN;
}//int Func_BySeparabilityOf_2_ColorIntervals(...
//////////////////////////////////////////////////////////////

int NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues(
	const COLOR_IMAGE *sColor_Imagef, //[]

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int &nNumOfPixelsIn_2_ColorIntervals_OneImagef, 

	float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf) //
{
	int
		nIndexOfPixelCurf,

		nIndexOfBinCurf,
		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nImageSizeCurf = nImageWidthf * nImageLengthf,

		iFeaf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedMinArrf[nNumOfColorIntervals],
		nRedMaxArrf[nNumOfColorIntervals],

		nGreenMinArrf[nNumOfColorIntervals],
		nGreenMaxArrf[nNumOfColorIntervals],

		nBlueMinArrf[nNumOfColorIntervals],
		nBlueMaxArrf[nNumOfColorIntervals],

		iIntervf,
		iWidf,
		iLenf;
/////////////////////////////////////////

	if (nImageSizeCurf <= 0)
	{
		printf("\n\n An error in 'NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues': nImageSizeCurf = %d", nImageSizeCurf);
		printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nImageSizeCurf <= 0)
	////////////////////////////////
	nNumOfPixelsIn_2_ColorIntervals_OneImagef = 0;
	fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf = 0.0;
	
/////////////////////////////
	for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)
	{
		nRedMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervf];
		nRedMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervf];

		nGreenMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervf];
		nGreenMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervf];

		nBlueMinArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervf];
		nBlueMaxArrf[iIntervf] = sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervf];

	} //for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)
			{
				if ((nRedCurf >= nRedMinArrf[iIntervf] && nRedCurf < nRedMaxArrf[iIntervf]) && (nGreenCurf >= nGreenMinArrf[iIntervf] && nGreenCurf < nGreenMaxArrf[iIntervf]) && 
					(nBlueCurf >= nBlueMinArrf[iIntervf] && nBlueCurf < nBlueMaxArrf[iIntervf]))
				{
					nNumOfPixelsIn_2_ColorIntervals_OneImagef += 1;
					break;
				}//if ((nRedCurf >= nRedMinArrf[iIntervf] && nRedCurf < nRedMaxArrf[iIntervf]) && (nGreenCurf >= nGreenMinArrf[iIntervf] && nGreenCurf < nGreenMaxArrf[iIntervf]) && 
			
			} //for (iIntervf = 0; iIntervf < nNumOfColorIntervals; iIntervf++)

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf = (float)(nNumOfPixelsIn_2_ColorIntervals_OneImagef) / (float)(nImageSizeCurf);

	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues(...
////////////////////////////////////////////////////////////////////////

int NumOfPixelsIn_2_ColorIntervals_All_ImagesByReadingPixelValues(
	//const COLOR_IMAGE *sColor_Imagef, //[]

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

	int nNumOfPixelsIn_ColorIntervals_Normal_Images_Arrf[], //[nNumOfVecs_Normal]
	int nNumOfPixelsIn_ColorIntervals_Malignant_Images_Arrf[], //[nNumOfVecs_Malignant]

	float fNumOfPixelsIn_ColorIntervals_Normal_Images_Normalized_Arrf[], //[nNumOfVecs_Normal]
	float fNumOfPixelsIn_ColorIntervals_Malignant_Images_Normalized_Arrf[]) //[nNumOfVecs_Malignant]
	{

	int NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues(
		const COLOR_IMAGE *sColor_Imagef, //[]

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

		float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

	int
		nResf,

		nNumOfProcessedFiles_NormalTotf = 0,
		nNumOfProcessedFiles_MalignantTotf = 0,

		nNumOfPixelsIn_2_ColorIntervals_OneImagef,

		iVecf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf;

	float
		fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;
	////////////////////////////////////////////////

	for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
	{
		nNumOfPixelsIn_ColorIntervals_Normal_Images_Arrf[iVecf] = 0;

		fNumOfPixelsIn_ColorIntervals_Normal_Images_Normalized_Arrf[iVecf] = 0.0;
	}//for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)

	for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
	{
		nNumOfPixelsIn_ColorIntervals_Malignant_Images_Arrf[iVecf] = 0;

		fNumOfPixelsIn_ColorIntervals_Malignant_Images_Normalized_Arrf[iVecf] = 0.0;
	}//for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
///////////////////////////////////////
	Image image_inf;

	char cFileName_inDirectoryOf_Normal_Imagesf[200]; //[150]
	char cFileName_inDirectoryOf_Malignant_Imagesf[200]; //[150]
	//image_inf.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIRf;
	struct dirent *entryf;

	COLOR_IMAGE sColor_Imagef;
	////////////////////////////////

	//woody breast
	//50 D:\Imago\Images\spleen\Benign_AOI_50_SelecColor
		//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\Benign_AOI_50_SelecColor\\"; //copy to if (pDIRf = opendir(.. as well

		//227
		//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\BenignAOI-Color-PNG\\"; //copy to if (pDIRf = opendir(.. as well

	//	478 //D:\Imago\Images\spleen\Color\Oct_30\BenignFOV-Sym3
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\Oct_30\\BenignFOV-Sym3\\"; //copy to if (pDIRf = opendir(.. as well

	//500
//D:\Imago\Images\WoodyBreast\Color_Images\Oct20_Symmetry_2\Normal-COBB-AOI
//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\Normal-COBB-AOI\\"; //copy to if (pDIRf = opendir(.. as well

//D:\Imago\Images\WoodyBreast\Color_Images\Oct27_Sym_3\COBB-Normal-Sym3
char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-Normal-Sym3\\"; //copy to if (pDIRf = opendir(.. as well

//D:\Imago\Images\WoodyBreast\Color_Images\Color_Island\COBB - Normal - CI2_2H
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-Normal-CI2_2H\\"; //copy to if (pDIRf = opendir(.. as well

//D:\Imago\Images\WoodyBreast\Color_Images\Color_Island\COBB-Normal-CI2_all
//	char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-Normal-CI2_all\\";
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//check out '#define nNumOfVecs_Malignant'!
//50 D:\Imago\Images\spleen\Malignant_AOI_50_SelecColor
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\";

//311
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\MalignantAOI-Color-PNG\\"; //copy to if (pDIRf = opendir(.. as well

//625 // D:\Imago\Images\spleen\Color\Oct_30\MalignantFOV-Sym3
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\Oct_30\\MalignantFOV-Sym3\\"; //copy to if (pDIRf = opendir(.. as well

//298 //D:\Imago\Images\WoodyBreast\Color_Images\Oct20_Symmetry_2\WB-Color-AOI
//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\WB-Color-AOI\\";

//D:\Imago\Images\WoodyBreast\Color_Images\Oct27_Sym_3\COBB-WB-Sym3
char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-WB-Sym3\\";

//D:\Imago\Images\WoodyBreast\Color_Images\Color_Island\COBB - WB - CI2_2H
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_2H\\";

//D:\Imago\Images\WoodyBreast\Color_Images\Color_Island\COBB-WB-CI2_all
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_all\\";

/////////////////////////////////////////////////////
//Normal 
	//////////////////////////////////////////////////////////////

	nNumOfProcessedFiles_NormalTotf = 0;
	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Benign_AOI_50_SelecColor\\"))

	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Color\\BenignAOI-Color-PNG\\"))


	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Color\\Oct_30\\BenignFOV-Sym3\\"))

		//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\Normal-COBB-AOI\\"))

		if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-Normal-Sym3\\"))

		//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-Normal-CI2_2H\\"))

		//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-Normal-CI2_all\\"))

	{
		while (entryf = readdir(pDIRf))
		{
			if (strcmp(entryf->d_name, ".") != 0 && strcmp(entryf->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_NormalTotf += 1;

				nNumOfProcessedFiles_NormalTot_Glob = nNumOfProcessedFiles_NormalTotf;

				if (nNumOfProcessedFiles_NormalTotf > nNumOfVecs_Normal)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_NormalTotf = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTotf, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_NormalTotf = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTotf, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit"); fflush(fout);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_NormalTotf > nNumOfVecs_Normal)

//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entryf->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_inDirectoryOf_Normal_Imagesf, '\0', sizeof(cFileName_inDirectoryOf_Normal_Imagesf));

				strcpy(cFileName_inDirectoryOf_Normal_Imagesf, cInput_DirectoryOf_Normal_Images);

				strcat(cFileName_inDirectoryOf_Normal_Imagesf, entryf->d_name);

				fprintf(fout, "\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTotf = %d\n", cFileName_inDirectoryOf_Normal_Imagesf, nNumOfProcessedFiles_NormalTotf);

				if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTotf = %d\n", cFileName_inDirectoryOf_Normal_Imagesf, nNumOfProcessedFiles_NormalTotf);
				} //if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)
////////////////////////////////////////
				image_inf.read(cFileName_inDirectoryOf_Normal_Imagesf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n Concatenated input file: %s\n", cFileName_inDirectoryOf_Normal_Imagesf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				nResf = Reading_AColorImage(
					image_inf, //const Image& image_inf,

					&sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);
					//sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'SeparabilityOf_2_Color_Intervals' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} // if (nResf == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)
				{
					//printf("\n\n So far nNumOfProcessedFiles_NormalTotf = %d", nNumOfProcessedFiles_NormalTotf);
				} //if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)

					////////////////////////////////////

				nResf = NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues(
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[]

					sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

					nNumOfPixelsIn_2_ColorIntervals_OneImagef, //int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

					fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf); // float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

				nNumOfPixelsIn_ColorIntervals_Normal_Images_Arrf[nNumOfProcessedFiles_NormalTotf - 1] = nNumOfPixelsIn_2_ColorIntervals_OneImagef;  //[nNumOfVecs_Normal]

				fNumOfPixelsIn_ColorIntervals_Normal_Images_Normalized_Arrf[nNumOfProcessedFiles_NormalTotf - 1] = fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;

		////////////////////////////////////////////////

				delete[] sColor_Imagef.nRed_Arr;
				delete[] sColor_Imagef.nGreen_Arr;
				delete[] sColor_Imagef.nBlue_Arr;

				delete[] sColor_Imagef.nLenObjectBoundary_Arr;
				delete[] sColor_Imagef.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTotf = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTotf);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)
				{
					printf("\n Normal: the end of nNumOfProcessedFiles_NormalTotf = %d, nNumOfVecs_Normal = %d; going to to the next image", nNumOfProcessedFiles_NormalTotf, nNumOfVecs_Normal);
				} //if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entryf->d_name, ".") != 0 && strcmp(entryf->d_name, "..") != 0)
		}//while (entryf = readdir(pDIRf))

		closedir(pDIRf);
	}//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Normal_AOI_50_SelecColor\\"))

	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of normal images can not be opened");

		printf("\n\n Press any key to exit"); fflush(fout);	getchar(); exit(1);

		perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

//printf("\n\n The end of normal inages: press any key to exit");	fflush(fout);  getchar(); exit(1);
	//printf("\n\n The end of normal images: press any key to continue to the malignant ones");	fflush(fout);  getchar();

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Malignant 

//check out '#define nNumOfVecs_Malignant'!
	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\WB-Color-AOI\\"))

	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Color\\MalignantAOI-Color-PNG\\"))

	//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Color\\Oct_30\\MalignantFOV-Sym3"))

		if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-WB-Sym3\\"))

		//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_2H\\"))

			//if (pDIRf = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Color_Island\\COBB-WB-CI2_all\\"))
	{
		nNumOfProcessedFiles_MalignantTotf = 0;
		while (entryf = readdir(pDIRf))
		{
			if (strcmp(entryf->d_name, ".") != 0 && strcmp(entryf->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_MalignantTotf += 1;

				if (nNumOfProcessedFiles_MalignantTotf > nNumOfVecs_Malignant)
				{
					printf("\n\n An error in counting the number of malignant images: nNumOfProcessedFiles_MalignantTotf = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTotf, nNumOfVecs_Malignant);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of malignant images: nNumOfProcessedFiles_MalignantTotf = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTotf, nNumOfVecs_Malignant);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_MalignantTotf > nNumOfVecs_Malignant)

		//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entryf->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_inDirectoryOf_Malignant_Imagesf, '\0', sizeof(cFileName_inDirectoryOf_Malignant_Imagesf));

				strcpy(cFileName_inDirectoryOf_Malignant_Imagesf, cInput_DirectoryOf_Malignant_Images);

				strcat(cFileName_inDirectoryOf_Malignant_Imagesf, entryf->d_name);

				if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_MalignantTotf = %d\n", cFileName_inDirectoryOf_Malignant_Imagesf, nNumOfProcessedFiles_MalignantTotf);
				} //if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)

				//fprintf(fout, "\n\n Concatenated input file: %s\n", cFileName_inDirectoryOf_Malignant_Imagesf);

				//printf("\n\n Press any key 1:");	fflush(fout);  getchar();
////////////////////////////////////////////////
				image_inf.read(cFileName_inDirectoryOf_Malignant_Imagesf);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS

				nResf = Reading_AColorImage(
					image_inf, //const Image& image_inf,

					&sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'SeparabilityOf_2_Color_Intervals' for 'Reading_AColorImage' 2");
					printf("\n\n Press any key to exit");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nResf == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)
				{
					//printf("\n\n So far nNumOfProcessedFiles_MalignantTotf = %d", nNumOfProcessedFiles_MalignantTotf);
				} //if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)

/////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n Concatenated input file: %s\n", cFileName_inDirectoryOf_Normal_Imagesf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				nResf = Reading_AColorImage(
					image_inf, //const Image& image_inf,

					&sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);
					//sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'SeparabilityOf_2_Color_Intervals' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} // if (nResf == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)
				{
					//printf("\n\n So far nNumOfProcessedFiles_NormalTotf = %d", nNumOfProcessedFiles_NormalTotf);
				} //if ((nNumOfProcessedFiles_NormalTotf / 100) * 100 == nNumOfProcessedFiles_NormalTotf)

					////////////////////////////////////

				nResf = NumOfPixelsIn_2_ColorIntervals_OneImageByReadingPixelValues(
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[]

					sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

					nNumOfPixelsIn_2_ColorIntervals_OneImagef, //int &nNumOfPixelsIn_2_ColorIntervals_OneImagef,

					fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf); // float &fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf);

				nNumOfPixelsIn_ColorIntervals_Malignant_Images_Arrf[nNumOfProcessedFiles_MalignantTotf - 1] = nNumOfPixelsIn_2_ColorIntervals_OneImagef;  //[nNumOfVecs_Normal]

				fNumOfPixelsIn_ColorIntervals_Malignant_Images_Normalized_Arrf[nNumOfProcessedFiles_MalignantTotf - 1] = fNumOfPixelsIn_2_ColorIntervals_OneImage_Normalizedf;

				////////////////////////////////////////////////

						////////////////////////////////////////////////////////////////////
				delete[] sColor_Imagef.nRed_Arr;
				delete[] sColor_Imagef.nGreen_Arr;
				delete[] sColor_Imagef.nBlue_Arr;

				delete[] sColor_Imagef.nLenObjectBoundary_Arr;
				delete[] sColor_Imagef.nIsAPixelBackground_Arr;

				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTotf = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTotf);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)
				{
					printf("\n Malignant: the end of nNumOfProcessedFiles_MalignantTotf = %d, nNumOfVecs_Malignant = %d; going to to the next image",
						nNumOfProcessedFiles_MalignantTotf, nNumOfVecs_Malignant);
				} //if ((nNumOfProcessedFiles_MalignantTotf / 100) * 100 == nNumOfProcessedFiles_MalignantTotf)

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entryf->d_name, ".") != 0 && strcmp(entryf->d_name, "..") != 0)
		}//while (entryf = readdir(pDIRf))

		closedir(pDIRf);
	}//if (pDIRf = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n The directory of malignant images can not be opened");

		printf("\n\n Press any key to exit");	fflush(fout);  getchar(); exit(1);

		//perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_2_ColorIntervals_All_ImagesByReadingPixelValues(...
////////////////////////////////////////////////////////////////////////////

int Copying_Adjusting_OneDimFeaVec_To_Intervals(
	const int nNumOfFeasf,

	int nOneDimArrf[], //[nNumOfFeasf]

	BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
{
	int
		//nIntervalCurf,

		nColorTempf,

		iIntervalf,

		nBeginFor_iFeaForOneIntervalf,

		nFeaCurf,
		iFeaForOneIntervalf,
		iFeaf;
///////////////////////////////////
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf])
		{
			sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] = -1;

			sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf] = -1;
		}//if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf])

		if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf])
		{
			sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] = -1;

			sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf] = -1;
		}//if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf])

		if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf])
		{
			sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] = -1;

			sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf] = -1;
		}//if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf])

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
////////////////////////////////////////////////////////

	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		nBeginFor_iFeaForOneIntervalf = iIntervalf * nNumOfBoundariesFor_3ColorsAnd_OneInterval;

		for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < nNumOfBoundariesFor_3ColorsAnd_OneInterval; iFeaForOneIntervalf++)
		{
			nFeaCurf = iFeaForOneIntervalf + nBeginFor_iFeaForOneIntervalf;
			if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)
			{
				printf("\n\n An error in 'Copying_Adjusting_OneDimFeaVec_To_Intervals': nFeaCurf = %d >= nNumOfFeas = %d",nFeaCurf,	nNumOfFeas);

				fflush(fout); getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)

			if (nOneDimArrf[nFeaCurf] < 0)
				nOneDimArrf[nFeaCurf] = 0;
			else if (nOneDimArrf[nFeaCurf] >= nNumOfBins_OneColor)
				nOneDimArrf[nFeaCurf] = nNumOfBins_OneColor - 1;

			if (iFeaForOneIntervalf == 0)
			{
				sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //else if (iFeaForOneIntervalf == 0)
			else if (iFeaForOneIntervalf == 1)
			{
				sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //else if (iFeaForOneIntervalf == 1)
			else if (iFeaForOneIntervalf == 2)
			{
				sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //else if (iFeaForOneIntervalf == 2)
			else if (iFeaForOneIntervalf == 3)
			{
				sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //else if (iFeaForOneIntervalf == 3)
			else if (iFeaForOneIntervalf == 4)
			{
				sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //else if (iFeaForOneIntervalf == 4)
			else if (iFeaForOneIntervalf == 5)
			{
				sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] = nOneDimArrf[nFeaCurf];
			} //if (iFeaForOneIntervalf == 5)

		} //for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < nNumOfBoundariesFor_3ColorsAnd_OneInterval; iFeaForOneIntervalf++)

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)

///////////////////////////////////////////////////
/*
	if (nIntervalCurf != nNumOfColorIntervals - 1)
	{
		printf("\n\n An error in 'Copying_Adjusting_OneDimFeaVec_To_Intervals': nIntervalCurf = %d != nNumOfColorIntervals - 1 = %d", nIntervalCurf, nNumOfColorIntervals - 1);
		getchar(); fflush(fout);  exit(1);
	}//if (nIntervalCurf != nNumOfColorIntervals - 1)
*/
///////////////////////////////////////////////////
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf])
		{
			nColorTempf = sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf];

			sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] = sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf];

			sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf] = nColorTempf;

		}//if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf])

		if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf])
		{
			nColorTempf = sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf];

			sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] = sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf];

			sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf] = nColorTempf;
		}//if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf])

		if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf])
		{
			nColorTempf = sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf];

			sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] = sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf];

			sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf] = nColorTempf;
		}//if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] < sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf])

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
//////////////////////////////////////
//verification
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf] == -1)
		{
			printf("\n\n An error in 'Copying_Adjusting_OneDimFeaVec_To_Intervals' (== -1): sBoundaries_Of_Intervalsf->nRedIntervals_High[%d] = %d || sBoundaries_Of_Intervalsf->nRedIntervals_Low[%d] = %d",
				iIntervalf, sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf],
				iIntervalf, sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf]);

			getchar(); fflush(fout);  exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (sBoundaries_Of_Intervalsf->nRedIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nRedIntervals_Low[iIntervalf] == -1)

		if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf] == -1)
		{
			printf("\n\n An error in 'Copying_Adjusting_OneDimFeaVec_To_Intervals' (== -1): sBoundaries_Of_Intervalsf->nGreenIntervals_High[%d] = %d || sBoundaries_Of_Intervalsf->nGreenIntervals_Low[%d] = %d",
				iIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf],
				iIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf]);

			getchar(); fflush(fout);  exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (sBoundaries_Of_Intervalsf->nGreenIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iIntervalf] == -1)

		if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf] == -1)
		{
			printf("\n\n An error in 'Copying_Adjusting_OneDimFeaVec_To_Intervals' (== -1): sBoundaries_Of_Intervalsf->nBlueIntervals_High[%d] = %d || sBoundaries_Of_Intervalsf->nBlueIntervals_Low[%d] = %d",
				iIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf],
				iIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf]);

			getchar(); fflush(fout);  exit(1);

			return UNSUCCESSFUL_RETURN;
		}//if (sBoundaries_Of_Intervalsf->nBlueIntervals_High[iIntervalf] == -1 || sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iIntervalf] == -1)

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)

	return SUCCESSFUL_RETURN;
} //int Copying_Adjusting_OneDimFeaVec_To_Intervals(
///////////////////////////////////////////////////////////////

//Files of CuckSear

int Int_Belongs_To_Arr_Or_Not(
	const int nDimf,
	const int nTestf,
	const int nArrf[],

	int &nNumBelongsf, //'>= 1' --> belongs and '0' otherwise

	int nPosArrf[]) //[nDimf]
{
	int
		i1,

		nDiff1f;

	nNumBelongsf = 0; //impossible
	for (i1 = 0; i1 < nDimf; i1++)
		nPosArrf[i1] = -1;

	for (i1 = 0; i1 < nDimf; i1++)
	{
		nDiff1f = nArrf[i1] - nTestf;

		if (nDiff1f == 0)
		{
			nNumBelongsf += 1; //belongs
			nPosArrf[nNumBelongsf - 1] = i1;
		} // if (nDiff1f == 0)

	} // for (i1 = 0; i1 < nDimf; i1++)

	return SUCCESSFUL_RETURN;
} // int Int_Belongs_To_Arr_Or_Not(...
//////////////////////////////////////////

int Int_Belongs_To_Arr_Or_Not_Simple(
	const int nDimf,
	const int nTestf,
	const int nArrf[],

	int &nNumBelongsf) //'== 1' --> belongs and '-1' otherwise

{
	int
		i1,
		nDiff1f;

	nNumBelongsf = -1; //impossible

	for (i1 = 0; i1 < nDimf; i1++)
	{
		nDiff1f = nArrf[i1] - nTestf;

		if (nDiff1f == 0)
		{
			nNumBelongsf = 1; //belongs
			return SUCCESSFUL_RETURN;
		} // if (nDiff1f == 0)

	} // for (i1 = 0; i1 < nDimf; i1++)

	return SUCCESSFUL_RETURN;
} // int Int_Belongs_To_Arr_Or_Not_Simple(...
//////////////////////////////////////////

double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
	long &l_Seedf)
{
	long
		l_Af = 16807,
		l_Mf = 2147483647;

	double
		dReturnf;

	l_Seedf = (l_Af*l_Seedf) % l_Mf;

	return dReturnf = ((double)(l_Seedf) / (double)(l_Mf)+1.0) / 2.0;

} //float dRandNumGenerFrom0To1WithUpTo2billionCombinats(
//////////////////////////////////////////

int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(
	const int nDimf,
	const int nNumOfSelecf,

	const int nNumOfRandInitializingLoopsMaxf,

	long &l_Seedf, //must be already specified
	int nPositOfIntArrf[])  //nNumOfSelecf
{
	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int Int_Belongs_To_Arr_Or_Not_Simple(
		const int nDimf,
		const int nTestf,
		const int nArrf[],

		int &nNumBelongsf); //'== 1' --> belongs and '-1' otherwise
	int
		nResf,
		nRanSelecCurf,
		nNumOfRandInitializingLoopsCurf = 0,

		nNumBelongsf,
		i1;

	double
		dRandFrom1To0fCurf;

	if (nNumOfSelecf > nDimf)
	{
		printf("\n\n An error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nNumOfSelecf = %d > nDimf = %d",
			nNumOfSelecf, nDimf);
		printf("\n\nThe number of selected vecs must be less or equal to the dimension");

		fprintf(fout, "\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nNumOfSelecf = %d > nDimf = %d",
			nNumOfSelecf, nDimf);

		fprintf(fout, "\n\nThe number of selected vecs must be less or equal to the dimension");

		getchar();	exit(1);
	} //if (nNumOfSelecf > nDimf)

	if (nNumOfSelecf == nDimf)
	{
		for (i1 = 0; i1 < nDimf; i1++)
			nPositOfIntArrf[i1] = i1;

		return SUCCESSFUL_RETURN;
	} //if (nNumOfSelecf == nDimf)

	for (i1 = 0; i1 < nNumOfSelecf; i1++)
		nPositOfIntArrf[i1] = -1;

	for (i1 = 0; i1 < nNumOfSelecf; i1++)
	{
		nNumOfRandInitializingLoopsCurf = 0;

		//MarkRandLoopf: nRanSelecCurf = (int)( nDimf*(float)( rand() )/(float)(RAND_MAX) );
	MarkRandLoopf: dRandFrom1To0fCurf = dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		l_Seedf); //long &l_Seedf);

				   nRanSelecCurf = (int)((double)(nDimf)*dRandFrom1To0fCurf);

				   if (nRanSelecCurf == nDimf)
				   {
					   nRanSelecCurf = nDimf - 1;
				   } //if (nRanSelecCurf == nDimf)
				   else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
				   {
					   printf("\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nRanSelecCurf = %d > nDimf = %d, ...",
						   nRanSelecCurf, nDimf);
					   fprintf(fout, "\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nRanSelecCurf = %d > nDimf = %d, ...",
						   nRanSelecCurf, nDimf);

					   getchar();	exit(1);
				   } // else if (nRanSelecCurf > nDimf || ...)

				   nResf = Int_Belongs_To_Arr_Or_Not_Simple(
					   nNumOfSelecf, //const int nDimf,

					   nRanSelecCurf, //const int nTestf,
					   nPositOfIntArrf, //const int nArrf[],

					   nNumBelongsf); //int &nNumBelongsf);

				   if (nNumBelongsf != 1)
					   nPositOfIntArrf[i1] = nRanSelecCurf;
				   else
				   {
					   nNumOfRandInitializingLoopsCurf += 1;
					   if (nNumOfRandInitializingLoopsCurf < nNumOfRandInitializingLoopsMaxf)
						   goto MarkRandLoopf;
					   else
					   {
						   printf("\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nNumOfRandInitializingLoopsCurf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
							   nNumOfRandInitializingLoopsCurf, nNumOfRandInitializingLoopsMaxf);
						   fprintf(fout, "\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap': nNumOfRandInitializingLoopsCurf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
							   nNumOfRandInitializingLoopsCurf, nNumOfRandInitializingLoopsMaxf);

						   getchar();	exit(1);
					   } //else

				   } //else

	} //for (i1 = 0; i1 < nNumOfSelecf; i1++)

	return SUCCESSFUL_RETURN;
} //int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(...
//////////////////////////////////////////

int Rand_Int_2DimArr_SelecWithMoreCombinats(

	const int nNumOfSelecCombinsf,

	const int nNumOfFeasf,
	const int nFeasMinf[], //[nNumOfFeasf]
	const int nFeasMaxf[], //[nNumOfFeasf]

	const int nNumOfRandInitializingLoopsMaxf,

	long &l_Seedf, //must be already specified

	int n2DimArrf[])  //[nNumOfSelecCombinsf*nNumOfFeasf]
{
	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(
		const int nDimf,
		const int nNumOfSelecf,

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified
		int nPositOfIntArrf[]);  //nNumOfSelecf

//double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
	//						long &l_Seedf);

	int
		//nResf,
		iFeaf,
		iCombinf,
		nRanSelecCurf,
		nDiffMaxMinCurf;
	//i1;

	double
		dRandFrom1To0fCurf;

	if (nNumOfSelecCombinsf < 1 || nNumOfFeasf < 1)
	{
		printf("\n\nAn error in 'Rand_Int_2DimArr_SelecWithMoreCombinats': nNumOfSelecCombinsf = %d || nNumOfFeasf = %d",
			nNumOfSelecCombinsf, nNumOfFeasf);
		fprintf(fout, "\n\nAn error in 'Rand_Int_2DimArr_SelecWithMoreCombinats': nNumOfSelecCombinsf = %d || nNumOfFeasf = %d",
			nNumOfSelecCombinsf, nNumOfFeasf);
		getchar();	exit(1);
	} //if (nNumOfSelecCombinsf < 1 || ...

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nDiffMaxMinCurf = nFeasMaxf[iFeaf] - nFeasMinf[iFeaf];

		for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
		{
			dRandFrom1To0fCurf = dRandNumGenerFrom0To1WithUpTo2billionCombinats(
				l_Seedf); //long &l_Seedf);

			nRanSelecCurf = (int)((double)(nDiffMaxMinCurf)*dRandFrom1To0fCurf);

			if (nRanSelecCurf == nDiffMaxMinCurf)
			{
				nRanSelecCurf = nDiffMaxMinCurf - 1;
			} //if (nRanSelecCurf == nDimf)
			else if (nRanSelecCurf > nDiffMaxMinCurf || nRanSelecCurf < 0)
			{
				printf("\n\nAn error in 'Rand_Int_2DimArr_SelecWithMoreCombinats': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
					nRanSelecCurf, nDiffMaxMinCurf);
				fprintf(fout, "\n\nAn error in 'Rand_Int_2DimArr_SelecWithMoreCombinats': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
					nRanSelecCurf, nDiffMaxMinCurf);

				getchar();	exit(1);
			} // else if (nRanSelecCurf > nDiffMaxMinCurf || ...)

			n2DimArrf[iFeaf + (iCombinf*nNumOfFeasf)] = nFeasMinf[iFeaf] + nRanSelecCurf;

		} // for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)


	return SUCCESSFUL_RETURN;
} //int Rand_Int_2DimArr_SelecWithMoreCombinats(...
//////////////////////////////////////////

int Rand_IntArr_SelecWithMoreCombinats(

	const int nNumOfFeasf,

	const int nFeasMinf[], //[nNumOfFeasf]
	const int nFeasMaxf[], //[nNumOfFeasf]

	const int nNumOfRandInitializingLoopsMaxf,

	long &l_Seedf, //must be already specified

	int nFeaArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]
{
	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(
		const int nDimf,
		const int nNumOfSelecf,

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified
		int nPositOfIntArrf[]);  //nNumOfSelecf

//double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
	//						long &l_Seedf);

	int
		iFeaf,
		nRanSelecCurf,
		nDiffMaxMinCurf;

	double
		dRandFrom1To0fCurf;

	if (nNumOfFeasf < 1)
	{
		printf("\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinats': nNumOfFeasf = %d", nNumOfFeasf);
		fprintf(fout, "\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinats': nNumOfFeasf = %d", nNumOfFeasf);
		getchar();	exit(1);
	} //if (nNumOfSelecCombinsf < 1 || ...
	
	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nDiffMaxMinCurf = nFeasMaxf[iFeaf] - nFeasMinf[iFeaf];

		dRandFrom1To0fCurf = dRandNumGenerFrom0To1WithUpTo2billionCombinats(
			l_Seedf); //long &l_Seedf);

		nRanSelecCurf = (int)((double)(nDiffMaxMinCurf)*dRandFrom1To0fCurf);

		if (nRanSelecCurf == nDiffMaxMinCurf)
		{
			nRanSelecCurf = nDiffMaxMinCurf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDiffMaxMinCurf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinats': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
				nRanSelecCurf, nDiffMaxMinCurf);
			fprintf(fout, "\n\nAn error in 'Rand_IntArr_SelecWithMoreCombinats': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
				nRanSelecCurf, nDiffMaxMinCurf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDiffMaxMinCurf || ...)

		nFeaArrf[iFeaf] = nFeasMinf[iFeaf] + nRanSelecCurf;

	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Rand_IntArr_SelecWithMoreCombinats(...
//////////////////////////////////////////

int Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap(
	const int nDimf,
	const int nIndexOfAlreadySelecf,

	const int nNumOfRandInitializingLoopsMaxf,

	long &l_Seedf, //must be already specified
	int &nIndexOfNextSelecf)  //nNumOfSelecf
{
	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int
		nRanSelecCurf,
		nNumOfRandInitializingLoopsCurf = 0,

		i1;

	double
		dRandFrom1To0fCurf;

	if (nIndexOfAlreadySelecf > nDimf)
	{
		printf("\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nIndexOfAlreadySelecf = %d > nDimf = %d",
			nIndexOfAlreadySelecf, nDimf);
		printf("\n\nThe number of selected vecs must be less or equal to the dimension");

		fprintf(fout, "\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nIndexOfAlreadySelecf = %d > nDimf = %d",
			nIndexOfAlreadySelecf, nDimf);

		fprintf(fout, "\n\nThe number of selected vecs must be less or equal to the dimension");

		getchar();	exit(1);
	} //if (nIndexOfAlreadySelecf > nDimf)

	for (i1 = 0; i1 < nNumOfRandInitializingLoopsMaxf; i1++)
	{
		nNumOfRandInitializingLoopsCurf = 0;

		//MarkRandLoopf: nRanSelecCurf = (int)( nDimf*(float)( rand() )/(float)(RAND_MAX) );
	MarkRandLoopf: dRandFrom1To0fCurf = dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		l_Seedf); //long &l_Seedf);

				   nRanSelecCurf = (int)((double)(nDimf)*dRandFrom1To0fCurf);

				   if (nRanSelecCurf == nDimf)
				   {
					   nRanSelecCurf = nDimf - 1;
				   } //if (nRanSelecCurf == nDimf)
				   else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
				   {
					   printf("\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nRanSelecCurf = %d > nDimf = %d, ...",
						   nRanSelecCurf, nDimf);
					   fprintf(fout, "\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nRanSelecCurf = %d > nDimf = %d, ...",
						   nRanSelecCurf, nDimf);

					   getchar();	exit(1);
				   } // else if (nRanSelecCurf > nDimf || ...)

				   if (nRanSelecCurf != nIndexOfAlreadySelecf)
				   {
					   nIndexOfNextSelecf = nRanSelecCurf;
					   break;
				   } //if (nRanSelecCurf != nIndexOfAlreadySelecf)
				   else
				   {
					   nNumOfRandInitializingLoopsCurf += 1;
					   if (nNumOfRandInitializingLoopsCurf < nNumOfRandInitializingLoopsMaxf)
						   goto MarkRandLoopf;
					   else
					   {
						   printf("\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nNumOfRandInitializingLoopsCurf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
							   nNumOfRandInitializingLoopsCurf, nNumOfRandInitializingLoopsMaxf);
						   fprintf(fout, "\n\nAn error in 'Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap': nNumOfRandInitializingLoopsCurf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
							   nNumOfRandInitializingLoopsCurf, nNumOfRandInitializingLoopsMaxf);

						   getchar();	exit(1);
					   } //else

				   } //else

	} //for (i1 = 0; i1 < nNumOfRandInitializingLoopsMaxf; i1++)

	return SUCCESSFUL_RETURN;
} //int Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap(...
//////////////////////////////////////////

int StochHillClimbing_ColorSeparability(

	const int nNumOfFeasf,

	const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
	const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]
//////////////////////////////////////////

	const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

	const int nNumOfRandItersforRestartf,
	const int nNumOfRandItersforLocalPerturbf,
	////////////////////////////////////////////////////

	//for Func_BySeparabilityOf_2_ColorIntervals
	const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
	const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

	const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
///////////////////////////////////////////

	long &l_Seedf, //must be already specified

	const float fFuncValueInitf,
	const int nFeaInitArrf[],   //[nNumOfFeasf]
/////////////////////////////////////////////////////////////
	float &fFuncValueAfterHillClimbf,
	int nFeaAfterHillClimbArrf[])   //[nNumOfFeasf]
{
/*
	int fFuncForTest(
		const int nNumOfFeasf,

		const int nOneDimArrf[],
		float &fFuncValuef);   //[nNumOfFeasf]
*/
	int Copying_Adjusting_OneDimFeaVec_To_Intervals(
		const int nNumOfFeasf,

		int nOneDimArrf[], //[nNumOfFeasf]

		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int NonOverlapping_3ColorIntervals(
		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int Func_BySeparabilityOf_2_ColorIntervals(
		const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

		float &fBestBinSeparabilityBy_2_Intervalsf);

	int DistancesOfFeasToLeftAndRight(
		const int nNumOfFeasf,

		const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

		const int nOneDimArrf[],

		int nFeaPositionToLeftArrf[], //[nNumOfFeasf]
		int nFeaPositionToRightArr[]); //[nNumOfFeasf]

	int LocalPerturbationOf_1DimintArr(

		const int nNumOfFeasf,

		const int nFeaPositionToLeftArrf[], //[nNumOfFeasf]

		const int nFeaPositionToRightArrf[], //[nNumOfFeasf]

		const int nFeaInitArrf[], //[nNumOfFeasf]

		long &l_Seedf, //must be already specified

		int nFeaPerturbArrf[]);  //[nNumOfFeasf]

	int CopyIntArr(
		const int nNumOfFeasf,
		const int nFeaInitArrf[], //[nNumOfFeasf]
		int nFeaArrf[]); //[nNumOfFeasf]

	int Printing_Boundaries_Of_Intervals(
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);
//////////////////////////////////

	int
		nResf,
		nNumOfItersForPerturbsf,

		iFeaf,
		iRestartf,
		iLocalPerturbf;

	float
		//fFuncValueCurf,

		fBestBinSeparabilityBy_2_Intervalsf,
		fFuncValueAfterHillClimbforPerturbCurtf,

		fFuncValueAfterHillClimbforRestarInitf,
		fFuncValueAfterHillClimbforRestarPrevf,

		fFuncValueAfterHillClimbforRestarCurtf;

	int *nFeaCurArrf = new int[nNumOfFeasf];
	assert(nFeaCurArrf);

	int *nFeaForRestartArrf = new int[nNumOfFeasf];
	assert(nFeaForRestartArrf);

	int *nFeaInitForRestartArrf = new int[nNumOfFeasf];
	assert(nFeaInitForRestartArrf);

	int *nFeaPerturbArrf = new int[nNumOfFeasf];
	assert(nFeaPerturbArrf);

	int *nDistInitToLeftArrf = new int[nNumOfFeasf];
	assert(nDistInitToLeftArrf);

	int *nDistInitToRightArrf = new int[nNumOfFeasf];
	assert(nDistInitToRightArrf);

	int *nDistOfFeasFromRestartToLeftArrf = new int[nNumOfFeasf];
	assert(nDistOfFeasFromRestartToLeftArrf);

	int *nDistOfFeasFromRestartToRightArr = new int[nNumOfFeasf];
	assert(nDistOfFeasFromRestartToRightArr);

////////////////////////////////////////////////////
	BOUNDARIES_OF_INTERVALS sBoundaries_Of_Intervalsf;
////////////////////////////////////////////////////
	fFuncValueAfterHillClimbf = fFuncValueInitf;

	nResf = CopyIntArr(
		nNumOfFeasf, //const int nNumOfFeasf,
		nFeaInitArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]
		nFeaAfterHillClimbArrf); //int nFeaArrf[]) //[nNumOfFeasf]

	nResf = DistancesOfFeasToLeftAndRight(
		nNumOfFeasf, //const int nNumOfFeasf,

		nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

		nFeaInitArrf, //const int nOneDimArrf[],

		nDistInitToLeftArrf, //int nDistInitToLeftArrf[], //[nNumOfFeasf]
		nDistInitToRightArrf); //int nDistInitToRightArrf[]); //[nNumOfFeasf]

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
	fprintf(fout, "\n\n 'StochHillClimbing_ColorSeparability':  init. fFuncValueAfterHillClimbf = fFuncValueInitf = %E",
		fFuncValueAfterHillClimbf);

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		fprintf(fout, "\n Init. nFeaAfterHillClimbArrf[%d] = %d", iFeaf, nFeaAfterHillClimbArrf[iFeaf]);
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

	///////////////////////////////////////////
	for (iRestartf = 0; iRestartf < nNumOfRandItersforRestartf; iRestartf++)
	{
		nNumOfItersForPerturbsf = 0;
		nResf = LocalPerturbationOf_1DimintArr(

			nNumOfFeasf, //const int nNumOfFeasf,

			nDistInitToLeftArrf, //const int nDistInitToLeftArrf[], //[nNumOfFeasf]
			nDistInitToRightArrf, //const int nDistInitToRightArrf[], //[nNumOfFeasf]

			nFeaInitArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

			l_Seedf, //ong &l_Seedf, //must be already specified

			nFeaForRestartArrf); //int nFeaPerturbArrf[]);  //[nNumOfFeasf]

		nResf = CopyIntArr(
			nNumOfFeasf, //const int nNumOfFeasf,
			nFeaForRestartArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

			nFeaInitForRestartArrf); //int nFeaArrf[]) //[nNumOfFeasf]

		nResf = DistancesOfFeasToLeftAndRight(
			nNumOfFeasf, //const int nNumOfFeasf,

			nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
			nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

			nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

			nFeaForRestartArrf, //const int nOneDimArrf[],

			nDistOfFeasFromRestartToLeftArrf, //int nDistOfFeasFromRestartToLeftArrf[], //[nNumOfFeasf]
			nDistOfFeasFromRestartToRightArr); //int nDistOfFeasFromRestartToRightArr[]); //[nNumOfFeasf]

/*
		nResf = fFuncForTest(
			nNumOfFeasf, //const int nNumOfFeasf,

			nFeaForRestartArrf, //const int nOneDimArrf[],

			fFuncValueAfterHillClimbforRestarInitf); //float &fFuncValuef);   //[nNumOfFeasf]
*/
		nResf = Copying_Adjusting_OneDimFeaVec_To_Intervals(
			nNumOfFeas, //const int nNumOfFeasf,

			nFeaForRestartArrf, // int nOneDimArrf[], //[nNumOfFeasf]

			&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

		nResf = NonOverlapping_3ColorIntervals(
				&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

		nResf = Func_BySeparabilityOf_2_ColorIntervals(
			nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
			nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

			&sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
			nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

			fBestBinSeparabilityBy_2_Intervalsf); // float &fBestBinSeparabilityBy_2_Intervalsf);
//////////////////////////////////////////
		fFuncValueAfterHillClimbforRestarInitf = fBestBinSeparabilityBy_2_Intervalsf;

		if (fBestBinSeparabilityBy_2_Intervalsf < fFuncValueBest_Glob)
		{
			fprintf(fout, "\n\n Printing the boundaries in 'StochHillClimbing_ColorSeparability' 1: iIterForCuckSear_Glob = %d", iIterForCuckSear_Glob);
			fprintf(fout, "\n fFuncValueAfterHillClimbforRestarInitf = %E < fFuncValueBest_Glob = %E", fFuncValueAfterHillClimbforRestarInitf, fFuncValueBest_Glob);
			nResf = Printing_Boundaries_Of_Intervals(
				&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
		} //if (fBestBinSeparabilityBy_2_Intervalsf < fFuncValueBest_Glob)

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

		fprintf(fout, "\n\n 'StochHillClimbing_ColorSeparability' before 'iLocalPerturbf':  fFuncValueAfterHillClimbforRestarInitf = %E, iRestartf = %d",
			fFuncValueAfterHillClimbforRestarInitf, iRestartf);

		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFeaForRestartArrf[%d] = %d", iFeaf, nFeaForRestartArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

		if (fFuncValueAfterHillClimbforRestarInitf < fFuncValueAfterHillClimbf)
		{
			fFuncValueAfterHillClimbf = fFuncValueAfterHillClimbforRestarInitf;

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
			fprintf(fout, "\n\n 'StochHillClimbing_ColorSeparability': a new fFuncValueAfterHillClimbf = %E, iRestartf = %d, nIndexForRandSelecCombin_Glob = %d",
				fFuncValueAfterHillClimbf, iRestartf, nIndexForRandSelecCombin_Glob);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

			nResf = CopyIntArr(
				nNumOfFeasf, //const int nNumOfFeasf,
				nFeaForRestartArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

				nFeaAfterHillClimbArrf); //int nFeaArrf[]) //[nNumOfFeasf]

		} //if (fFuncValueAfterHillClimbforRestarInitf < fFuncValueAfterHillClimbf)

	/////////////////////////////////////////////////////////////////////
		fFuncValueAfterHillClimbforRestarPrevf = fFuncValueAfterHillClimbforRestarInitf;

		fFuncValueAfterHillClimbforRestarCurtf = fFuncValueAfterHillClimbforRestarInitf;

	MarkTheNextIterForPerturb: nNumOfItersForPerturbsf += 1;

		for (iLocalPerturbf = 0; iLocalPerturbf < nNumOfRandItersforLocalPerturbf; iLocalPerturbf++)
		{

			nResf = LocalPerturbationOf_1DimintArr(
				nNumOfFeasf, //const int nNumOfFeasf,

				nDistOfFeasFromRestartToLeftArrf, //const int nDistInitToLeftArrf[], //[nNumOfFeasf]
				nDistOfFeasFromRestartToRightArr, //const int nDistInitToRightArrf[], //[nNumOfFeasf]

				nFeaForRestartArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

				l_Seedf, //ong &l_Seedf, //must be already specified

				nFeaPerturbArrf); //int nFeaPerturbArrf[]);  //[nNumOfFeasf]

/*
			nResf = fFuncForTest(
				nNumOfFeasf, //const int nNumOfFeasf,

				nFeaPerturbArrf, //const int nOneDimArrf[],

				fFuncValueAfterHillClimbforPerturbCurtf); //float &fFuncValuef);   //[nNumOfFeasf]
*/
			nResf = Copying_Adjusting_OneDimFeaVec_To_Intervals(
				nNumOfFeas, //const int nNumOfFeasf,

				nFeaPerturbArrf, //int nOneDimArrf[], //[nNumOfFeasf]

				&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

			nResf = NonOverlapping_3ColorIntervals(
				&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

			nResf = Func_BySeparabilityOf_2_ColorIntervals(
				nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
				nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

				&sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

				nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
				nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

				fBestBinSeparabilityBy_2_Intervalsf); // float &fBestBinSeparabilityBy_2_Intervalsf);
//////////////////////////////////////////////////////

			fFuncValueAfterHillClimbforPerturbCurtf = fBestBinSeparabilityBy_2_Intervalsf;

			if (fBestBinSeparabilityBy_2_Intervalsf < fFuncValueBest_Glob)
			{
				fprintf(fout, "\n\n Printing the boundaries in 'StochHillClimbing_ColorSeparability' 2: iIterForCuckSear_Glob = %d", iIterForCuckSear_Glob);
				fprintf(fout, "\n fFuncValueAfterHillClimbforPerturbCurtf = %E < fFuncValueBest_Glob = %E", fFuncValueAfterHillClimbforPerturbCurtf, fFuncValueBest_Glob);
				nResf = Printing_Boundaries_Of_Intervals(
					&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
			} //if (fBestBinSeparabilityBy_2_Intervalsf < fFuncValueBest_Glob)

///////////////////////////////////////////////////////////
#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
			fprintf(fout, "\n\n 'StochHillClimbing_ColorSeparability': iLocalPerturbf = %d,   fFuncValueAfterHillClimbforPerturbCurtf = %E, iRestartf = %d",
				iLocalPerturbf, fFuncValueAfterHillClimbforPerturbCurtf, iRestartf);

			for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
			{
				fprintf(fout, "\n nFeaPerturbArrf[%d] = %d, nFeaForRestartArrf[%d] = %d",
					iFeaf, nFeaPerturbArrf[iFeaf], iFeaf, nFeaForRestartArrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

			if (fFuncValueAfterHillClimbforPerturbCurtf < fFuncValueAfterHillClimbforRestarCurtf)
			{
				fFuncValueAfterHillClimbforRestarCurtf = fFuncValueAfterHillClimbforPerturbCurtf;

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
				fprintf(fout, "\n\n'StochHillClimbing_ColorSeparability': a new fFuncValueAfterHillClimbforRestarCurtf = %E, fFuncValueAfterHillClimbforRestarInitf = %E, fFuncValueAfterHillClimbf = %E",
					fFuncValueAfterHillClimbforRestarCurtf, fFuncValueAfterHillClimbforRestarInitf, fFuncValueAfterHillClimbf);

				fprintf(fout, "\n iLocalPerturbf = %d, nNumOfItersForPerturbsf = %d, iRestartf = %d, nIndexForRandSelecCombin_Glob = %d",
					iLocalPerturbf, nNumOfItersForPerturbsf, iRestartf, nIndexForRandSelecCombin_Glob);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

				nResf = CopyIntArr(
					nNumOfFeasf, //const int nNumOfFeasf,

					nFeaPerturbArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

					nFeaInitForRestartArrf); //int nFeaArrf[]) //[nNumOfFeasf]

				if (fFuncValueAfterHillClimbforPerturbCurtf < fFuncValueAfterHillClimbf)
				{
					fFuncValueAfterHillClimbf = fFuncValueAfterHillClimbforPerturbCurtf;

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
					fprintf(fout, "\n\n 'StochHillClimbing_ColorSeparability': a new fFuncValueAfterHillClimbf = %E, iLocalPerturbf = %d, iRestartf = %d, nIndexForRandSelecCombin_Glob = %d",
						fFuncValueAfterHillClimbf, iLocalPerturbf, iRestartf, nIndexForRandSelecCombin_Glob);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

					nResf = CopyIntArr(
						nNumOfFeasf, //const int nNumOfFeasf,
						nFeaPerturbArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

						nFeaAfterHillClimbArrf); //int nFeaArrf[]) //[nNumOfFeasf]

				} //if (fFuncValueAfterHillClimbforPerturbCurtf < fFuncValueAfterHillClimbf)

			} //if (fFuncValueAfterHillClimbforPerturbCurtf < fFuncValueAfterHillClimbforRestarCurtf)

		} // for (iLocalPerturbf = 0; iLocalPerturbf < nNumOfRandItersforLocalPerturbf; iLocalPerturbf++)

		if (fFuncValueAfterHillClimbforRestarCurtf < fFuncValueAfterHillClimbforRestarPrevf)
		{
			fFuncValueAfterHillClimbforRestarPrevf = fFuncValueAfterHillClimbforRestarCurtf;

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
			fprintf(fout, "\n\n'StochHillClimbing_ColorSeparability': a new fFuncValueAfterHillClimbforRestarPrevf = %E, prev. nNumOfItersForPerturbsf = %d, iRestartf = %d, nIndexForRandSelecCombin_Glob = %d",
				fFuncValueAfterHillClimbforRestarPrevf, nNumOfItersForPerturbsf, iRestartf, nIndexForRandSelecCombin_Glob);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

			nResf = CopyIntArr(
				nNumOfFeasf, //const int nNumOfFeasf,
				nFeaInitForRestartArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]

				nFeaForRestartArrf); //int nFeaArrf[]) //[nNumOfFeasf]

			nResf = DistancesOfFeasToLeftAndRight(
				nNumOfFeasf, //const int nNumOfFeasf,

				nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
				nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

				nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]
				nFeaInitForRestartArrf, //const int nOneDimArrf[],

				nDistOfFeasFromRestartToLeftArrf, //int nDistOfFeasFromRestartToLeftArrf[], //[nNumOfFeasf]
				nDistOfFeasFromRestartToRightArr); //int nDistOfFeasFromRestartToRightArr[]); //[nNumOfFeasf]

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
			fprintf(fout, "\nGoing to 'MarkTheNextIterForPerturb', nNumOfItersForPerturbsf = %d", nNumOfItersForPerturbsf);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

			goto MarkTheNextIterForPerturb;
		} //if (fFuncValueAfterHillClimbforRestarCurtf < fFuncValueAfterHillClimbforRestarPrevf)
		else
		{

#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability
			fprintf(fout, "\n\n'StochHillClimbing_ColorSeparability': no improvement for fFuncValueAfterHillClimbforRestarPrevf = %E --continue for the next restart, iRestartf = %d, nNumOfRandItersforRestartf = %d, nIndexForRandSelecCombin_Glob = %d",
				fFuncValueAfterHillClimbforRestarPrevf, iRestartf, nNumOfRandItersforRestartf, nIndexForRandSelecCombin_Glob);
#endif //#ifndef SKIPPNG_PRINTING_IN_StochHillClimbing_ColorSeparability

		} //else

	} //for (iRestartf = 0; iRestartf < nNumOfRandItersforRestartf; iRestartf++)

	delete[] nFeaCurArrf;
	delete[] nFeaForRestartArrf;
	delete[] nFeaInitForRestartArrf;

	delete[] nFeaPerturbArrf;

	delete[] nDistInitToLeftArrf;
	delete[] nDistInitToRightArrf;

	delete[] nDistOfFeasFromRestartToLeftArrf;
	delete[] nDistOfFeasFromRestartToRightArr;

	return SUCCESSFUL_RETURN;
} // int StochHillClimbing_ColorSeparability(...
//////////////////////////////////////////

/*
int fFuncForTest(
	const int nNumOfFeasf,

	const int nOneDimArrf[],
	float &fFuncValuef)   //[nNumOfFeasf]
{
	int
		iFeaf;

	fFuncValuef = 0.0;

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		fFuncValuef += (float)(nOneDimArrf[iFeaf] * nOneDimArrf[iFeaf])*0.1;
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int fFuncForTest(....
*/
//////////////////////////////////////////

int LocalPerturbationOf_1DimintArr(

	const int nNumOfFeasf,

	const int nFeaPositionToLeftArrf[], //[nNumOfFeasf]

	const int nFeaPositionToRightArrf[], //[nNumOfFeasf]

	const int nFeaInitArrf[], //[nNumOfFeasf]

	long &l_Seedf, //must be already specified

	int nFeaPerturbArrf[])  //[nNumOfFeasf]
{
	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(
		const int nDimf,
		const int nNumOfSelecf,

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified
		int nPositOfIntArrf[]);  //nNumOfSelecf

	int
		//nResf,
		iFeaf,

		nRanSelecCurf,
		nDiffMaxMinCurf;

	double
		dRandFrom1To0fCurf;

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nDiffMaxMinCurf = nFeaPositionToRightArrf[iFeaf] - nFeaPositionToLeftArrf[iFeaf];

#ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr
		fprintf(fout, "\n\n 'LocalPerturbationOf_1DimintArr': nDiffMaxMinCurf = %d, iFeaf = %d",
			nDiffMaxMinCurf, iFeaf);

		fprintf(fout, "\n nFeaInitArrf[%d] = %d, nFeaPositionToRightArrf[%d] = %d, nFeaPositionToLeftArrf[%d] = %d",
			iFeaf, nFeaInitArrf[iFeaf], iFeaf, nFeaPositionToRightArrf[iFeaf], iFeaf, nFeaPositionToLeftArrf[iFeaf]);
#endif // #ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr

		if (nDiffMaxMinCurf < 0)
		{
			printf("\n\nAn error in 'LocalPerturbationOf_1DimintArr': nDiffMaxMinCurf = %d < 0, iFeaf = %d",
				nDiffMaxMinCurf, iFeaf);
			printf("\n\n nFeaPositionToRightArrf[iFeaf] = %d, nFeaPositionToLeftArrf[iFeaf] = %d",
				nFeaPositionToRightArrf[iFeaf], nFeaPositionToLeftArrf[iFeaf]);


			fprintf(fout, "\n\nAn error in 'LocalPerturbationOf_1DimintArr': nDiffMaxMinCurf = %d < 0, iFeaf = %d",
				nDiffMaxMinCurf, iFeaf);
			fprintf(fout, "\n\n nFeaPositionToRightArrf[iFeaf] = %d, nFeaPositionToLeftArrf[iFeaf] = %d",
				nFeaPositionToRightArrf[iFeaf], nFeaPositionToLeftArrf[iFeaf]);

			getchar();	exit(1);
		} //if (nDiffMaxMinCurf < 0)

		dRandFrom1To0fCurf = dRandNumGenerFrom0To1WithUpTo2billionCombinats(
			l_Seedf); //long &l_Seedf);

		nRanSelecCurf = (int)((double)(nDiffMaxMinCurf)*dRandFrom1To0fCurf);

#ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr
		fprintf(fout, "\n dRandFrom1To0fCurf = %E, nRanSelecCurf = %d", dRandFrom1To0fCurf, nRanSelecCurf);
#endif // #ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr

		if (nRanSelecCurf == nDiffMaxMinCurf)
		{
			//nRanSelecCurf = nDiffMaxMinCurf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDiffMaxMinCurf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'LocalPerturbationOf_1DimintArr': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
				nRanSelecCurf, nDiffMaxMinCurf);
			fprintf(fout, "\n\nAn error in 'LocalPerturbationOf_1DimintArr': nRanSelecCurf = %d > nDiffMaxMinCurf = %d, ...",
				nRanSelecCurf, nDiffMaxMinCurf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDiffMaxMinCurf || ...)

//nFeaPerturbArrf[iFeaf] = nFeaInitArrf[iFeaf] - nFeaPositionToLeftArrf[iFeaf] + nRanSelecCurf;
		nFeaPerturbArrf[iFeaf] = nFeaPositionToLeftArrf[iFeaf] + nRanSelecCurf;

#ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr
		fprintf(fout, "\n nFeaPerturbArrf[%d] = %d, nFeaInitArrf[%d] = %d, nFeaPositionToLeftArrf[%d] = %d",
			iFeaf, nFeaPerturbArrf[iFeaf], iFeaf, nFeaInitArrf[iFeaf], iFeaf, nFeaPositionToLeftArrf[iFeaf]);
#endif // #ifndef SKIPPNG_PRINTING_IN_LocalPerturbationOf_1DimintArr

	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int LocalPerturbationOf_1DimintArr(...
//////////////////////////////////////////

int DistancesOfFeasToLeftAndRight(
	const int nNumOfFeasf,

	const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
	const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

	const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

	const int nOneDimArrf[],

	int nFeaPositionToLeftArrf[], //[nNumOfFeasf]
	int nFeaPositionToRightArr[])//[nNumOfFeasf]

{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nFeaPositionToLeftArrf[iFeaf] = nOneDimArrf[iFeaf] - nLocalRadiusOfFeaNeighArrf[iFeaf];

		if (nFeaPositionToLeftArrf[iFeaf] < nFeaWholeRegionMinArrf[iFeaf])
			nFeaPositionToLeftArrf[iFeaf] = nFeaWholeRegionMinArrf[iFeaf];

		nFeaPositionToRightArr[iFeaf] = nOneDimArrf[iFeaf] + nLocalRadiusOfFeaNeighArrf[iFeaf];

		if (nFeaPositionToRightArr[iFeaf] > nFeaWholeRegionMaxArrf[iFeaf])
			nFeaPositionToRightArr[iFeaf] = nFeaWholeRegionMaxArrf[iFeaf];

	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int DistancesOfFeasToLeftAndRight(....
//////////////////////////////////////////

int CopyIntArr(
	const int nNumOfFeasf,
	const int nFeaInitArrf[], //[nNumOfFeasf]
	int nFeaArrf[]) //[nNumOfFeasf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nFeaArrf[iFeaf] = nFeaInitArrf[iFeaf];
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} // int CopyIntArr(
//////////////////////////////////////////

int CopySelecIntArrFrom_2DimIntArr(
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	const int nSelecCombinsf,

	const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

	int nFeaArrf[]) //[nNumOfFeasf]
{
	int
		iFeaf;

	if (nSelecCombinsf < 0 || nSelecCombinsf > nNumOfSelecCombinsf - 1)
	{
		printf("\nAn error in 'CopySelecIntArrFrom_2DimIntArr': nSelecCombinsf = %d > nNumOfSelecCombinsf - 1 = %d || ....",
			nSelecCombinsf, nNumOfSelecCombinsf - 1);
		fprintf(fout, "\nAn error in 'CopySelecIntArrFrom_2DimIntArr': nSelecCombinsf = %d > nNumOfSelecCombinsf - 1 = %d || ....",
			nSelecCombinsf, nNumOfSelecCombinsf - 1);

		getchar();	exit(1);
	} //if (nSelecCombinsf < 0 || nSelecCombinsf ...

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nFeaArrf[iFeaf] = nFea2DimArrf[iFeaf + (nSelecCombinsf*nNumOfFeasf)];
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} // int CopySelecIntArrFrom_2DimIntArr(...
//////////////////////////////////////////

int CopyIntArr_Into_2DimIntArr(
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	const int nSelecCombinsf,
	const int nFeaArrf[],
	////////////////////////////////////
	int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]
{
	int
		iFeaf;

	if (nSelecCombinsf < 0 || nSelecCombinsf > nNumOfSelecCombinsf - 1)
	{
		printf("\nAn error in 'CopyIntArr_Into_2DimIntArr': nSelecCombinsf = %d > nNumOfSelecCombinsf - 1 = %d || ....",
			nSelecCombinsf, nNumOfSelecCombinsf - 1);
		fprintf(fout, "\nAn error in 'CopyIntArr_Into_2DimIntArr': nSelecCombinsf = %d > nNumOfSelecCombinsf - 1 = %d || ....",
			nSelecCombinsf, nNumOfSelecCombinsf - 1);

		getchar();	exit(1);
	} //if (nSelecCombinsf < 0 || nSelecCombinsf ...

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nFea2DimArrf[iFeaf + (nSelecCombinsf*nNumOfFeasf)] = nFeaArrf[iFeaf];
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	return SUCCESSFUL_RETURN;
} // int CopyIntArr_Into_2DimIntArr(...
//////////////////////////////////////////

int ValuesOf2DimFeaArr(
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]
////////////////////////////////////////////////////
		//for Func_BySeparabilityOf_2_ColorIntervals
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
///////////////////////////////////////////

	float fFuncValueOfCombinsArrf[]) //[nNumOfSelecCombinsf]
{
	int Copying_Adjusting_OneDimFeaVec_To_Intervals(
		const int nNumOfFeasf,

		int nOneDimArrf[], //[nNumOfFeasf]

		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int NonOverlapping_3ColorIntervals(
		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	void Rearranging_nFea2DimArrf_ForColor_Separability(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		int nFea2DimArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

	int Printing_Boundaries_Of_Intervals(
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int CopySelecIntArrFrom_2DimIntArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nSelecCombinsf,

		const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

		int nFeaArrf[]); //[nNumOfFeasf]

	int Func_BySeparabilityOf_2_ColorIntervals(
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

		float &fBestBinSeparabilityBy_2_Intervalsf);
/*
	int fFuncForTest(
		const int nNumOfFeasf,

		const int nOneDimArrf[],
		float &fFuncValuef);   //[nNumOfFeasf]
*/


	int
		nResf,
		iFeaf,
		iCombinf;

	float
		fBestBinSeparabilityBy_2_Intervalsf,
		fFuncValueCurf;
/////////////////////////////////////
	printf("\n\n Beginning of 'ValuesOf2DimFeaArr': nNumOfFeasf = %d, nNumOfSelecCombinsf = %d", nNumOfFeasf, nNumOfSelecCombinsf);

	Rearranging_nFea2DimArrf_ForColor_Separability(
		nNumOfFeasf, //const int nNumOfFeasf,
		nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf,

		nFea2DimArrf); // int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]

	printf("\n\n After 'Rearranging_nFea2DimArrf_ForColor_Separability' in 'ValuesOf2DimFeaArr'");

///////////////////////////////////
	int *nFeaCurArrf = new int[nNumOfFeasf];
	assert(nFeaCurArrf != 0);

	BOUNDARIES_OF_INTERVALS sBoundaries_Of_Intervalsf;

	for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
	{
		nResf = CopySelecIntArrFrom_2DimIntArr(
			nNumOfFeasf, //const int nNumOfFeasf,
			nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

			iCombinf, // int nSelecCombinsf, 

			nFea2DimArrf, //const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

			nFeaCurArrf); //int nFeaArrf[]); //[nNumOfFeasf]
/*
		nResf = fFuncForTest(
			nNumOfFeasf, //const int nNumOfFeasf,

			nFeaCurArrf, //const int nOneDimArrf[],
			fFuncValueCurf); //float &fFuncValuef);   //[nNumOfFeasf]
*/

		nResf = Copying_Adjusting_OneDimFeaVec_To_Intervals(
			nNumOfFeas, //const int nNumOfFeasf,

			nFeaCurArrf, //int nOneDimArrf[], //[nNumOfFeasf]

			&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

		nResf = NonOverlapping_3ColorIntervals(
			&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

		nResf = Func_BySeparabilityOf_2_ColorIntervals(
			nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArr[], //[nNumOfVecs_Normal],
			nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArr[], //[nNumOfVecs_Malignant],

			&sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

			nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArr[], //[nNumOfBins_AllColorsForAll_NorImages]
			nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArr[], //[nNumOfBins_AllColorsForAll_MalImages]

			fBestBinSeparabilityBy_2_Intervalsf); // float &fBestBinSeparabilityBy_2_Intervalsf);

///////////////////////////
		fFuncValueCurf = fBestBinSeparabilityBy_2_Intervalsf;
//////////////////////
		fFuncValueOfCombinsArrf[iCombinf] = fFuncValueCurf;

#ifndef SKIPPING_PRINTING_IN_ValuesOf2DimFeaArr
		printf( "\n\n 'ValuesOf2DimFeaArr':  fFuncValueOfCombinsArrf[%d] = %E",
			iCombinf, fFuncValueOfCombinsArrf[iCombinf]);

		fprintf(fout, "\n\n 'ValuesOf2DimFeaArr':  fFuncValueOfCombinsArrf[%d] = %E",
			iCombinf, fFuncValueOfCombinsArrf[iCombinf]);

		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFeaCurArrf[%d] = %d", iFeaf, nFeaCurArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		
	nResf = Printing_Boundaries_Of_Intervals(
			&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
	
#endif // #ifndef SKIPPING_PRINTING_IN_ValuesOf2DimFeaArr

	} //for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)

	delete[] nFeaCurArrf;

	//printf("\n\n The end of 'ValuesOf2DimFeaArr': please press any key"); fflush(fout);  getchar();

	return SUCCESSFUL_RETURN;
} //int ValuesOf2DimFeaArr(...
//////////////////////////////////////////

int BestValueByMinInFloatArr(
	const int nLargef,

	const int nNumOfSelecCombinsf,

	const float fFuncValueOfCombinsArrf[],

	int &nIndexOfBestCombinf,
	float &fValueOfBestCombinf) //[nNumOfSelecCombinsf]
{
	int
		//nResf,
		iCombinf;

	fValueOfBestCombinf = (float)(nLargef);
	for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
	{
		//Min for fBest
		if (fFuncValueOfCombinsArrf[iCombinf] < fValueOfBestCombinf)
		{
			fValueOfBestCombinf = fFuncValueOfCombinsArrf[iCombinf];
			nIndexOfBestCombinf = iCombinf;
		} //if (fFuncValueOfCombinsArrf[iCombinf] < fValueOfBestCombinf)

	} //for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)

	return SUCCESSFUL_RETURN;
} //int BestValueByMinInFloatArr(...
//////////////////////////////////////////

int WorstValueByMaxInFloatArr(
	const int nLargef,

	const int nNumOfSelecCombinsf,

	const float fFuncValueOfCombinsArrf[],

	int &nIndexOfWorstCombinf,
	float &fValueOfWorstCombinf) //[nNumOfSelecCombinsf]
{
	int
		//nResf,
		iCombinf;

	fValueOfWorstCombinf = -(float)(nLargef);
	for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
	{

		//Max for Worst
		if (fFuncValueOfCombinsArrf[iCombinf] > fValueOfWorstCombinf)
		{
			fValueOfWorstCombinf = fFuncValueOfCombinsArrf[iCombinf];
			nIndexOfWorstCombinf = iCombinf;
		} //if (fFuncValueOfCombinsArrf[iCombinf] > fValueOfWorstCombinf)

	} //for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)

	return SUCCESSFUL_RETURN;
} //int WorstValueByMaxInFloatArr(...
//////////////////////////////////////////

int OptByCuckSear_ColorSeparability(
	const int nLargef,
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
	const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

	const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

	const int nNumOfItersForCuckSearf,

	const int nNumOfRandItersforRestartf,
	const int nNumOfRandItersforLocalPerturbf,
	const int nNumOfRandInitializingLoopsMaxf,
////////////////////////////////////////////////////
//for Func_BySeparabilityOf_2_ColorIntervals
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

///////////////////////////////////////////
			long &l_Seedf, //must be already specified

			//const int nFeaInitArrf[],   //[nNumOfFeasf]
		/////////////////////////////////////////////////////////////
			float &fFuncValueFinalBestf,
			int nFeaFinalBestArrf[])   //[nNumOfFeasf]
{
	int Rand_Int_2DimArr_SelecWithMoreCombinats(

		const int nNumOfSelecCombinsf,

		const int nNumOfFeasf,
		const int nFeasMinf[], //[nNumOfFeasf]
		const int nFeasMaxf[], //[nNumOfFeasf]

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified

		int n2DimArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

	int CopySelecIntArrFrom_2DimIntArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nSelecCombinsf,

		const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

		int nFeaArrf[]); //[nNumOfFeasf]

	int Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap(
		const int nDimf,
		const int nIndexOfAlreadySelecf,

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified
		int &nIndexOfNextSelecf);  //nNumOfSelecf

	int Rand_IntArr_SelecWithMoreCombinatsAndNoOverlap(
		const int nDimf,
		const int nNumOfSelecf,

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified
		int nPositOfIntArrf[]);  //nNumOfSelecf

	int Rand_IntArr_SelecWithMoreCombinats(

		const int nNumOfFeasf,

		const int nFeasMinf[], //[nNumOfFeasf]
		const int nFeasMaxf[], //[nNumOfFeasf]

		const int nNumOfRandInitializingLoopsMaxf,

		long &l_Seedf, //must be already specified

		int nFeaArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

	int ValuesOf2DimFeaArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]
	////////////////////////////////////////////////////
			//for Func_BySeparabilityOf_2_ColorIntervals
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

///////////////////////////////////////////

		float fFuncValueOfCombinsArrf[]); //[nNumOfSelecCombinsf]

	int StochHillClimbing_ColorSeparability(

		const int nNumOfFeasf,

		const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
		const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

		const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

		const int nNumOfRandItersforRestartf,
		const int nNumOfRandItersforLocalPerturbf,
		////////////////////////////////////////////////////

		//for Func_BySeparabilityOf_2_ColorIntervals
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
	///////////////////////////////////////////

		long &l_Seedf, //must be already specified

		const float fFuncValueInitf,
		const int nFeaInitArrf[],   //[nNumOfFeasf]
/////////////////////////////////////////////////////////////
		float &fFuncValueAfterHillClimbf,
		int nFeaAfterHillClimbArrf[]);   //[nNumOfFeasf]

	int BestValueByMinInFloatArr(
		const int nLargef,

		const int nNumOfSelecCombinsf,

		const float fFuncValueOfCombinsArrf[],

		int &nIndexOfBestCombinf,
		float &fValueOfBestCombinf); //[nNumOfSelecCombinsf]

	int WorstValueByMaxInFloatArr(
		const int nLargef,

		const int nNumOfSelecCombinsf,

		const float fFuncValueOfCombinsArrf[],

		int &nIndexOfWorstCombinf,
		float &fValueOfWorstCombinf); //[nNumOfSelecCombinsf]

	double dRandNumGenerFrom0To1WithUpTo2billionCombinats(
		long &l_Seedf);

	int CopyIntArr_Into_2DimIntArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nSelecCombinsf,
		const int nFeaArrf[],
		////////////////////////////////////
		int nFea2DimArrf[]);//[nNumOfSelecCombinsf*nNumOfFeasf]

	int CopyIntArr(
		const int nNumOfFeasf,
		const int nFeaInitArrf[], //[nNumOfFeasf]
		int nFeaArrf[]); //[nNumOfFeasf]

/*
	int fFuncForTest(
		const int nNumOfFeasf,

		const int nOneDimArrf[],
		float &fFuncValuef);   //[nNumOfFeasf]
*/
	int Copying_Adjusting_OneDimFeaVec_To_Intervals(
		const int nNumOfFeasf,

		int nOneDimArrf[], //[nNumOfFeasf]

		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int NonOverlapping_3ColorIntervals(
		BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int Func_BySeparabilityOf_2_ColorIntervals(
		const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

		float &fBestBinSeparabilityBy_2_Intervalsf);

	int Printing_All_Combins(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const float fFuncValueOfCombinsCurArrf[], //[nNumOfSelecCombinsf]
		const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]
		float &fAverOfFuncValuesf); //[nNumOfSelecCombinsf*nNumOfFeasf]

	int Printing_Boundaries_Of_Intervals(
		const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

	int fMeanOfFloatArr(
		const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
		const float fArrf[], //	

		float &fMeanf);
/////////////////////////////////////
	int
		nIndexOfBestCombinCurf,
		nIndexOfWorstCombinCurf,
		nIndicSuccessOrNotf = 0, //1 -- Success, 0 -- Not

		nIndexForRandSelecCurf,
		nIndexForRandSelecMaxf = nNumOfSelecCombinsf - 1,

		iIterForCuckSearf,

		iFeaf,
		nNumOfWorstSubstitsf = 0,
		nResf;

	float

		fFuncValueBestCurf,
		fFuncValueWorstCurf,

		fBestBinSeparabilityBy_2_Intervalsf,
		fFuncValueAfterHillClimbf,

		fFuncValueForSubstitutCurf,
		fFuncValueOfCombinsCurAverf,

		fAverOfFuncValuesf,
		fFuncValueInitf;
	//fFuncValueCurf;

	int *nFeaForSubstitutCurArrf = new int[nNumOfFeasf];
	assert(nFeaForSubstitutCurArrf != 0);

	int *nFeaBestCurArrf = new int[nNumOfFeasf];
	assert(nFeaBestCurArrf != 0);


	int *nFeaInitArrf = new int[nNumOfFeasf];
	assert(nFeaInitArrf != 0);

	int *nFeaAfterHillClimbArrf = new int[nNumOfFeasf];
	assert(nFeaAfterHillClimbArrf != 0);

	int *nFea2DimArrf = new int[nNumOfSelecCombinsf*nNumOfFeasf];
	assert(nFea2DimArrf != 0);

	float *fFuncValueOfCombinsCurArrf = new float[nNumOfSelecCombinsf];
	assert(fFuncValueOfCombinsCurArrf != 0);

	float *fProbForItersForCuckSearArrf = new float[nNumOfItersForCuckSearf];
	assert(fProbForItersForCuckSearArrf != 0);

	for (iIterForCuckSearf = 0; iIterForCuckSearf < nNumOfItersForCuckSearf; iIterForCuckSearf++)
	{
		fProbForItersForCuckSearArrf[iIterForCuckSearf] = (float)(dRandNumGenerFrom0To1WithUpTo2billionCombinats(l_Seedf));

	} //for (iIterForCuckSearf = 0; iIterForCuckSearf < nNumOfItersForCuckSearf; iIterForCuckSearf++)
///////////////////////////////////
	BOUNDARIES_OF_INTERVALS sBoundaries_Of_Intervalsf;
	/*
	if (fout == NULL)
	{
	printf("\nAn error in opening the file 'fout'");
	getchar();	exit(1);
	} // if (...
	*/

	nResf = Rand_Int_2DimArr_SelecWithMoreCombinats(

		nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

		nNumOfFeasf, //const int nNumOfFeasf,
		nFeaWholeRegionMinArrf, //const int nFeasMinf[], //[nNumOfFeasf]
		nFeaWholeRegionMaxArrf, //const int nFeasMaxf[], //[nNumOfFeasf]

		nNumOfRandInitializingLoopsMaxf, //const int nNumOfRandInitializingLoopsMaxf,

		l_Seedf, //long &l_Seedf, //must be already specified

		nFea2DimArrf); //int n2DimArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

	nResf = ValuesOf2DimFeaArr(
		nNumOfFeasf, //const int nNumOfFeasf,
		nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

		nFea2DimArrf, //const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]
////////////////////////////////////////////////////
			//for Func_BySeparabilityOf_2_ColorIntervals
		nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
		nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

		nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

///////////////////////////////////////////
		fFuncValueOfCombinsCurArrf); //float fFuncValueOfCombinsArrf[]) //[nNumOfSelecCombinsf]

	nResf = BestValueByMinInFloatArr(
		nLargef, //const int nLargef,

		nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

		fFuncValueOfCombinsCurArrf, //const float fFuncValueOfCombinsArrf[],

		nIndexOfBestCombinCurf, //int &nIndexOfBestCombinf,
		fFuncValueBestCurf); //float &fValueOfBestCombinf); //[nNumOfSelecCombinsf]

	fFuncValueBest_Glob = fFuncValueBestCurf;
	printf("\n\n 'OptByCuckSear_ColorSeparability' after 'BestValueByMinInFloatArr': a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d ",
		fFuncValueBestCurf, nIndexOfBestCombinCurf);

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

	fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' after 'BestValueByMinInFloatArr': a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d ",
		fFuncValueBestCurf, nIndexOfBestCombinCurf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

	nResf = CopySelecIntArrFrom_2DimIntArr(
		nNumOfFeasf, //const int nNumOfFeasf,
		nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

		nIndexOfBestCombinCurf, //const int nSelecCombinsf, 

		nFea2DimArrf, //const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

		nFeaBestCurArrf); //int nFeaArrf[]); //[nNumOfFeasf]
/////////////////////////////////////////////////////

	for (iIterForCuckSearf = 0; iIterForCuckSearf < nNumOfItersForCuckSearf; iIterForCuckSearf++)
	{
		iIterForCuckSear_Glob = iIterForCuckSearf;

		nResf = Rand_OneInt_SelecWithMoreCombinatsAndNoOverlap(
			nNumOfSelecCombinsf, //const int nDimf,

			nIndexOfBestCombinCurf, //const int nIndexOfAlreadySelecf, 

			nNumOfRandInitializingLoopsMaxf, //const int nNumOfRandInitializingLoopsMaxf,

			l_Seedf, //long &l_Seedf, //must be already specified
			nIndexForRandSelecCurf); //int &nIndexOfNextSelecf);  //nNumOfSelecf

		nIndexForRandSelecCombin_Glob = nIndexForRandSelecCurf;

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': nIndexForRandSelecCurf = %d has been selected, iIterForCuckSearf = %d",
			nIndexForRandSelecCurf, iIterForCuckSearf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		//if ((iIterForCuckSearf / 100) * 100 == iIterForCuckSearf)
		if ((iIterForCuckSearf / 20) * 20 == iIterForCuckSearf)
		{
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': nIndexForRandSelecCurf = %d has been selected, iIterForCuckSearf = %d",
				nIndexForRandSelecCurf, iIterForCuckSearf);

			fprintf(fout, "\n\n /// So far fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d ",
				fFuncValueBestCurf, nIndexOfBestCombinCurf);

			fflush(fout);

			printf("\n\n 'OptByCuckSear_ColorSeparability': nIndexForRandSelecCurf = %d has been selected, iIterForCuckSearf = %d",
				nIndexForRandSelecCurf, iIterForCuckSearf);

			printf("\n\n /// So far fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d ",
				fFuncValueBestCurf, nIndexOfBestCombinCurf);

		} //if ( (iIterForCuckSearf/100)*100 == iIterForCuckSearf)

		if (nIndexForRandSelecCurf < 0 || nIndexForRandSelecCurf > nIndexForRandSelecMaxf)
		{
			printf("\nAn error in 'OptByCuckSear_ColorSeparability': nIndexForRandSelecCurf = %d > nIndexForRandSelecMaxf = %d || ...",
				nIndexForRandSelecCurf, nIndexForRandSelecMaxf);

			fprintf(fout, "\nAn error in 'OptByCuckSear_ColorSeparability': nIndexForRandSelecCurf = %d > nIndexForRandSelecMaxf = %d || ...",
				nIndexForRandSelecCurf, nIndexForRandSelecMaxf);

			getchar();	exit(1);
		} // if (nIndexForRandSelecCurf < 0 || nIndexForRandSelecCurf > nIndexForRandSelecMaxf)

		fFuncValueInitf = fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf];

		nResf = CopySelecIntArrFrom_2DimIntArr(
			nNumOfFeasf, //const int nNumOfFeasf,
			nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

			nIndexForRandSelecCurf, //const int nSelecCombinsf, 

			nFea2DimArrf, //const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

			nFeaInitArrf); //int nFeaArrf[]); //[nNumOfFeasf]

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' before 'StochHillClimbing_ColorSeparability': iIterForCuckSearf = %d, fFuncValueOfCombinsCurArrf[%d] = %E",
			iIterForCuckSearf, nIndexForRandSelecCurf, fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf]);

		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFeaInitArrf[%d] = %d", iFeaf, nFeaInitArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		nResf = StochHillClimbing_ColorSeparability(

			nNumOfFeasf, //const int nNumOfFeasf,

			nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
			nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

			nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

			nNumOfRandItersforRestartf, //const int nNumOfRandItersforRestartf,
			nNumOfRandItersforLocalPerturbf, //const int nNumOfRandItersforLocalPerturbf,

		////////////////////////////////////////////////////

		//for Func_BySeparabilityOf_2_ColorIntervals
			nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
			nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

			nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
			nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
		///////////////////////////////////////////

			l_Seedf, //long &l_Seedf, //must be already specified

			fFuncValueInitf, //const float fFuncValueInitf,

			nFeaInitArrf, //const int nFeaInitArrf[],   //[nNumOfFeasf]
/////////////////////////////////////////////////////////////
				fFuncValueAfterHillClimbf, //float &fFuncValueAfterHillClimbf,

				nFeaAfterHillClimbArrf); //int nFeaAfterHillClimbArrf[]);   //[nNumOfFeasf]
////////////////////////////////
#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' after 'StochHillClimbing_ColorSeparability': iIterForCuckSearf = %d, fFuncValueAfterHillClimbf = %E",
			iIterForCuckSearf, fFuncValueAfterHillClimbf);
		fprintf(fout, "\n Prev fFuncValueOfCombinsCurArrf[%d] = %E", , nIndexForRandSelecCurf, fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf]);
		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFeaAfterHillClimbArrf[%d] = %d", iFeaf, nFeaAfterHillClimbArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		if (fFuncValueAfterHillClimbf < fFuncValueInitf)
		{
			nIndicSuccessOrNotf = 1;

			nResf = CopyIntArr_Into_2DimIntArr(
				nNumOfFeasf, //const int nNumOfFeasf,
				nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

				nIndexForRandSelecCurf, //const int nSelecCombinsf, 

				nFeaAfterHillClimbArrf, //const int nFeaArrf[],

////////////////////////////////////
				nFea2DimArrf); //int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]

			fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf] = fFuncValueAfterHillClimbf;

//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			printf( "\n\n 'OptByCuckSear_ColorSeparability' after 'StochHillClimbing_ColorSeparability': a better fFuncValueOfCombinsCurArrf[%d] = %E, fFuncValueInitf = %E, iIterForCuckSearf = %d",
				nIndexForRandSelecCurf, fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf], fFuncValueInitf, iIterForCuckSearf);
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' after 'StochHillClimbing_ColorSeparability': a better fFuncValueOfCombinsCurArrf[%d] = %E, fFuncValueInitf = %E, iIterForCuckSearf = %d",
				nIndexForRandSelecCurf, fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf], fFuncValueInitf, iIterForCuckSearf);

			fflush(fout);
			//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		} //if (fFuncValueAfterHillClimbf < fFuncValueInitf)
		else
		{
#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' after 'StochHillClimbing_ColorSeparability': no improvement, nIndexForRandSelecCurf = %d",
				nIndexForRandSelecCurf);

			fprintf(fout, "\n iIterForCuckSearf = %d, fFuncValueAfterHillClimbf = %E, fFuncValueInitf = %E",
				iIterForCuckSearf, fFuncValueAfterHillClimbf, fFuncValueInitf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		} //else
/////////////////////////////

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFeaAfterHillClimbArrf[%d] = %d", iFeaf, nFeaAfterHillClimbArrf[iFeaf]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		if (fFuncValueAfterHillClimbf < fFuncValueBestCurf)
		{
			nIndicSuccessOrNotf = 1;
			fFuncValueBestCurf = fFuncValueAfterHillClimbf;

			fFuncValueBest_Glob = fFuncValueBestCurf;

			nIndexOfBestCombinCurf = nIndexForRandSelecCurf;

			nResf = CopyIntArr(
				nNumOfFeasf, //const int nNumOfFeasf,
				nFeaAfterHillClimbArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]
				nFeaBestCurArrf); //int nFeaArrf[]); //[nNumOfFeasf]

			fFuncValueOfCombinsCurArrf[nIndexForRandSelecCurf] = fFuncValueAfterHillClimbf;

//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			printf( "\n\n 'OptByCuckSear_ColorSeparability': a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d, fFuncValueInitf = %E, iIterForCuckSearf = %d",
				fFuncValueBestCurf, nIndexOfBestCombinCurf, fFuncValueInitf, iIterForCuckSearf);
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d, fFuncValueInitf = %E, iIterForCuckSearf = %d",
				fFuncValueBestCurf, nIndexOfBestCombinCurf, fFuncValueInitf, iIterForCuckSearf);

			for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
			{
				fprintf(fout, "\n nFeaBestCurArrf[%d] = %d", iFeaf, nFeaBestCurArrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

/*
			//fprintf(fout, "\n\n Printing the boundaries for the new fFuncValueBestCurf by 'StochHillClimbing_ColorSeparability': iIterForCuckSearf = %d", iIterForCuckSearf);
			//nResf = Printing_Boundaries_Of_Intervals(
				//&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
*/
			fflush(fout);

//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

		} //if (fFuncValueAfterHillClimbf < fFuncValueBestCurf)
//////////////////////////////////////////////
#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': fProbForItersForCuckSearArrf[iIterForCuckSearf] = %E, iIterForCuckSearf = %d",
			fProbForItersForCuckSearArrf[iIterForCuckSearf], iIterForCuckSearf);

#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
/////////////////////////////////////////////////////////////////////
		if (fProbForItersForCuckSearArrf[iIterForCuckSearf] < fProbOfWorstSubstitution)
		{
//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': substituting the worst solution, fProbForItersForCuckSearArrf[iIterForCuckSearf] = %E < fProbOfWorstSubstitution = %E, iIterForCuckSearf = %d",
				fProbForItersForCuckSearArrf[iIterForCuckSearf],fProbOfWorstSubstitution, iIterForCuckSearf);

//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			nNumOfWorstSubstitsf += 1;

			nResf = WorstValueByMaxInFloatArr(
				nLargef, //const int nLargef,

				nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

				fFuncValueOfCombinsCurArrf, //const float fFuncValueOfCombinsArrf[],

				nIndexOfWorstCombinCurf, //int &nIndexOfWorstCombinf,
				fFuncValueWorstCurf); //float &fValueOfWorstCombinf); //[nNumOfSelecCombinsf]

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': fFuncValueWorstCurf = %E, nIndexOfWorstCombinCurf = %d, nNumOfWorstSubstitsf = %d, iIterForCuckSearf = %d",
				fFuncValueWorstCurf, nIndexOfWorstCombinCurf, nNumOfWorstSubstitsf, iIterForCuckSearf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			nResf = Rand_IntArr_SelecWithMoreCombinats(

				nNumOfFeasf, //const int nNumOfFeasf,

				nFeaWholeRegionMinArrf, //const int nFeasMinf[], //[nNumOfFeasf]
				nFeaWholeRegionMaxArrf, //const int nFeasMaxf[], //[nNumOfFeasf]

				nNumOfRandInitializingLoopsMaxf, //const int nNumOfRandInitializingLoopsMaxf,

				l_Seedf, //long &l_Seedf, //must be already specified

				nFeaForSubstitutCurArrf); //int nFeaArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]

			nResf = CopyIntArr_Into_2DimIntArr(
				nNumOfFeasf, //const int nNumOfFeasf,
				nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

				nIndexOfWorstCombinCurf, //const int nSelecCombinsf, 

				nFeaForSubstitutCurArrf, //const int nFeaArrf[],

////////////////////////////////////
				nFea2DimArrf); //int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]

/*
			nResf = fFuncForTest(
				nNumOfFeasf, //const int nNumOfFeasf,

				nFeaForSubstitutCurArrf, //const int nOneDimArrf[],

				fFuncValueForSubstitutCurf); //float &fFuncValuef)   //[nNumOfFeasf]
*/

			nResf = Copying_Adjusting_OneDimFeaVec_To_Intervals(
				nNumOfFeas, //const int nNumOfFeasf,

				nFeaForSubstitutCurArrf, //const int nOneDimArrf[], //[nNumOfFeasf]

				&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

			nResf = NonOverlapping_3ColorIntervals(
				&sBoundaries_Of_Intervalsf); // BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf);

			if ((iIterForCuckSearf / 20) * 20 == iIterForCuckSearf)
			{
				fprintf(fout, "\n\n Printing the boundaries: iIterForCuckSearf = %d", iIterForCuckSearf);
				nResf = Printing_Boundaries_Of_Intervals(
					&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
			} //if ((iIterForCuckSearf / 20) * 20 == iIterForCuckSearf)

			nResf = Func_BySeparabilityOf_2_ColorIntervals(
				nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
				nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

				&sBoundaries_Of_Intervalsf, //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf,

				nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
				nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

				fBestBinSeparabilityBy_2_Intervalsf); // float &fBestBinSeparabilityBy_2_Intervalsf);
////////////////////////////////////////

			fFuncValueForSubstitutCurf = fBestBinSeparabilityBy_2_Intervalsf;
///////////////////////////////////////////////////
			fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf] = fFuncValueForSubstitutCurf;

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': the worst nIndexOfWorstCombinCurf = %d has been substituted",
				nIndexOfWorstCombinCurf);

			fprintf(fout, "\n fFuncValueWorstCurf = %E, nIndexOfWorstCombinCurf = %d, fFuncValueForSubstitutCurf = %E, iIterForCuckSearf = %d",
				fFuncValueWorstCurf, nIndexOfWorstCombinCurf, fFuncValueForSubstitutCurf, iIterForCuckSearf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			if (fFuncValueForSubstitutCurf < fFuncValueBestCurf)
			{
				nIndicSuccessOrNotf = 1;
				fFuncValueBestCurf = fFuncValueForSubstitutCurf;

				fFuncValueBest_Glob = fFuncValueBestCurf;

				nIndexOfBestCombinCurf = nIndexOfWorstCombinCurf;

				nResf = CopyIntArr(
					nNumOfFeasf, //const int nNumOfFeasf,

					nFeaForSubstitutCurArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]
					nFeaBestCurArrf); //int nFeaArrf[]); //[nNumOfFeasf]

				fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf] = fFuncValueForSubstitutCurf;

				printf( "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a new fFuncValueBestCurf = %E by substit., nIndexOfWorstCombinCurf = %d, iIterForCuckSearf = %d",
					fFuncValueBestCurf, nIndexOfWorstCombinCurf, iIterForCuckSearf);

				for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
				{
					printf( "\n nFeaBestCurArrf[%d] = %d", iFeaf, nFeaBestCurArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
				fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a new fFuncValueBestCurf = %E by substit., nIndexOfWorstCombinCurf = %d, iIterForCuckSearf = %d",
					fFuncValueBestCurf, nIndexOfWorstCombinCurf, iIterForCuckSearf);

				for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
				{
					fprintf(fout, "\n nFeaBestCurArrf[%d] = %d", iFeaf, nFeaBestCurArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

				fprintf(fout, "\n\n Printing the boundaries for the new fFuncValueBestCurf: iIterForCuckSearf = %d", iIterForCuckSearf);
				nResf = Printing_Boundaries_Of_Intervals(
					&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)

				fflush(fout);
//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			} //if (fFuncValueAfterHillClimbf < fFuncValueBestCurf)
	////////////////////////////////////////////////////////////////////

			nResf = StochHillClimbing_ColorSeparability(

				nNumOfFeasf, //const int nNumOfFeasf,

				nFeaWholeRegionMinArrf, //const int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
				nFeaWholeRegionMaxArrf, //const int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

				nLocalRadiusOfFeaNeighArrf, //const int nLocalRadiusOfFeaNeighArrf[], //[nNumOfFeasf]

				nNumOfRandItersforRestartf, //const int nNumOfRandItersforRestartf,
				nNumOfRandItersforLocalPerturbf, //const int nNumOfRandItersforLocalPerturbf,
	////////////////////////////////////////////////////

		//for Func_BySeparabilityOf_2_ColorIntervals
				nAreaOf_NorImagesArrf, //const int nAreaOf_NorImagesArrf[], //[nNumOfVecs_Normal],
				nAreaOf_MalImagesArrf, //const int nAreaOf_MalImagesArrf[], //[nNumOfVecs_Malignant],

				nAllBinsAll_Colors_NorImagesArrf, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
				nAllBinsAll_Colors_MalImagesArrf, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
			///////////////////////////////////////////
				l_Seedf, //long &l_Seedf, //must be already specified

				fFuncValueForSubstitutCurf, //const float fFuncValueInitf,

				nFeaForSubstitutCurArrf, //const int nFeaInitArrf[],   //[nNumOfFeasf]
/////////////////////////////////////////////////////////////
					fFuncValueAfterHillClimbf, //float &fFuncValueAfterHillClimbf,

					nFeaAfterHillClimbArrf); //int nFeaAfterHillClimbArrf[]);   //[nNumOfFeasf]

			if (fFuncValueAfterHillClimbf < fFuncValueForSubstitutCurf)
			{
				nIndicSuccessOrNotf = 1;

				nResf = CopyIntArr_Into_2DimIntArr(
					nNumOfFeasf, //const int nNumOfFeasf,
					nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf, 

					nIndexOfWorstCombinCurf, //const int nSelecCombinsf, 

					nFeaAfterHillClimbArrf, //const int nFeaArrf[],

	////////////////////////////////////
					nFea2DimArrf); //int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]

				fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf] = fFuncValueAfterHillClimbf;

//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
				printf( "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a better fFuncValueOfCombinsCurArrf[%d] = %E, fFuncValueForSubstitutCurf = %E, iIterForCuckSearf = %d",
					nIndexOfWorstCombinCurf, fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf], fFuncValueForSubstitutCurf, iIterForCuckSearf);

				fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a better fFuncValueOfCombinsCurArrf[%d] = %E, fFuncValueForSubstitutCurf = %E, iIterForCuckSearf = %d",
					nIndexOfWorstCombinCurf, fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf], fFuncValueForSubstitutCurf, iIterForCuckSearf);
				fflush(fout);

//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			} //if (fFuncValueAfterHillClimbf < fFuncValueForSubstitutCurf)
			else
			{

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
				fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' after 'StochHillClimbing_ColorSeparability' in the worst case substitution: no improvement, nIndexOfWorstCombinCurf = %d",
					nIndexOfWorstCombinCurf);

				fprintf(fout, "\n iIterForCuckSearf = %d, fFuncValueAfterHillClimbf = %E, fFuncValueForSubstitutCurf = %E",
					iIterForCuckSearf, fFuncValueAfterHillClimbf, fFuncValueForSubstitutCurf);
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

			} //else

#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
			for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
			{
				fprintf(fout, "\n The worst case substitution: nFeaAfterHillClimbArrf[%d] = %d", iFeaf, nFeaAfterHillClimbArrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

//////////////////////////////////////////////////////////
			if (fFuncValueAfterHillClimbf < fFuncValueBestCurf)
			{
				nIndicSuccessOrNotf = 1;
				fFuncValueBestCurf = fFuncValueAfterHillClimbf;

				fFuncValueBest_Glob = fFuncValueBestCurf;

				nIndexOfBestCombinCurf = nIndexOfWorstCombinCurf;

				nResf = CopyIntArr(
					nNumOfFeasf, //const int nNumOfFeasf,
					nFeaAfterHillClimbArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]
					nFeaBestCurArrf); //int nFeaArrf[]); //[nNumOfFeasf]

				fFuncValueOfCombinsCurArrf[nIndexOfWorstCombinCurf] = fFuncValueAfterHillClimbf;

//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
				printf( "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d, iIterForCuckSearf = %d",
					fFuncValueBestCurf, nIndexOfBestCombinCurf, iIterForCuckSearf);

				for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
				{
					printf( "\n The worst case substitution: nFeaBestCurArrf[%d] = %d", iFeaf, nFeaBestCurArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

				fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability' in the worst case substitution: a new fFuncValueBestCurf = %E, nIndexOfBestCombinCurf = %d, iIterForCuckSearf = %d",
					fFuncValueBestCurf, nIndexOfBestCombinCurf, iIterForCuckSearf);

				for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
				{
					fprintf(fout, "\n The worst case substitution: nFeaBestCurArrf[%d] = %d", iFeaf, nFeaBestCurArrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability

				fFuncValueBest_Glob = fFuncValueBestCurf;
/*
//'sBoundaries_Of_Intervalsf' may not be directly related to 'nFeaAfterHillClimbArrf[]'
				fFuncValueBest_Glob = fFuncValueBestCurf;

				fprintf(fout, "\n\n Printing the boundaries for the new fFuncValueBestCurf in the worst case substitution: iIterForCuckSearf = %d", iIterForCuckSearf);
				nResf = Printing_Boundaries_Of_Intervals(
					&sBoundaries_Of_Intervalsf); //const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
*/
			} //if (fFuncValueAfterHillClimbf < fFuncValueBestCurf)

		} // if (fProbForItersForCuckSearArrf[iIterForCuckSearf] < fProbOfWorstSubstitution )
/*
		nResf = fMeanOfFloatArr(
			nNumOfSelecCombinsf, // const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
			fFuncValueOfCombinsCurArrf, //const float fArrf[], //	

			fFuncValueOfCombinsCurAverf); // float &fMeanf)
*/
//#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		if ((iIterForCuckSearf / 20) * 20 == iIterForCuckSearf)
		{
			fprintf(fout, "\n\n 'OptByCuckSear_ColorSeparability': printing all combins., iIterForCuckSearf = %d, fFuncValueBestCurf = %E", iIterForCuckSearf, fFuncValueBestCurf);
			nResf = Printing_All_Combins(
				nNumOfFeasf, //const int nNumOfFeasf,
				nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf,

				fFuncValueOfCombinsCurArrf, //const float fFuncValueOfCombinsCurArrf[], //[nNumOfSelecCombinsf]
				nFea2DimArrf, //const int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]
				fAverOfFuncValuesf); // float &fAverOfFuncValuesf); //[nNumOfSelecCombinsf*nNumOfFeasf]

		//	printf("\n\n The end of iIterForCuckSearf = %d, fAverOfFuncValuesf = %E, fFuncValueOfCombinsCurAverf = %E",
			//	iIterForCuckSearf, fAverOfFuncValuesf, fFuncValueOfCombinsCurAverf);

		//	fprintf(fout, "\n\n The end of iIterForCuckSearf = %d, fAverOfFuncValuesf = %E, fFuncValueOfCombinsCurAverf = %E",
			//	iIterForCuckSearf, fAverOfFuncValuesf, fFuncValueOfCombinsCurAverf);

			printf("\n\n The end of iIterForCuckSearf = %d, fAverOfFuncValuesf = %E", iIterForCuckSearf, fAverOfFuncValuesf);
			fprintf(fout, "\n\n The end of iIterForCuckSearf = %d, fAverOfFuncValuesf = %E", iIterForCuckSearf, fAverOfFuncValuesf);

			fflush(fout);
			//#endif //#ifndef SKIPPNG_PRINTING_IN_OptByCuckSear_ColorSeparability
		} //if ((iIterForCuckSearf / 20) * 20 == iIterForCuckSearf)

	} //for (iIterForCuckSearf = 0; iIterForCuckSearf < nNumOfItersForCuckSearf; iIterForCuckSearf++)

	fFuncValueFinalBestf = fFuncValueBestCurf;
	nResf = CopyIntArr(
		nNumOfFeasf, //const int nNumOfFeasf,
		nFeaBestCurArrf, //const int nFeaInitArrf[], //[nNumOfFeasf]
		nFeaFinalBestArrf); //int nFeaArrf[]); //[nNumOfFeasf]

	fflush(fout);

	delete[] nFeaForSubstitutCurArrf;
	delete[] nFeaBestCurArrf;

	delete[] nFeaInitArrf;
	delete[] nFeaAfterHillClimbArrf;

	delete[] nFea2DimArrf;
	delete[] fFuncValueOfCombinsCurArrf;
	delete[] fProbForItersForCuckSearArrf;

	return SUCCESSFUL_RETURN;
} //int OptByCuckSear_ColorSeparability(..
////////////////////////////////////////////////

void InitOfFeasOfWholeRegionAndLocalRadii(
	const int nNumOfFeasf,

	const int nLocalRadiusOfWholeRegionf,
	const int nLocalRadiusOfNeighf,

	int nFeaWholeRegionMinArrf[], //[nNumOfFeasf]
	int nFeaWholeRegionMaxArrf[], //[nNumOfFeasf]

	int nLocalRadiusOfFeaNeighArrf[]) //[nNumOfFeasf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
	{
		nFeaWholeRegionMinArrf[iFeaf] = 0; // -nLocalRadiusOfWholeRegionf;
		nFeaWholeRegionMaxArrf[iFeaf] = nLocalRadiusOfWholeRegionf;

		nLocalRadiusOfFeaNeighArrf[iFeaf] = nLocalRadiusOfNeighf;
	} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

} //void InitOfFeasOfWholeRegionAndLocalRadii(...
////////////////////////////////////////////

int Printing_All_Combins(
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	const float fFuncValueOfCombinsCurArrf[], //[nNumOfSelecCombinsf]
	const int nFea2DimArrf[],
	float &fAverOfFuncValuesf) //[nNumOfSelecCombinsf*nNumOfFeasf]
{
	int
		iCombinf,
		iFeaf;
/////////////////////////////
	fAverOfFuncValuesf = 0.0;

	fprintf(fout, "\n\n/////////////////////////////////////////////////");
	fprintf(fout, "\n 'Printing_All_Combins':");

	for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)
	{
		fprintf(fout, "\n\n fFuncValueOfCombinsCurArrf[%d] = %E", iCombinf, fFuncValueOfCombinsCurArrf[iCombinf]);

		fAverOfFuncValuesf += fFuncValueOfCombinsCurArrf[iCombinf];
		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			fprintf(fout, "\n nFea2DimArrf[%d + (%d*nNumOfFeasf) ] = %d",
				iFeaf, iCombinf, nFea2DimArrf[iFeaf + (iCombinf*nNumOfFeasf)]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	} //for (iCombinf = 0; iCombinf < nNumOfSelecCombinsf; iCombinf++)

	fAverOfFuncValuesf = fAverOfFuncValuesf / nNumOfSelecCombinsf;
	return SUCCESSFUL_RETURN;
} //int Printing_All_Combins(...
 //////////////////////////////////////////////////////////

int InsertSortOf_IntArrWithPosit(
	const int nDimf,

	int nPositionArrf[], //[nDimf]
	int nArrf[]) //[nDimf]
{
	//From smaller to larger
	int
		nTempf,
		i1,
		i2;

	float
		fTempf;

	if (nDimf < 2)
	{
		printf("\nAn error in 'InsertSortOf_IntArrWithPosit': nDimf < 2");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\nAn error in 'InsertSortOf_IntArrWithPosit': nDimf < 2");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nDimf < 2)

	for (i1 = 0; i1 < nDimf; i1++)
		nPositionArrf[i1] = i1;

	for (i1 = 1; i1 < nDimf; i1++)
	{
		fTempf = nArrf[i1];
		nTempf = nPositionArrf[i1];

		i2 = i1;
		while ((i2 > 0) && nArrf[i2 - 1] > fTempf)
		{
			nArrf[i2] = nArrf[i2 - 1];
			nPositionArrf[i2] = nPositionArrf[i2 - 1];

			i2 = i2 - 1;
		} // while ( (i2 > 0) && nArrf[i2-1] > fTempf)

		nArrf[i2] = fTempf;
		nPositionArrf[i2] = nTempf;
	} // for (i1=1; i1 < nDimf; i1++)

	return SUCCESSFUL_RETURN;
} // int InsertSortOf_IntArrWithPosit(const int nDimf, int nArrf[])
//////////////////////////////////////////////

void Rearranging_nFea2DimArrf_ForColor_Separability(
	const int nNumOfFeasf,
	const int nNumOfSelecCombinsf,

	int nFea2DimArrf[]) //[nNumOfSelecCombinsf*nNumOfFeasf]
{
	int CopySelecIntArrFrom_2DimIntArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nSelecCombinsf,

		const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

		int nFeaArrf[]); //[nNumOfFeasf]

	int Extracting_FeasOfOneColor_From_nFeaArrf(
		const int nColorf, // 0-Red,1-Green, 2-Blue
		const int nFeaArrf[], ///[nNumOfFeasf]

		int nFeasOfOneColorArrf[]); //[nNumOfFeasOfOneColor] == 4

	int InsertSortOf_IntArrWithPosit(
		const int nDimf,

		int nPositionArrf[], //[nDimf]
		int nArrf[]); //[nDimf]

	int Copying_FeasOfOneColor_To_nFeaArrf(
		const int nColorf, // 0-Red,1-Green, 2-Blue

		const int nFeasOfOneColorArrf[],  //[nNumOfFeasOfOneColor] == 4

		int nFeaArrf[]); ///[nNumOfFeasf]

	int CopyIntArr_Into_2DimIntArr(
		const int nNumOfFeasf,
		const int nNumOfSelecCombinsf,

		const int nSelecCombinsf,
		const int nFeaArrf[],
		////////////////////////////////////
		int nFea2DimArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

///////////////////////////////
	int
		nResf,

		nFeasOfOneColorArrf[nNumOfFeasOfOneColor],
		iFeaf,
		iColorf,
		iVecf;

	int *nFeaArrf = new int[nNumOfFeasf];
	assert(nFeaArrf != 0);
	
	int *nPositionArrf = new int[nNumOfFeasOfOneColor];
	assert(nPositionArrf != 0);
////////////////////////////////////////

	for (iFeaf = 0; iFeaf < nNumOfFeasOfOneColor; iFeaf++)
	{
		nPositionArrf[iFeaf] = iFeaf;
	} //for (iFeaf = 0; iFeaf < nNumOfFeasOfOneColor; iFeaf++)
	/////////////////////////

	for (iVecf = 0; iVecf < nNumOfSelecCombinsf; iVecf++)
	{
		nResf = CopySelecIntArrFrom_2DimIntArr(
			nNumOfFeasf, //const int nNumOfFeasf,
			nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf,

			iVecf, //const int nSelecCombinsf,

			nFea2DimArrf, //const int nFea2DimArrf[], //[nNumOfSelecCombinsf*nNumOfFeasf]

			nFeaArrf); // int nFeaArrf[]); //[nNumOfFeasf]

		for (iColorf = 0; iColorf < 3; iColorf++)
		{
			nResf = Extracting_FeasOfOneColor_From_nFeaArrf(
				iColorf, //const int nColorf, // 0-Red,1-Green, 2-Blue
				nFeaArrf, //const int nFeaArrf[], ///[nNumOfFeasf]

				nFeasOfOneColorArrf); // int nFeasOfOneColorArrf[]); //[nNumOfFeasOfOneColor] == 4

			nResf = InsertSortOf_IntArrWithPosit(
				nNumOfFeasOfOneColor, //const int nDimf,

				nPositionArrf, //int nPositionArrf[], //[nDimf]
				nFeasOfOneColorArrf); // int nArrf[]); //[nDimf]

			nResf = Copying_FeasOfOneColor_To_nFeaArrf(
				iColorf, //const int nColorf, // 0-Red,1-Green, 2-Blue

				nFeasOfOneColorArrf, //const int nFeasOfOneColorArrf[],  //[nNumOfFeasOfOneColor] == 4

				nFeaArrf); // int nFeaArrf[]); ///[nNumOfFeasf]

		} //for (iColorf = 0; iColorf < 3; iColorf++)

		nResf =  CopyIntArr_Into_2DimIntArr(
			nNumOfFeasf, //const int nNumOfFeasf,
			nNumOfSelecCombinsf, //const int nNumOfSelecCombinsf,

			iVecf, //const int nSelecCombinsf,

			nFeaArrf, //const int nFeaArrf[],
			////////////////////////////////////
			nFea2DimArrf); // int nFea2DimArrf[]); //[nNumOfSelecCombinsf*nNumOfFeasf]

	}//for (iVecf = 0; iVecf < nNumOfSelecCombinsf; iVecf++)

	fprintf(fout, "\n\n/////////////////////////////////////////////////");
	fprintf(fout, "\n 'Rearranging_nFea2DimArrf_ForColor_Separability':");

	for (iVecf = 0; iVecf < nNumOfSelecCombinsf; iVecf++)
	{
		fprintf(fout, "\n\n 'Rearranging_nFea2DimArrf_ForColor_Separability': iVecf = %d", iVecf);

		for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)
		{
			if ((iFeaf / nNumOfBoundariesFor_3ColorsAnd_OneInterval)*nNumOfBoundariesFor_3ColorsAnd_OneInterval == iFeaf)
				fprintf(fout, "\n\n The next color interval = %d", iFeaf / nNumOfBoundariesFor_3ColorsAnd_OneInterval);

			fprintf(fout, "\n nFea2DimArrf[%d + (%d*nNumOfFeasf) ] = %d",
				iFeaf, iVecf, nFea2DimArrf[iFeaf + (iVecf*nNumOfFeasf)]);
		} //for (iFeaf = 0; iFeaf < nNumOfFeasf; iFeaf++)

	} //for (iVecf = 0; iVecf < nNumOfSelecCombinsf; iVecf++)

	fflush(fout);

	delete[] nFeaArrf;
	delete[] nPositionArrf;
}//void Rearranging_nFea2DimArrf_ForColor_Separability(
/////////////////////////////////////////

int Extracting_FeasOfOneColor_From_nFeaArrf(
	const int nColorf, // 0-Red,1-Green, 2-Blue
	const int nFeaArrf[], ///[nNumOfFeasf]

	int nFeasOfOneColorArrf[]) //[nNumOfFeasOfOneColor] == 4
{
	int
		nBeginFor_iFeaForOneIntervalf,

		nFeasOfOneColorTotf, 
		nFeaCurf,
		iIntervalf,
		iFeaForOneIntervalf;
//////////////////////////////////////

	nFeasOfOneColorTotf = 0;
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		nBeginFor_iFeaForOneIntervalf = nColorf*2 + (iIntervalf * nNumOfBoundariesFor_3ColorsAnd_OneInterval);

		for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < 2; iFeaForOneIntervalf++)
		{
			nFeaCurf = iFeaForOneIntervalf + nBeginFor_iFeaForOneIntervalf;
			if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)
			{
				printf("\n\n An error in 'Extracting_FeasOfOneColor_From_nFeaArrf': nFeaCurf = %d >= nNumOfFeas = %d", nFeaCurf, nNumOfFeas);

				fflush(fout); getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)

/*
			if (nFeaArrf[nFeaCurf] < 0)
				nFeaArrf[nFeaCurf] = 0;
			else if (nFeaArrf[nFeaCurf] >= nNumOfBins_OneColor)
				nFeaArrf[nFeaCurf] = nNumOfBins_OneColor - 1;
*/
			nFeasOfOneColorTotf += 1;

			if (nFeasOfOneColorTotf > nNumOfFeasOfOneColor)
			{
				printf("\n\n An error in 'Extracting_FeasOfOneColor_From_nFeaArrf': nFeasOfOneColorTotf = %d > nNumOfFeasOfOneColor = %d", 
					nFeasOfOneColorTotf, nNumOfFeasOfOneColor);

				fflush(fout); getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nFeasOfOneColorTotf > nNumOfFeasOfOneColor)

			nFeasOfOneColorArrf[nFeasOfOneColorTotf - 1] = nFeaArrf[nFeaCurf];
		} //for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < 2; iFeaForOneIntervalf++)

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)

	if (nFeasOfOneColorTotf != nNumOfFeasOfOneColor)
	{
		printf("\n\n An error in 'Extracting_FeasOfOneColor_From_nFeaArrf': nFeasOfOneColorTotf = %d != nNumOfFeasOfOneColor = %d",
			nFeasOfOneColorTotf, nNumOfFeasOfOneColor);

		fflush(fout); getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nFeasOfOneColorTotf != nNumOfFeasOfOneColor)

	return SUCCESSFUL_RETURN;
}//int Extracting_FeasOfOneColor_From_nFeaArrf(
//////////////////////////////////////////////

int Copying_FeasOfOneColor_To_nFeaArrf(
	const int nColorf, // 0-Red,1-Green, 2-Blue

	const int nFeasOfOneColorArrf[],  //[nNumOfFeasOfOneColor] == 4

	int nFeaArrf[]) ///[nNumOfFeasf]
{
	int
		nBeginFor_iFeaForOneIntervalf,

		nFeasOfOneColorTotf,
		nFeaCurf,
		iIntervalf,
		iFeaForOneIntervalf;
	//////////////////////////////////////

	nFeasOfOneColorTotf = 0;
	for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)
	{
		nBeginFor_iFeaForOneIntervalf = nColorf * 2 + (iIntervalf * nNumOfBoundariesFor_3ColorsAnd_OneInterval);

		for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < 2; iFeaForOneIntervalf++)
		{
			nFeaCurf = iFeaForOneIntervalf + nBeginFor_iFeaForOneIntervalf;
			if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)
			{
				printf("\n\n An error in 'Copying_FeasOfOneColor_To_nFeaArrf': nFeaCurf = %d >= nNumOfFeas = %d", nFeaCurf, nNumOfFeas);

				fflush(fout); getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nFeaCurf < 0 || nFeaCurf >= nNumOfFeas)

/*
			if (nFeaArrf[nFeaCurf] < 0)
				nFeaArrf[nFeaCurf] = 0;
			else if (nFeaArrf[nFeaCurf] >= nNumOfBins_OneColor)
				nFeaArrf[nFeaCurf] = nNumOfBins_OneColor - 1;
*/
			nFeasOfOneColorTotf += 1;

			if (nFeasOfOneColorTotf > nNumOfFeasOfOneColor)
			{
				printf("\n\n An error in 'Copying_FeasOfOneColor_To_nFeaArrf': nFeasOfOneColorTotf = %d > nNumOfFeasOfOneColor = %d",
					nFeasOfOneColorTotf, nNumOfFeasOfOneColor);

				fflush(fout); getchar();  exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nFeasOfOneColorTotf > nNumOfFeasOfOneColor)

			nFeaArrf[nFeaCurf] = nFeasOfOneColorArrf[nFeasOfOneColorTotf - 1];
		} //for (iFeaForOneIntervalf = 0; iFeaForOneIntervalf < 2; iFeaForOneIntervalf++)

	} //for (iIntervalf = 0; iIntervalf < nNumOfColorIntervals; iIntervalf++)

	if (nFeasOfOneColorTotf != nNumOfFeasOfOneColor)
	{
		printf("\n\n An error in 'Copying_FeasOfOneColor_To_nFeaArrf': nFeasOfOneColorTotf = %d != nNumOfFeasOfOneColor = %d",
			nFeasOfOneColorTotf, nNumOfFeasOfOneColor);

		fflush(fout); getchar();  exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (nFeasOfOneColorTotf != nNumOfFeasOfOneColor)

	return SUCCESSFUL_RETURN;
}//int Copying_FeasOfOneColor_To_nFeaArrf(
/////////////////////////////////////////////

int Printing_Boundaries_Of_Intervals(

	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
{
	int
		iColorIntervalf;
///////////////////////////////////////////////////////////////
	fprintf(fout, "\n\n/////////////////////////////////////////////////");
	fprintf(fout, "\n 'Printing_Boundaries_Of_Intervals': nNumOfColorIntervals = %d", nNumOfColorIntervals);
////////////////////////////////
//Red
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nRedIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nRedIntervals_High[%d] = %d", 
			iColorIntervalf,sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf])
	
	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////
//Green
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nGreenIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nGreenIntervals_High[%d] = %d",
			iColorIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout);  getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf])

	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
///////////////////////////////////////////////////////
//Blue
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nBlueIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nBlueIntervals_High[%d] = %d",
			iColorIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout);  getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf])

	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
////////////////////////////////////////////////////////////////////////////////

//Red Nonoverlapping
	fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals);

	//fprintf(fout_features, "\n\n/////////////////////////////////////");
	//fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[%d] = %d",
			iColorIntervalf, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf])

		//fprintf(fout_features, "\n %d %d", 
			//sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf],
			//sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]);

	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////

//Green Nonoverlapping
	fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals);

	//fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[%d] = %d",
			iColorIntervalf, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf])

	//	fprintf(fout_features, "\n %d %d",
		//	sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf],
		//	sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]);

	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////

//Blue Nonoverlapping
	fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals);
	//fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iColorIntervalf++)
	{
		fprintf(fout, "\n sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[%d] = %d",
			iColorIntervalf, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf],
			iColorIntervalf, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf])

		//fprintf(fout_features, "\n %d %d",
			//sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf],
			//sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]);
	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iColorIntervalf++)
/////////////////////////////////////////////////////////////////////////////////
	//fflush(fout_features);
	fflush(fout);
	return SUCCESSFUL_RETURN;
}//int Printing_Boundaries_Of_Intervals(
///////////////////////////////////////////////////////

int Printing_Boundaries_Of_Intervals_With_Effic(

	const float fEfficf,
	const BOUNDARIES_OF_INTERVALS *sBoundaries_Of_Intervalsf)
{
	int
		iColorIntervalf;
	///////////////////////////////////////////////////////////////
	//fprintf(fout, "\n\n/////////////////////////////////////////////////");
	//fprintf(fout, "\n 'Printing_Boundaries_Of_Intervals_With_Effic': nNumOfColorIntervals = %d", nNumOfColorIntervals);
	////////////////////////////////
	//Red
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
	//	fprintf(fout, "\n sBoundaries_Of_Intervalsf->nRedIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nRedIntervals_High[%d] = %d",
		//	iColorIntervalf, sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf],
			//iColorIntervalf, sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf])

	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////
//Green
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
		//fprintf(fout, "\n sBoundaries_Of_Intervalsf->nGreenIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nGreenIntervals_High[%d] = %d",
			//iColorIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf],
			//iColorIntervalf, sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout);  getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf])

	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
///////////////////////////////////////////////////////
//Blue
	for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
	{
		//fprintf(fout, "\n sBoundaries_Of_Intervalsf->nBlueIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nBlueIntervals_High[%d] = %d",
		//	iColorIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf],
		//	iColorIntervalf, sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout);  getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf])

	}//for (iColorIntervalf = 0; iColorIntervalf < nNumOfColorIntervals; iColorIntervalf++)
////////////////////////////////////////////////////////////////////////////////

//Red Nonoverlapping
	//fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals);

	fprintf(fout_features, "\n\n/////////////////////////////////////");
	fprintf(fout_features, "\n%E",fEfficf);
	fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iColorIntervalf++)
	{
		//fprintf(fout, "\n sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[%d] = %d",
			//iColorIntervalf, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf],
			//iColorIntervalf, sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nRedIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRedIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf])

		fprintf(fout_features, "\n %d %d",
			sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_Low[iColorIntervalf]* nBinSizeForColorSepar,
			sBoundaries_Of_Intervalsf->nRed_NonOverlappingIntervals_High[iColorIntervalf]* nBinSizeForColorSepar);

	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfRed_NonOverlappingIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////

//Green Nonoverlapping
	//fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals);

	fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iColorIntervalf++)
	{
	//	fprintf(fout, "\n sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[%d] = %d",
	//		iColorIntervalf, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf],
	//		iColorIntervalf, sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nGreenIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreenIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf])

		fprintf(fout_features, "\n %d %d",
			sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_Low[iColorIntervalf]* nBinSizeForColorSepar,
			sBoundaries_Of_Intervalsf->nGreen_NonOverlappingIntervals_High[iColorIntervalf]* nBinSizeForColorSepar);

	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfGreen_NonOverlappingIntervals; iColorIntervalf++)
//////////////////////////////////////////////////////////////////////////////////////

//Blue Nonoverlapping
	//fprintf(fout, "\n\n sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals = %d", sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals);
	fprintf(fout_features, "\n%d", sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals);

	for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iColorIntervalf++)
	{
	//	fprintf(fout, "\n sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[%d] = %d, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[%d] = %d",
		//	iColorIntervalf, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf],
		//	iColorIntervalf, sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]);

		if (sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf])
		{
			printf("\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\nAn error in 'Printing_Boundaries_Of_Intervals_With_Effic': sBoundaries_Of_Intervalsf->nBlueIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlueIntervals_High[iColorIntervalf]");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fflush(fout); getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf] > sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf])

		fprintf(fout_features, "\n %d %d",
			sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_Low[iColorIntervalf]* nBinSizeForColorSepar,
			sBoundaries_Of_Intervalsf->nBlue_NonOverlappingIntervals_High[iColorIntervalf]* nBinSizeForColorSepar);
	}//for (iColorIntervalf = 0; iColorIntervalf < sBoundaries_Of_Intervalsf->nNumOfBlue_NonOverlappingIntervals; iColorIntervalf++)
/////////////////////////////////////////////////////////////////////////////////
	fflush(fout_features);
	//fflush(fout);
	return SUCCESSFUL_RETURN;
}//int Printing_Boundaries_Of_Intervals_With_Effic(
///////////////////////////////////////////

int fMeanOfIntArr(const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
	const int nArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,

	float &fMeanf)
{
	int
		nNumRealMembf = 0,
		i1;

	fMeanf = 0.0;
	for (i1 = 0; i1 < nDimf; i1++)
	{
		if (nArrf[i1] == -1)
			break;

		nNumRealMembf += 1;
		/*
			if (nArrf[i1] < 1)
			{
			printf("\n\nAn error in 'fMeanOfIntArr': nArrf[%d] = %d < 1, ObjDataSymbArrAllf.get_nNumRecordsTotCurf() = %d",
				i1,nArrf[i1],ObjDataSymbArrAllf.get_nNumRecordsTotCurf() );
			fprintf(fout,"\n\nAn error in 'fMeanOfIntArr': nArrf[%d] = %d < 1, ObjDataSymbArrAllf.get_nNumRecordsTotCurf() = %d",
				i1,nArrf[i1],ObjDataSymbArrAllf.get_nNumRecordsTotCurf() );

			getchar(); exit(1);
			} //if (nArrf[i1] < 1)
		*/
		fMeanf += (float)(nArrf[i1]);
	} //for (i1 = 0; i1 < nDimf; i1++)

	if (i1 == 0)
	{
		fMeanf = -1.0;
		return 1;
	} //if (i1 == 0)
	else
	{
		fMeanf = fMeanf / nNumRealMembf;
	} //else

	return SUCCESSFUL_RETURN;
} //int fMeanOfIntArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
//////////////////////////////////////////////

int fMeanOfFloatArr(
	const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
	const float fArrf[], //	

	float &fMeanf)
{
	int
		nNumRealMembf = 0,
		i1;

	fMeanf = 0.0;
	for (i1 = 0; i1 < nDimf; i1++)
	{
		if (fArrf[i1] == -fLarge)
		{
			printf("\n\n An error in 'fMeanOfFloatArr': fArrf[%d] = %E", i1,fArrf[i1]);

			fprintf(fout, "\n\n An error in 'fMeanOfFloatArr': fArrf[%d] = %E", i1, fArrf[i1]);

			fflush(fout); getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
			//break;
		} //if (fArrf[i1] == -fLarge)

		nNumRealMembf += 1;
		fMeanf += (float)(fArrf[i1]);
	} //for (i1 = 0; i1 < nDimf; i1++)

	if (nNumRealMembf == 0)
	{
		fMeanf = -fLarge;

		printf("\n\n An error in 'fMeanOfFloatArr': nNumRealMembf = %d", nNumRealMembf);
		fprintf(fout, "\n\nAn error in 'fMeanOfFloatArr': nNumRealMembf = %d", nNumRealMembf);

		fflush(fout); getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (i1 == 0)
	else
	{
		fMeanf = fMeanf / nNumRealMembf;
	} //else

	return SUCCESSFUL_RETURN;
} //int fMeanOfIntArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,





/*
//From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
//From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
// Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HLS_Fr_RGB(
	const COLOR_IMAGE *sColor_Imagef, //[nImageAreaMax]

	HLS_IMAGE *sHLS_Imagef)
*/

///////////////////////////////////////////////////////////////////////////////
  //printf("\n\n Please press any key:"); getchar();



