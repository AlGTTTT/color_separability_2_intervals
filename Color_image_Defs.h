//
//  Color_image_Defs.h
//  ICEReveal
//
//  Created by Anna Yang on 10/17/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef Color_image_Defs_h
#define Color_image_Defs_h

typedef struct
{
    int
    nRed;
    
    int
    nGreen;
    int
    nBlue;
    
} WEIGHTED_RGB_COLORS;

typedef struct
{
    int nSideOfObjectLocation; //-1 - left, 1 - right
    
    int nIntensityOfBackground_Red; //
    int nIntensityOfBackground_Green; //
    int nIntensityOfBackground_Blue; //
    
    int nWidth;
    int nLength;
    
    int *nLenObjectBoundary_Arr;
    
    //    int nRed_Arr[nImageSizeMax];
    //    int nGreen_Arr[nImageSizeMax];
    //    int nBlue_Arr[nImageSizeMax];
    int *nRed_Arr;
    int *nGreen_Arr;
    int *nBlue_Arr;
    
    int *nIsAPixelBackground_Arr;
} COLOR_IMAGE;

/*
typedef struct
{
    int nWidth;
    int nLength;
    
    //    int nRed_Arr[nImageSizeMax];
    //    int nGreen_Arr[nImageSizeMax];
    //    int nBlue_Arr[nImageSizeMax];
    int *nRed_Arr;
    int *nGreen_Arr;
    int *nBlue_Arr;
    
} COLOR_IMAGE;
*/

typedef struct
{
    int nWidth;
    int nLength;
    
    int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
    int *nGrayScale_Arr;
    
} GRAYSCALE_IMAGE;

typedef struct
{
    int
    nCyan;
    int
    nMagenta;
    int
    nYellow;
    int
    nBlack;
    
    float
    fWeightCyan;
    float
    fWeightMagenta;
    float
    fWeightYellow;
    
    int
    nRed_FrWeightedCyan;
    
    int
    nGreen_FrWeightedMagenta;
    
    int
    nBlue_FrWeightedYellow;
    
} WEIGHTED_CMYK_COLORS;

typedef struct
{
    //no weighting for XYZ
    float
    fX;
    float
    fY;
    float
    fZ;
    
} XYZ_COLORS;


typedef struct
{
    
    float fL_FrXYZf;
    float fA_FrXYZf;
    float fB_FrXYZf;
    
    float
    fWeighted_L;
    float
    fWeighted_A;
    float
    fWeighted_B;
    
} WEIGHTED_LAB_COLORS;


typedef struct
{
    int nWidth;
    int nLength;
    
    float *fHue_Arr;
    float *fLightness_Arr;
    float *fSaturation_Arr;
    
} HLS_IMAGE;

#endif /* Color_image_Defs_h */
